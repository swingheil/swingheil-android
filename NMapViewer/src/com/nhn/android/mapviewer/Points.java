package com.nhn.android.mapviewer;
import java.util.List;


public class Points {

	List<GeoLocation> points;

	public List<GeoLocation> getLocations() {
		return points;
	}

	public void setLocations(List<GeoLocation> points) {
		this.points = points;
	} 
}
