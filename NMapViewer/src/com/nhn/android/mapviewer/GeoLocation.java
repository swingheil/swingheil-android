package com.nhn.android.mapviewer;


import java.io.Serializable;

/**
 * 위성 좌표 (위도, 경도)
 */
public class GeoLocation implements Serializable {
	//
	private static final long serialVersionUID = -4219877625517675569L;

	/** 위도 */
	private double latitude;
	/** 경도 */
	private double longitude;
	/** 설명 */
	private String description;

	//--------------------------------------------------------------------------
	
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
