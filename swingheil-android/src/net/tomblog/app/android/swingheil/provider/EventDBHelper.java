package net.tomblog.app.android.swingheil.provider;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.tomblog.app.android.swingheil.model.Events;
import net.tomblog.app.android.swingheil.util.L;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.swingheil.domain.event.Event;

/**
 * Event Provider
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class EventDBHelper extends SQLiteOpenHelper {

	private final String TAG = EventDBHelper.class.getName();
	
    private static final String DATABASE_NAME = "events.db";
    private static final int DATABASE_VERSION = 1;
    
    private static final String TABLE_EVENTS = "events";
	private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_EVENTS + " (" 
		+ Events._ID + " INTEGER PRIMARY KEY, " 
		+ Events.EVENT_ID + " TEXT, " 
		+ Events.REG_DATE + " LONG)";

    public EventDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		L.w(TAG, "Upgrading database from version " + oldVersion + " to "
				+ newVersion + ", which will destroy all old data");

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
		onCreate(db);
	}
	
	// 새로운 event 추가
	public void addEvent(Event event) { 
	    SQLiteDatabase db = this.getWritableDatabase(); 
	  
        Cursor cursor = db.query(TABLE_EVENTS, 
                new String[] { Events.EVENT_ID, Events.REG_DATE}, 
                Events.EVENT_ID + "=?",
                new String[] { event.getId() }, null, null, null, null);

        if (cursor == null || !cursor.moveToFirst()) {
        	// 새로운 Row 추가 
        	ContentValues values = getContentValuesFromEvent(event); 
        	db.insert(TABLE_EVENTS, null, values); 
        }
        
	    db.close(); // 연결종료
	}
	
	
	public Event getEvent(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_EVENTS, 
                             new String[] { Events.EVENT_ID, Events.REG_DATE}, 
                             Events.EVENT_ID + "=?",
                             new String[] { id }, null, null, null, null);
         
        Event event = null;
        if (cursor != null && cursor.moveToFirst()) {
        	event = new Event();
        	event.setId(cursor.getString(0)); 
        	event.setRegDate(new Date(cursor.getLong(1)));
        }

        return event;
	}
	
	public List<Event> getAllEvents() {
		List<Event> eventtList = new ArrayList<Event>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_EVENTS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if(cursor != null && cursor.moveToFirst()) {
			do {
				Event event = new Event();
	        	event.setId(cursor.getString(1)); 
	        	event.setRegDate(new Date(cursor.getLong(2)));
	        	L.d(TAG, "id: " + cursor.getString(1) + " regDate : " + cursor.getLong(2) + " " + event.getRegDate());
	        	eventtList.add(event);
			} while (cursor.moveToNext());
		}

		return eventtList;
	}
	
	public int updateEvent(Event event) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = getContentValuesFromEvent(event);

        // updating row
        return db.update(TABLE_EVENTS, values, Events.EVENT_ID + " = ?", new String[] { event.getId() });
	}
	
	public void deleteEvent(Event event) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_EVENTS, Events.EVENT_ID + " = ?", new String[] { event.getId() });
        db.close();
	}
	
	public void deleteOldEvent(Date date) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_EVENTS, Events.REG_DATE + " < ?", new String[] { String.valueOf(date.getTime()) }); 
		L.e(TAG, "deleteOldEvent " + date.getTime() + " " + date);
		db.close();
	}
	
	public int getEventsCount() { 
	    String countQuery = "SELECT  * FROM " + TABLE_EVENTS; 
	    SQLiteDatabase db = this.getReadableDatabase(); 
	    Cursor cursor = db.rawQuery(countQuery, null); 
	    cursor.close(); 
	  
	    // return count 
	    return cursor.getCount(); 
	}
	
	/**
	 * ContentValues로 데이터 변환 메소드
	 * @param event
	 * @return
	 */
	public static ContentValues getContentValuesFromEvent(Event event) {
		ContentValues values = new ContentValues();
		
		values.put(Events.EVENT_ID, event.getId());
		values.put(Events.REG_DATE, event.getRegDate().getTime());
		
		return values;
	}
}
