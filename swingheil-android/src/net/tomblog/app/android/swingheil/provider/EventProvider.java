package net.tomblog.app.android.swingheil.provider;

import net.tomblog.app.android.swingheil.model.Article;
import net.tomblog.app.android.swingheil.model.Events;
import net.tomblog.app.android.swingheil.util.L;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

/**
 * Event Provider
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class EventProvider extends ContentProvider {

	private final String TAG = EventProvider.class.getName();
	
    private static final String DATABASE_NAME = "events.db";
    private static final int DATABASE_VERSION = 1;
    
    public static final String UPDATED = "updated";
    public static final String INSERTED = "inserted";

    private static final int EVENT_ID = 1;
    private static final int EVENTS = 2;
    
    private static final String TABLE = "articles";
	private static final String CREATE_TABLE = "CREATE TABLE " + TABLE + " (" 
		+ Events._ID + " INTEGER PRIMARY KEY, " 
		+ Events.EVENT_ID + " TEXT, " 
		+ Events.REG_DATE + " LONG)";

    private static final UriMatcher URI_MATCHER;
    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(Article.AUTHORITY, "articles", EVENTS);
        URI_MATCHER.addURI(Article.AUTHORITY, "articles/#", EVENT_ID);
    }
    
    private SQLiteOpenHelper mOpenHelper;
    
	@Override
	public boolean onCreate() {
		L.d(TAG, " >>> called onCreate()");
        mOpenHelper = new DatabaseHelper(getContext());
        return true;
	}
	
	@Override
	public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
        case EVENTS:
            return "vnd.android.cursor.dir/vnd.net.tomblog.swingheil.event";
        case EVENT_ID:
            return "vnd.android.cursor.item/vnd.net.tomblog.swingheil.event";
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
	}
	
	@Override
	public Uri insert(Uri uri, ContentValues values) {

		if (URI_MATCHER.match(uri) != EVENTS) {
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
		
		// 같은 글이 있는지 확인하여, 있다면 update
		// (EVENT_ID 로 판단)
		String eventId = values.getAsString(Events.EVENT_ID);
		
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TABLE);
        qb.appendWhere(Events.EVENT_ID + "='" + eventId + "'");
        Cursor cursor = qb.query(db, null, null, null, null, null, null);
		if(cursor.getCount() > 0 && cursor.moveToFirst()) {
			String id = cursor.getString(cursor.getColumnIndex(Article._ID));
			Uri updateUri = Uri.withAppendedPath(Events.CONTENT_URI, id); 
			update(updateUri, values, null, null);
//			L.d(TAG, "updated : " + updateUri.toString());
			updateUri = Uri.withAppendedPath(updateUri, UPDATED);
			updateUri = Uri.withAppendedPath(updateUri, String.valueOf(eventId));
			return updateUri;
		}
        
		// 새로운 글이라면 insert
        final long rowId = db.insert(TABLE, null, values);
        if (rowId > 0) {
            Uri insertUri = ContentUris.withAppendedId(Events.CONTENT_URI, rowId);
            getContext().getContentResolver().notifyChange(uri, null);
            insertUri = Uri.withAppendedPath(insertUri, INSERTED);
            insertUri = Uri.withAppendedPath(insertUri, String.valueOf(eventId));
            return insertUri;
        }

        throw new SQLException("Failed to insert row into " + uri);
	}
	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        switch (URI_MATCHER.match(uri)) {
            case EVENTS:
                qb.setTables(TABLE);
                break;
            case EVENT_ID:
                qb.setTables(TABLE);
                qb.appendWhere("_id=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        
        String orderBy;
        if (TextUtils.isEmpty(sortOrder)) {
            orderBy = Article.DEFAULT_SORT_ORDER;
        } else {
            orderBy = sortOrder;
        }
        
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor cursor = qb.query(db, projection, selection, selectionArgs, null, null, orderBy);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
	}
	
	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
	     SQLiteDatabase db = mOpenHelper.getWritableDatabase();
	     
		int count = 0;
		switch (URI_MATCHER.match(uri)) {
		case EVENTS:
			count = db.update(TABLE, values, selection, selectionArgs);
			break;
		case EVENT_ID:
			count = db.update(TABLE, values, Article._ID
					+ " = "
					+ uri.getPathSegments().get(1)
					+ (!TextUtils.isEmpty(selection) ? " AND (" + selection
							+ ')' : ""), selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
		
		getContext().getContentResolver().notifyChange(uri, null);

		return count;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        int count = 0;
		switch (URI_MATCHER.match(uri)) {
		case EVENTS:
			count = db.delete(TABLE, selection, selectionArgs);
			break;
		case EVENT_ID:
			String segment = uri.getPathSegments().get(1);
			count = db.delete(TABLE, Article._ID
					+ "="
					+ segment
					+ (!TextUtils.isEmpty(selection) ? " AND (" + selection
							+ ')' : ""), selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

        getContext().getContentResolver().notifyChange(uri, null);

        return count;
	}
	
	private static class DatabaseHelper extends SQLiteOpenHelper {
		private final String TAG = "DatabaseHelper";

		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_TABLE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			L.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");

			db.execSQL("DROP TABLE IF EXISTS books");
			onCreate(db);
		}
	}
	
	/**
	 * Content Provider에 데이터 삽입을 위한 변환 메소드
	 * @param event
	 * @return
	 */
	public static ContentValues getContentValuesFromEvent(Events event) {
		ContentValues values = new ContentValues();
		
		values.put(Events.EVENT_ID, event.getEventId());
		values.put(Events.REG_DATE, event.getRegDate().getTime());
		
		return values;
	}
}
