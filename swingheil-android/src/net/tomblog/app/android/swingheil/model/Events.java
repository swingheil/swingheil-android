package net.tomblog.app.android.swingheil.model;

import java.util.Date;

import android.net.Uri;
import android.provider.BaseColumns;

public class Events implements BaseColumns{
	
    public static final String AUTHORITY = "net.tomblog.provider.Events";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/events");
	
    public static final String EVENT_ID = "event_id";
    public static final String REG_DATE = "reg_date";
	public static final String DEFAULT_SORT_ORDER = "reg_date desc";
    
	public static String[] PROJECTION = new String[] { Events._ID, Events.EVENT_ID, Events.REG_DATE};
	public static String SELECTION = Events.EVENT_ID + "=?";
	
	private String eventId;
	
	private Date regDate;
	
	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	@Override
	public String toString() {
		return "Event [eventId=" + eventId + ", regDate=" + regDate + "]";
	}
}