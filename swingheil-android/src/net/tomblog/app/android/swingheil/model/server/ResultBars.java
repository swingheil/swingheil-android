package net.tomblog.app.android.swingheil.model.server;

import java.util.List;

import net.tomblog.app.android.swingheil.util.Util;

import com.swingheil.domain.group.Bar;

public class ResultBars {
	//
	private String version;
	
	private List<Bar> bars;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<Bar> getBars() {
		return bars;
	}

	public void setBars(List<Bar> bars) {
		this.bars = bars;
	}
	
	@Override
	public String toString() {
		return Util.toString(this);
	}
}
