package net.tomblog.app.android.swingheil.model;

import android.net.Uri;

public class QueryArgs {

	private Uri uri;

	private String[] projection;

	private String selection;

	private String[] selectionArgs;

	public QueryArgs(Uri uri, String[] projection, String selection,
			String[] selectionArgs) {
		this.uri = uri;
		this.projection = projection;
		this.selection = selection;
		this.selectionArgs = selectionArgs;
	}

	public Uri getUri() {
		return uri;
	}

	public void setUri(Uri uri) {
		this.uri = uri;
	}

	public String[] getProjection() {
		return projection;
	}

	public void setProjection(String[] projection) {
		this.projection = projection;
	}

	public String getSelection() {
		return selection;
	}

	public void setSelection(String selection) {
		this.selection = selection;
	}

	public String[] getSelectionArgs() {
		return selectionArgs;
	}

	public void setSelectionArgs(String[] selectionArgs) {
		this.selectionArgs = selectionArgs;
	}
}