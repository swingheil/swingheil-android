package net.tomblog.app.android.swingheil.model;

import net.tomblog.app.android.swingheil.parser.cafe.DaumArticleParser;

/**
 * 게시글 리스트의 페이징을 위한 객체
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class ArticleSearch {
	
    /**
     * 글 목록 조회시 한 페이지당 가져오는 기본 글 수(네이버는 15개, 다음은 20개)
     */
    public static final int DEFAULT_SEARCH_PER_PAGE = 15; 
    
	/**
	 * 다음 까페 아이디
	 */
	private String cafeId;
	
	/**
	 * 까페의 게시판 아이디
	 */
	private String boardId;
	
	/**
	 * 게시판 페이지 번호
	 */
	private int searchPage;
	
	/**
	 * 한 페이지당 글 수
	 */
	private int searchPerPage;
	
	/**
	 * searchPage를 증가 시킬지 여부 결정
	 */
	private boolean increasingSearchPage = true;
	
	public ArticleSearch(){};
	
	public ArticleSearch(String cafeId, String boardId, int searchPage) {
		this.cafeId = cafeId;
		this.boardId = boardId;
		this.searchPage = searchPage;
		this.searchPerPage = DEFAULT_SEARCH_PER_PAGE; 
	}
	
	public ArticleSearch(String cafeId, String boardId, int searchPage, int searchPerPage) {
		this.cafeId = cafeId;
		this.boardId = boardId;
		this.searchPage = searchPage;
		this.searchPerPage = searchPerPage; 
	}
	
	public ArticleSearch(int clubIndex, int searchPage, int searchPerPage) {
		setCafeCodeAndBoardIdForNotice(clubIndex);
		this.searchPage = searchPage;
		this.searchPerPage = searchPerPage; 
	}
	
	public ArticleSearch(int clubIndex, int searchPage) {
		setCafeCodeAndBoardIdForNotice(clubIndex);
		this.searchPage = searchPage;
		this.searchPerPage = DEFAULT_SEARCH_PER_PAGE; 
	}

	public String getCafeId() {
		return cafeId;
	}

	public void setCafeId(String cafeId) {
		this.cafeId = cafeId;
	}

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public int getSearchPage() {
		return searchPage;
	}

	public void setSearchPage(int searchPage) {
		this.searchPage = searchPage;
	}

	public int getSearchPerPage() {
		return searchPerPage;
	}

	public void setSearchPerPage(int searchPerPage) {
		this.searchPerPage = searchPerPage;
	}
	
	public boolean isIncreasingSearchPage() {
		return increasingSearchPage;
	}

	public void setIncreasingSearchPage(boolean increasingSearchPage) {
		this.increasingSearchPage = increasingSearchPage;
	}

	/**
	 * 공지 게시판 조회를 위해 cafeCode와 boardId를 
	 * NaverArticleParser.CLUB_SWING_ADE와 같은 값을 사용해 설정한다.
	 * @param clubIndex
	 */
	public void setCafeCodeAndBoardIdForNotice(int clubIndex) { 
		this.cafeId = DaumArticleParser.getCafeId(clubIndex);
		this.boardId = DaumArticleParser.getNoticeBoardId(clubIndex);
	}
	
}