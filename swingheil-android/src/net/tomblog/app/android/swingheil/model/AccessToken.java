package net.tomblog.app.android.swingheil.model;

/**
 * OAuth 인증을 받은 Token 정보
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class AccessToken {
	
	private String accessToken;
	private String tokenSecret;
	
	public AccessToken(String accessToken, String tokenSecret) {
		this.accessToken = accessToken;
		this.tokenSecret = tokenSecret;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenSecret() {
		return tokenSecret;
	}

	public void setTokenSecret(String tokenSecret) {
		this.tokenSecret = tokenSecret;
	}

	@Override
	public String toString() {
		return "AccessToken [accessToken=" + accessToken + ", tokenSecret="
				+ tokenSecret + "]";
	}
}