package net.tomblog.app.android.swingheil.model.server;

import java.util.List;

import net.tomblog.app.android.swingheil.util.Util;

import com.swingheil.domain.group.Club;

public class ResultClubs {
	//
	private String version;
	
	private List<Club> clubs;

	public String getVersion() {
		return version; 
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<Club> getClubs() {
		return clubs;
	}

	public void setClubs(List<Club> clubs) {
		this.clubs = clubs;
	}

	@Override
	public String toString() {
		return Util.toString(this);
	}
}
