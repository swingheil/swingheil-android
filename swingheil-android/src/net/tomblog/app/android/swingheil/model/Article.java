package net.tomblog.app.android.swingheil.model;

import java.util.Date;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * 게시판 글 정보
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class Article implements BaseColumns{
	
    public static final String AUTHORITY = "net.tomblog.provider.Articles";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/articles");
	
    public static final String BOARD_ID = "board_id";
    public static final String ARTICLE_ID = "article_id";
    public static final String ARTICLE_TITLE = "article_title";
    public static final String NICKNAME = "nickname";
    public static final String WRITE_DATE = "write_date";
    public static final String COMMENT_COUNT = "comment_count";
    public static final String READ_COUNT = "read_count";
    public static final String CAFE_ID = "cafe_id";
    public static final String HIDDEN = "hidden";
    public static final String NEW_ARTICLE = "new_article";
	public static final String DEFAULT_SORT_ORDER = "write_date desc";
    
	public static String[] PROJECTION = new String[] { Article._ID,
		Article.BOARD_ID, Article.ARTICLE_ID, Article.ARTICLE_TITLE,
		Article.NICKNAME, Article.WRITE_DATE, Article.COMMENT_COUNT,
		Article.READ_COUNT, Article.CAFE_ID, Article.HIDDEN,
		Article.NEW_ARTICLE };
	public static String SELECTION = Article.CAFE_ID + "=? AND " + Article.BOARD_ID + "=?";
	
	/**
	 * 게시판 아이디
	 */
	private String boardId;
	
	/**
	 * 게시글 아이디
	 */
	private int articleId;
	
	/**
	 * 게시글 제목
	 */
	private String articleTitle;
	
	/**
	 * 작성자 nickname
	 */
	private String nickname;
	
	/**
	 * 글 작성일자 
	 */
	private Date writeDate;
	
	/**
	 * 댓글 개수
	 */
	private int commentCount;

	/**
	 * 조회수
	 */
	private int readCount;
	
	/**
	 * 비밀글 여부 (한줄 메모장에 한함), Daum에서 사용
	 */
	private boolean hidden;
	
	/**
	 * 새로운 글 여부, Naver에서 사용
	 */
	private boolean newArticle;

	/**
	 * 까페 아이디
	 */
	private String cafeId;

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public int getArticleId() {
		return articleId;
	}

	public void setArticleId(int articleId) {
		this.articleId = articleId;
	}

	public String getArticleTitle() {
		return articleTitle;
	}

	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public int getReadCount() {
		return readCount;
	}

	public void setReadCount(int readCount) {
		this.readCount = readCount;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public String getCafeId() {
		return cafeId;
	}

	public void setCafeId(String cafeId) {
		this.cafeId = cafeId;
	}

	public boolean isNewArticle() {
		return newArticle;
	}

	public void setNewArticle(boolean newArticle) {
		this.newArticle = newArticle;
	}

	@Override
	public String toString() {
		return "Article [articleId=" + articleId + ", articleTitle="
				+ articleTitle + ", boardId=" + boardId + ", cafeId=" + cafeId
				+ ", commentCount=" + commentCount + ", hidden=" + hidden
				+ ", newArticle=" + newArticle + ", nickname=" + nickname
				+ ", readCount=" + readCount + ", writeDate=" + writeDate + "]";
	}
}