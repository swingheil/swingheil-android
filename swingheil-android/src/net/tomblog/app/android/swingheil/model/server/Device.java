package net.tomblog.app.android.swingheil.model.server;

import net.tomblog.app.android.swingheil.util.Util;

public class Device {
	//
	private String nickname; 
	private String deviceUid;
	private String userId;
	private String modelId;
	private String osVersion;
	private String registrationId;
	
	//--------------------------------------------------------------------------
	// constructor
	
	public Device() {
		//
	}
	
	public Device(String deviceUid, String userId) {
		//
		this.deviceUid = deviceUid;
		this.userId = userId;
	}
	
	public Device(String deviceUid, String userId, String modelId, String osVersion) {
		this.deviceUid = deviceUid;
		this.userId = userId;
		this.modelId = modelId;
		this.osVersion = osVersion;
	}

	public Device(String deviceUid, String userId, String modelId, String osVersion, String registrationId) {
		this.deviceUid = deviceUid;
		this.userId = userId;
		this.modelId = modelId;
		this.osVersion = osVersion;
		this.registrationId = registrationId;
	}
	
	public Device(String nickname, String deviceUid, String userId, String modelId, String osVersion, String registrationId) {
		this.nickname = nickname;
		this.deviceUid = deviceUid;
		this.userId = userId;
		this.modelId = modelId;
		this.osVersion = osVersion;
		this.registrationId = registrationId;
	}
	
	//--------------------------------------------------------------------------
	// getter/setter


	public String getDeviceUid() {
		return deviceUid;
	}

	public void setDeviceUid(String deviceUid) {
		this.deviceUid = deviceUid;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	
	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	@Override
	public String toString() {
		return Util.toString(this);
	}
}
