package net.tomblog.app.android.swingheil.fragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.activity.EventDetailActivity;
import net.tomblog.app.android.swingheil.adapter.SeparatedListAdapter;
import net.tomblog.app.android.swingheil.parser.DownloadImagesTask;
import net.tomblog.app.android.swingheil.parser.RefreshAdapter;
import net.tomblog.app.android.swingheil.parser.RefreshEventList;
import net.tomblog.app.android.swingheil.parser.server.EventListParser;
import net.tomblog.app.android.swingheil.parser.server.FavoriteParser;
import net.tomblog.app.android.swingheil.util.ClubAndBarHolder;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.EventComparator;
import net.tomblog.app.android.swingheil.util.EventHolder;
import net.tomblog.app.android.swingheil.util.L;
import net.tomblog.app.android.swingheil.util.ThumbnailHolder;
import net.tomblog.app.android.swingheil.util.Util;
import net.tomblog.app.android.swingheil.view.ActionItem;
import net.tomblog.app.android.swingheil.view.QuickAction;
import net.tomblog.app.android.swingheil.view.SwipyRefreshLayout;
import net.tomblog.app.android.swingheil.view.SwipyRefreshLayoutDirection;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.swingheil.domain.event.Event;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.group.Group;
import com.swingheil.domain.type.enums.EventCategory;

public class EventFragment extends Fragment implements RefreshAdapter, RefreshEventList {

	private static final String TAG = EventFragment.class.getName(); 

	private static final String KEY_TITLE = "title";
	private static final String KEY_INDICATOR_COLOR = "indicator_color";
	private static final String KEY_DIVIDER_COLOR = "divider_color";
	
	/**
	 * 이 Fragment가 처음 호출되었는지 여부
	 */
	private boolean mIsFirtLaunch = true;
	private Event mSelectedEvent = null;
	private int mPage = 1;

	private Context mContext = null;
	private RefreshAdapter mRefreshList= null;
	private ListView mEventListView = null;
	private SwipyRefreshLayoutDirection mRefreshDirection = null; 
	private SwipyRefreshLayout mSwipeRefreshLayout;
	
	private SeparatedListAdapter mAdapter = null;
	private EventListAdapter mSwingHeilAdapter = null;
	private EventListAdapter mClubAndBarAdapter = null;

	private Map<String, Event> mEventMap = null;
	private static ArrayList<Event> mSwingHeilList = null;
	private static ArrayList<Event> mClubAndBarList = null;

	public final static String ITEM_TITLE = "title";
	public final static String ITEM_CAPTION = "caption";

	public Map<String, String> createItem(String title, String caption) {
		Map<String, String> item = new HashMap<String, String>();
		item.put(ITEM_TITLE, title);
		item.put(ITEM_CAPTION, caption);
		return item;
	}

	/**
	 * @return a new instance of {@link EventFragment}, adding the parameters
	 *         into a bundle and setting them as arguments.
	 */
	public static EventFragment newInstance(CharSequence title,
			int indicatorColor, int dividerColor) {
		Bundle bundle = new Bundle();
		bundle.putCharSequence(KEY_TITLE, title);
		bundle.putInt(KEY_INDICATOR_COLOR, indicatorColor);
		bundle.putInt(KEY_DIVIDER_COLOR, dividerColor);

		EventFragment fragment = new EventFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		L.d(TAG, " >>> called onCreate()");
		
		mContext = getActivity().getApplicationContext();
		
		mSwingHeilList = new ArrayList<Event>();
		Event event = new Event();
		event.setTitle("titl2");
		mSwingHeilList.add(event);
		
		mClubAndBarList = new ArrayList<Event>();
		event = new Event();
		event.setTitle("titl3");
		mClubAndBarList.add(event);
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		L.d(TAG, " >>> called onCreateView()");
		return inflater.inflate(R.layout.pager_item_event, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		L.d(TAG, " >>> called onViewCreated() mIsFirtLaunch : " + mIsFirtLaunch);

		inflateView(view);

		initObject();

		if(mIsFirtLaunch) {
			mIsFirtLaunch = false;
			
			mSwingHeilList.clear();
			mClubAndBarList.clear();
			refreshAdapter();
			
			mEventMap = new HashMap<String, Event>();
			
			// 스윙하일의 이벤트를 조회한다.
			new EventListParser(mContext, this, Constants.SWING_HEIL, "3", mPage).execute();
			
			// 즐겨찾기한 동호회와 스윙바의 이벤트를 조회한다.
			new EventListParser(mContext, this, Constants.SWING_ALL, mPage).execute();
		}
	}

	private void inflateView(View view) {
		L.d(TAG, " >>> called inflateView()");

		mEventListView = (ListView) view.findViewById(R.id.event_list);
		mSwipeRefreshLayout = (SwipyRefreshLayout)view.findViewById(R.id.swipe_refresh_layout); 
	}

	private void initObject() {
		L.d(TAG, " >>> called initObject()");
		
		mRefreshList = this;

		final QuickAction mQuickAction = new QuickAction(mContext);
		ActionItem addItem = new ActionItem(QuickAction.DETAIL, getResources().getString(R.string.quick_action_detail), 
				getResources().getDrawable(R.drawable.popup_menu_icon_detail));
		ActionItem acceptItem = new ActionItem(QuickAction.FAVORITE, getResources().getString(R.string.quick_action_favorite), 
				getResources().getDrawable(R.drawable.popup_menu_icon_favorite));
		ActionItem uploadItem = new ActionItem(QuickAction.UNFAVORITE, getResources().getString(R.string.quick_action_unfavorite), 
				getResources().getDrawable(R.drawable.popup_menu_icon_unfavorite)); 
		mQuickAction.addActionItem(addItem);
		mQuickAction.addActionItem(acceptItem);
		mQuickAction.addActionItem(uploadItem);
		mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction quickAction, int pos, int actionId) {
				if (actionId == QuickAction.DETAIL) { 
					Intent intent = new Intent(getActivity(), EventDetailActivity.class);
					intent.putExtra(Constants.EVENT, mSelectedEvent); 
					getParentFragment().startActivityForResult(intent, Constants.REQUEST_EVENT_DETAIL);
				} else if(mSelectedEvent != null && mSelectedEvent instanceof Event){
					if(actionId == QuickAction.FAVORITE){
						addFavorite(mSelectedEvent);
					} else if(actionId == QuickAction.UNFAVORITE){
						removeFavorite(mSelectedEvent);
					}
				} 
			}
		});
		mQuickAction.setOnDismissListener(new PopupWindow.OnDismissListener() {
			@Override
			public void onDismiss() {
			}
		});

		mSwingHeilAdapter = new EventListAdapter(mContext, mSwingHeilList);
		mClubAndBarAdapter = new EventListAdapter(mContext, mClubAndBarList);

		// create our list and custom adapter
		mAdapter = new SeparatedListAdapter(mContext);
		mAdapter.addSection(R.string.list_header_swingheil, mSwingHeilAdapter);
		mAdapter.addSection(R.string.list_header_club_and_bar, mClubAndBarAdapter);

		mEventListView.setAdapter(mAdapter);
		mEventListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				L.d(TAG, "onItemClick() is called position : " + position + " id : " + id);
				Event event = (Event)view.getTag(R.id.tag_event);
				if(event != null) {
					L.d(TAG, "event title : " + event.getTitle());
					Intent intent = new Intent(getActivity(), EventDetailActivity.class);
					intent.putExtra(Constants.EVENT, event); 
					startActivityForResult(intent, Constants.REQUEST_EVENT_DETAIL); 
				}
			}
		});
		mEventListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				L.d(TAG, "onItemLongClick() is called position : " + position + " id : " + id);
				mSelectedEvent = (Event)view.getTag(R.id.tag_event);
				
				mQuickAction.show(view);
				 
				return true;
			}
		});

		mSwipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                L.d("MainActivity", "Refresh triggered at " + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
                refreshContent(direction);
                mSwipeRefreshLayout.setRefreshing(false);
            }
		});
		mSwipeRefreshLayout.setColorSchemeResources(R.color.blue, R.color.green, R.color.orange, R.color.red);
		
		refreshAdapter();
	}
	
	private void refreshContent(SwipyRefreshLayoutDirection direction) {
		if (direction == SwipyRefreshLayoutDirection.TOP) {
			// 스윙하일의 이벤트를 조회한다.
			mRefreshDirection = SwipyRefreshLayoutDirection.TOP;
			mPage = 1;
			String limit = "3";
			new EventListParser(mContext, this, Constants.SWING_HEIL, limit, mPage).execute();
			new EventListParser(mContext, this, Constants.SWING_ALL, mPage).execute();
		}else {
			// 즐겨찾기한 동호회와 스윙바의 이벤트를 조회한다.
			mRefreshDirection = SwipyRefreshLayoutDirection.BOTTOM;
			new EventListParser(mContext, this, Constants.SWING_ALL, mPage).execute();
		}
	}

	@Override
	public void onResume() {
		L.d(TAG, " >>> called onResume()");
		refreshAdapter();
		super.onResume();
	}
	
	@Override
	public void onStart() {
		L.d(TAG, " >>> called onStart()");
		super.onStart();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data); 
		L.d(TAG, " >>> called onActivityResult() requestCode : " + requestCode + ", resultCode : " + resultCode);
		
		if(requestCode == Constants.REQUEST_EVENT_DETAIL && resultCode== Activity.RESULT_OK){
			Event event = (Event) data.getSerializableExtra(Constants.EVENT);
	    	if(event != null) {
	    		if(event.isFavorite()) { 
	    			addFavorite(event);
	    		}else {
	    			removeFavorite(event);
	    		}
	    	}
		}
	}

	public class EventListAdapter extends BaseAdapter {

		private LayoutInflater inflater = null;
		private List<Event> mEventList = null;
		private final int margin0 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
		private final int margin15 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
		
		public EventListAdapter(Context context, List<Event> eventList) {
			inflater = LayoutInflater.from(context);
			mEventList = eventList;
		}

		public List<Event> getData() {
			return mEventList;
		}
		
		@Override
		public int getCount() {
			return mEventList.size();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.list_item_event, parent, false);
				convertView.setTag(holder);
				
				holder.eventImage = (ImageView) convertView.findViewById(R.id.event_image);
				holder.title = (TextView) convertView.findViewById(R.id.title);
				holder.newIcon = (ImageView) convertView.findViewById(R.id.icon_new);
				holder.host = (TextView) convertView.findViewById(R.id.host);
				holder.hostIcon = (ImageView) convertView.findViewById(R.id.icon_host);
				holder.description = (TextView) convertView.findViewById(R.id.description);
				holder.date = (TextView) convertView.findViewById(R.id.date);
				holder.favoriteIcon = (ImageView) convertView.findViewById(R.id.icon_favorite);
				holder.favorite = (View) convertView.findViewById(R.id.favorite);
				holder.favorite.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						View parentView = (View)view.getParent();
						Event event = (Event)parentView.getTag(R.id.tag_event); 
						if(event != null) {
							L.d(TAG, "event id : " + event.getId() + " , title : " + event.getTitle());
							if(event.isFavorite()) {
								removeFavorite(event);
								Toast.makeText(view.getContext(), R.string.do_unfavorite, Toast.LENGTH_LONG).show();
							}else {
								Toast.makeText(view.getContext(), R.string.do_favorite, Toast.LENGTH_LONG).show(); 
								addFavorite(event); 
							}
						}
					}
				});
				
			} else {
				holder = (ViewHolder)convertView.getTag();
			}

			Event event = mEventList.get(position); 
			if(event != null && holder.eventImage != null) {
				convertView.setTag(R.id.tag_event, event); 
				
				String url = event.getThumbnailUrl();
				
				ThumbnailHolder thumbnailHolder = ThumbnailHolder.getInstance();
				Bitmap thumbnail = thumbnailHolder.retrieve(url);
				
				if(thumbnail != null) { 
					holder.eventImage.setBackgroundColor(0);
					holder.eventImage.setImageBitmap(thumbnail);
				}else {
					if(!thumbnailHolder.isInTask(url)) {
						DownloadImagesTask task = new DownloadImagesTask(mRefreshList);
						thumbnailHolder.setTask(url, task);
						task.execute(url);
					}  
				}
				
				EventCategory eventCategory = EventCategory.findBy(event.getCategory().getCode());
				Group sponsor = event.getSponsor();
				String host = "";
				if(sponsor != null) {
					String id = sponsor.getId();
					if(id != null && !"".equals(id)) {
						Bar bar = ClubAndBarHolder.getInstance().retrieveBar(id);
						if(bar != null) {
							host = bar.getNameEng();
							if(Util.isEmpty(host)) {
								host = bar.getNameKor();
							}
						}
						Club club = ClubAndBarHolder.getInstance().retrieveClub(id);
						if(club != null) {
							host = club.getNameEng();
							if(Util.isEmpty(host)) {
								host = club.getNameKor();
							}
						}
					}
				}
				if (EventCategory.SwingHeil == eventCategory) {
					host = getString(R.string.str_swingheil);
					holder.hostIcon.setBackgroundResource(R.drawable.list_item_img_swingheil);
				}else if (EventCategory.SwingBar == eventCategory) {
					holder.hostIcon.setBackgroundResource(R.drawable.list_item_img_bar);
				}else {
					holder.hostIcon.setBackgroundResource(R.drawable.list_item_img_club);
				}
				holder.host.setText(host);

				if(EventHolder.getInstance().isFavorite(event.getId())) { 
					holder.favoriteIcon.setImageResource(R.drawable.icon_favorite_fill);
				}else {
					holder.favoriteIcon.setImageResource(R.drawable.icon_unfavorite);
				}
				
				holder.title.setText(event.getTitle()); 
				LinearLayout.LayoutParams lp = (android.widget.LinearLayout.LayoutParams) holder.title.getLayoutParams();
				if(event.isNew()) {
					lp.leftMargin = margin15;
					holder.newIcon.setVisibility(View.VISIBLE);
				}else {
					lp.leftMargin = margin0;
					holder.newIcon.setVisibility(View.GONE); 
				}  
				
				holder.description.setText(event.getSummary());
				if(event.getPeriod() != null) {
					holder.date.setText(event.getPeriod().getFrom().getDate()); 
				}
			}

			return convertView; 
		}
	}
	
	public static class ViewHolder {
		public ImageView eventImage;
		public TextView title;
		public ImageView newIcon;
		public View favorite;
		public ImageView favoriteIcon;
		public ImageView hostIcon;
		public TextView host;
		public TextView description;
		public TextView date;
	}
	
	@Override
	public void refresh(List<Event> eventList) { 
		L.d(TAG, " >>> called refresh()");
		if (eventList != null) {
			
			List<Event> swingheilEventList = mSwingHeilAdapter.getData();
			List<Event> clubAndBarEventList = mClubAndBarAdapter.getData();
			int lastEventNum = swingheilEventList.size() + clubAndBarEventList.size();
			boolean isSwingHeilEvent = false;
			
			for (Event event : eventList) {
				String code = event.getCategory().getCode();
				EventCategory eventCategory = EventCategory.findBy(code);
				if(EventCategory.SwingHeil == eventCategory) {
					isSwingHeilEvent = true;
				}
			}
			
			if (mRefreshDirection == SwipyRefreshLayoutDirection.TOP) {
				if(isSwingHeilEvent) {
					clearEventListFromMap(swingheilEventList);
					swingheilEventList.clear();
				} else {
					clearEventListFromMap(clubAndBarEventList);
					clubAndBarEventList.clear();
				}
			}
			
			for (Event event : eventList) {
				if (!mEventMap.containsKey(event.getId())) {
					mEventMap.put(event.getId(), event);
					L.d(TAG, event.getId() + event.getTitle() + " was added.");
					
					if(isSwingHeilEvent) {
						swingheilEventList.add(event);
					}else {
						clubAndBarEventList.add(event);
					}
				}
			}
			
			if (eventList.size() != 0) {
				Collections.sort(swingheilEventList, new EventComparator());
				Collections.sort(clubAndBarEventList, new EventComparator());
				refreshAdapter();
				if (mRefreshDirection == null || mRefreshDirection == SwipyRefreshLayoutDirection.TOP) {
					// 처음 진입시 자동로딩 
					if(!isSwingHeilEvent) {
						mPage++;
					}
				}else if (mRefreshDirection == SwipyRefreshLayoutDirection.BOTTOM) {
					// 화면의 아래에서 Pull Up 로딩
					if(!isSwingHeilEvent) {
						mPage++;
						mEventListView.setSelection(lastEventNum + 1);
					}
				} // 화면의 상단에서 Pull Down 로딩 
			}
		}
		
	}

	private void clearEventListFromMap(List<Event> eventList) {
		for (Event event : eventList) {
			mEventMap.remove(event.getId());
		}
	}

	@Override
	public void refreshAdapter() { 
		if (mSwingHeilAdapter != null) {
			mSwingHeilAdapter.notifyDataSetChanged();
		}
		if (mClubAndBarAdapter != null) {
			mClubAndBarAdapter.notifyDataSetChanged();
		}
		if (mAdapter != null) {
			mAdapter.notifyDataSetChanged();
		}
	}
	
	private void addFavorite(Event event) {
		if(event == null) {
			L.d(TAG, "addFavorite() : event is null.");
			return;
		}
		
		String id = event.getId();
		boolean isNew = event.isNew();
		EventCategory eventCategory = EventCategory.findBy(event.getCategory().getCode());
		
		if(EventCategory.SwingHeil == eventCategory) {
			for (Event aEvent : mSwingHeilList) {
				if(id.equals(aEvent.getId())) {
					aEvent.setFavorite(true);
					aEvent.setNew(isNew);
				}
			}
		}else {
			for (Event aEvent : mClubAndBarList) {
				if(id.equals(aEvent.getId())) {
					aEvent.setFavorite(true);
					aEvent.setNew(isNew);
				}
			}
		}

		EventHolder.getInstance().saveEventFavorite(id, true);
		
		new FavoriteParser(Constants.EVENT, Constants.ACTION_FOLLOW, id).execute();
		
		refreshAdapter();
	}
	
	private void removeFavorite(Event event) {
		if(event == null) {
			L.d(TAG, "removeFavorite() : event is null.");
			return;
		}
		
		String id = event.getId();
		boolean isNew = event.isNew();
		EventCategory eventCategory = EventCategory.findBy(event.getCategory().getCode());
		
		if(EventCategory.SwingHeil == eventCategory) {
			for (Event aEvent : mSwingHeilList) {
				if(id.equals(aEvent.getId())) {
					aEvent.setFavorite(false);
					aEvent.setNew(isNew);
				}
			}
		}else {
			for (Event aEvent : mClubAndBarList) {
				if(id.equals(aEvent.getId())) {
					aEvent.setFavorite(false); 
					aEvent.setNew(isNew);
				}
			}
		}
		
		EventHolder.getInstance().saveEventFavorite(id, false);
		
		new FavoriteParser(Constants.EVENT, Constants.ACTION_UNFOLLOW, id).execute();
			
		refreshAdapter();
	}
}