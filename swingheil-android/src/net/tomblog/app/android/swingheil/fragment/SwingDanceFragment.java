package net.tomblog.app.android.swingheil.fragment;

import java.util.ArrayList;
import java.util.List;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.util.L;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Simple Fragment used to display some meaningful content for each page in the sample's
 * {@link android.support.v4.view.ViewPager}.
 */
public class SwingDanceFragment extends Fragment {

	private static final String TAG = SwingDanceFragment.class.getName();
	
    private static final String KEY_TITLE = "title";
    private static final String KEY_INDICATOR_COLOR = "indicator_color";
    private static final String KEY_DIVIDER_COLOR = "divider_color";

	private Context mContext;

	private ExpandableListAdapter mAdapter;
	private ExpandableListView mListView;
	private ArrayList<FAQ> mFaqList = null;
	
    /**
     * @return a new instance of {@link SwingDanceFragment}, adding the parameters into a bundle and
     * setting them as arguments.
     */
    public static SwingDanceFragment newInstance(CharSequence title, int indicatorColor, int dividerColor) {
        Bundle bundle = new Bundle();  
        bundle.putCharSequence(KEY_TITLE, title);
        bundle.putInt(KEY_INDICATOR_COLOR, indicatorColor);
        bundle.putInt(KEY_DIVIDER_COLOR, dividerColor);

        SwingDanceFragment fragment = new SwingDanceFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pager_item_swing_dance, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        L.d(TAG, " >>> called onViewCreated()");
		mContext = view.getContext();
		
		inflateView(view);

		initObject();

    }

	private void inflateView(View view) {
		L.d(TAG, " >>> called inflateView()");

		mListView = (ExpandableListView) view.findViewById(R.id.list); 
	}
	
	private void initObject() {
		L.d(TAG, " >>> called initObject()");
		
		mFaqList = new ArrayList<FAQ>();
		
		String[] questions = getResources().getStringArray(R.array.questions);
		String[] answers = getResources().getStringArray(R.array.answers);
		
		if(questions != null && answers != null && questions.length == answers.length) {
			for (int i=0; i<questions.length; i++) {
				FAQ faq = new FAQ();
				faq.setQuestion(questions[i]);
				faq.setAnswer(answers[i]);
				mFaqList.add(faq);
			}
		}
		
		mAdapter = new MyExpandableListAdapter(mContext, mFaqList);
		mListView.setAdapter(mAdapter);
	}
	
	 /**
     * A simple adapter which maintains an ArrayList of photo resource Ids. 
     * Each photo is displayed as an image. This adapter supports clearing the
     * list of photos and adding a new photo.
     *
     */
    public class MyExpandableListAdapter extends BaseExpandableListAdapter {
    	
    	private LayoutInflater inflater = null;
		private List<FAQ> mFaqList = null;
		
		public MyExpandableListAdapter(Context context, List<FAQ> faqList) {
			inflater = LayoutInflater.from(context);
			mFaqList = faqList;
		}
    	
        public Object getChild(int groupPosition, int childPosition) {
            return mFaqList.get(groupPosition).getAnswer();
        }

        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        public int getChildrenCount(int groupPosition) {
            return 1;
        }

        public Object getGroup(int groupPosition) {
            return mFaqList.get(groupPosition);
        }

        public int getGroupCount() {
            return mFaqList.size();
        }

        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        	ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.list_item_question, parent, false);
				convertView.setTag(holder); 
				
				holder.questionImage = (ImageView) convertView.findViewById(R.id.group_indicator);
				holder.questionText = (TextView) convertView.findViewById(R.id.question_text);
			} else {
				holder = (ViewHolder)convertView.getTag();
			}
			
			if(isExpanded) {
				holder.questionImage.setImageResource(R.drawable.expanded);
			}else {
				holder.questionImage.setImageResource(R.drawable.collapsed);
			}
			
			if(mFaqList != null) {
				FAQ faq = mFaqList.get(groupPosition);
				holder.questionText.setText(faq.getQuestion());
			}
			
			return convertView;
        }

        public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                View convertView, ViewGroup parent) {
        	ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.list_item_answer, parent, false);
				convertView.setTag(holder);
				
				holder.answerText = (TextView) convertView.findViewById(R.id.answer_text);
			} else {
				holder = (ViewHolder)convertView.getTag();
			}
			
			if(mFaqList != null) {
				FAQ faq = mFaqList.get(groupPosition);
				holder.answerText.setText(faq.getAnswer());
			}
			
			return convertView;
        }
        
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        public boolean hasStableIds() {
            return true;
        }

    }

	public static class ViewHolder {
		public ImageView questionImage;
		public TextView questionText;
		public TextView answerText;
	}
	
	public class FAQ {
		private String question;
		private String answer;
		
		public String getQuestion() {
			return question;
		}
		public void setQuestion(String question) {
			this.question = question;
		}
		public String getAnswer() {
			return answer;
		}
		public void setAnswer(String answer) {
			this.answer = answer;
		}
	}
}
