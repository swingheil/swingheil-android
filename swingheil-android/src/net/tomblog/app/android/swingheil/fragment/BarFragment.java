package net.tomblog.app.android.swingheil.fragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.activity.BarDetailActivity;
import net.tomblog.app.android.swingheil.adapter.SeparatedListAdapter;
import net.tomblog.app.android.swingheil.parser.DownloadImagesTask;
import net.tomblog.app.android.swingheil.parser.RefreshAdapter;
import net.tomblog.app.android.swingheil.parser.RefreshBarList;
import net.tomblog.app.android.swingheil.parser.server.BarListParser;
import net.tomblog.app.android.swingheil.parser.server.FavoriteParser;
import net.tomblog.app.android.swingheil.util.BarComparator;
import net.tomblog.app.android.swingheil.util.ClubAndBarHolder;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.ImageResource;
import net.tomblog.app.android.swingheil.util.L;
import net.tomblog.app.android.swingheil.util.ThumbnailHolder;
import net.tomblog.app.android.swingheil.util.Util;
import net.tomblog.app.android.swingheil.view.ActionItem;
import net.tomblog.app.android.swingheil.view.QuickAction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.swingheil.domain.group.Bar;
import com.swingheil.domain.type.place.Station;
import com.swingheil.domain.type.place.Subway;
import com.swingheil.domain.type.place.Subway.Line;

public class BarFragment extends Fragment implements RefreshAdapter, RefreshBarList {

	private static final String TAG = BarFragment.class.getName();
	
    private static final String KEY_TITLE = "title";
    private static final String KEY_INDICATOR_COLOR = "indicator_color";
    private static final String KEY_DIVIDER_COLOR = "divider_color";

	private Context mContext;
	private ListView mBarListView;
	
	private SeparatedListAdapter mAdapter = null;
	private BaseAdapter mFavoriteAdapter = null;
	private BaseAdapter mBarAdapter = null;
	private RefreshAdapter mRefreshList= null;
	
	private ArrayList<Bar> mFavoriteList = null;
	private ArrayList<Bar> mBarList = null;
	private static String mLastSelectedId = "";
	
    /**
     * @return a new instance of {@link BarFragment}, adding the parameters into a bundle and
     * setting them as arguments.
     */
    public static BarFragment newInstance(CharSequence title, int indicatorColor,
            int dividerColor) {
        Bundle bundle = new Bundle();
        bundle.putCharSequence(KEY_TITLE, title);
        bundle.putInt(KEY_INDICATOR_COLOR, indicatorColor);
        bundle.putInt(KEY_DIVIDER_COLOR, dividerColor);

        BarFragment fragment = new BarFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
       	L.d(TAG, " >>> called onCreateView()");
       	
        return inflater.inflate(R.layout.pager_item_bar, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
		L.d(TAG, " >>> called onViewCreated()");
		mContext = view.getContext();
		
		inflateView(view);

		initObject();
    }
    
	private void inflateView(View view) {
		L.d(TAG, " >>> called inflateView()");

		mBarListView = (ListView) view.findViewById(R.id.bar_list);
	}
	
	private void initObject() {
		L.d(TAG, " >>> called initObject()");
		
		mRefreshList = this;
		
		if(mFavoriteList == null) {
			mFavoriteList = new ArrayList<Bar>();
		}
		if(mBarList == null) {
			mBarList = new ArrayList<Bar>();
		}
		
		List<Bar> barList = ClubAndBarHolder.getInstance().retrieveBar();
		if(barList != null && mFavoriteList.size() == 0 && mBarList.size() == 0) {
			for (Bar bar : barList) { 
				if (bar.isFavorite()) {
					mFavoriteList.add(bar);
				}else {
					mBarList.add(bar);
				}
			}
		}
		
		final QuickAction mQuickAction = new QuickAction(mContext);
		ActionItem addItem = new ActionItem(QuickAction.DETAIL, getResources().getString(R.string.quick_action_detail), 
				getResources().getDrawable(R.drawable.popup_menu_icon_detail));
		ActionItem acceptItem = new ActionItem(QuickAction.FAVORITE, getResources().getString(R.string.quick_action_favorite), 
				getResources().getDrawable(R.drawable.popup_menu_icon_favorite));
		ActionItem uploadItem = new ActionItem(QuickAction.UNFAVORITE, getResources().getString(R.string.quick_action_unfavorite), 
				getResources().getDrawable(R.drawable.popup_menu_icon_unfavorite)); 
		mQuickAction.addActionItem(addItem);
		mQuickAction.addActionItem(acceptItem);
		mQuickAction.addActionItem(uploadItem);
		mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction quickAction, int pos, int actionId) {
				if (actionId == QuickAction.DETAIL) { 
					Intent intent = new Intent(getActivity(), BarDetailActivity.class);
					intent.putExtra(Constants.BAR_ID, mLastSelectedId);
					startActivity(intent);
				} else if(mLastSelectedId != null && !"".equalsIgnoreCase(mLastSelectedId)) {
					if(actionId == QuickAction.FAVORITE){ 
						addFavorite(getBar(mLastSelectedId)); 
					} else if(actionId == QuickAction.UNFAVORITE){
						removeFavorite(getBar(mLastSelectedId));
					}
				} 
			}
		});
		mQuickAction.setOnDismissListener(new PopupWindow.OnDismissListener() {
			@Override
			public void onDismiss() {
			}
		});
		
		mFavoriteAdapter = new BarListAdapter(mContext, mFavoriteList);
		mBarAdapter = new BarListAdapter(mContext, mBarList);

		// create our list and custom adapter
		mAdapter = new SeparatedListAdapter(mContext);
		mAdapter.addSection(R.string.list_header_favorite, mFavoriteAdapter);
		mAdapter.addSection(R.string.list_header_bar, mBarAdapter);

		mBarListView.setAdapter(mAdapter);
		mBarListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				L.d(TAG, "onItemClick() is called position : " + position + " id : " + id); 
				Bar bar = (Bar)view.getTag(R.id.tag_bar);
				if(bar != null) {
					L.d(TAG, "bar name : " + bar.getNameKor());
					mLastSelectedId = bar.getId();
					Intent intent = new Intent(getActivity(), BarDetailActivity.class);
					intent.putExtra(Constants.BAR_ID, mLastSelectedId); 
					startActivity(intent);
				}
			}
		});
		mBarListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				L.d(TAG, "onItemLongClick() is called position : " + position + " id : " + id);
				Bar bar = (Bar)view.getTag(R.id.tag_bar);
				if(bar != null) {
					L.d(TAG, "bar name : " + bar.getNameKor());
					mLastSelectedId = bar.getId(); 
					
					mQuickAction.show(view); 
				}
				return true;
			}
		});
		
		refreshAdapter();
		
		new BarListParser(mContext, this).execute();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		L.d(TAG, "onResume() is called"); 
    	ClubAndBarHolder holder = ClubAndBarHolder.getInstance();
    	Bar bar = holder.retrieveBar(mLastSelectedId);
    	if(bar != null) {
    		L.d(TAG, "club name : " + bar.getNameKor()); 
    		if(bar.isFavorite()) { 
    			addFavorite(bar); 
    		}else { 
    			removeFavorite(bar);
    		}
    	} else { 
    		L.e(TAG, "last selected bar is null"); 
    	}
	}
	
	private class BarListAdapter extends BaseAdapter {

		private LayoutInflater inflater = null;
		private List<Bar> mBarList = null;
		
		private final int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
		private final int height = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
		private final int margin0 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
		private final int margin3 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getResources().getDisplayMetrics());
		private final int margin5 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics());
		private final int margin15 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());

		public BarListAdapter(Context context, List<Bar> barList) {
			inflater = LayoutInflater.from(context);
			mBarList = barList;
		}

		@Override
		public int getCount() {
			return mBarList.size();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.list_item_bar, parent, false);
				convertView.setTag(holder);
				
				holder.barImage = (ImageView) convertView.findViewById(R.id.bar_image);
				holder.nameKor = (TextView) convertView.findViewById(R.id.nameKor);
				holder.nameEng = (TextView) convertView.findViewById(R.id.nameEng);
				holder.district = (TextView) convertView.findViewById(R.id.district);
				holder.subways = (LinearLayout)convertView.findViewById(R.id.subways);
				holder.newIcon = (ImageView) convertView.findViewById(R.id.icon_new);
				holder.favoriteIcon = (ImageView) convertView.findViewById(R.id.icon_favorite);
				holder.favorite = (View) convertView.findViewById(R.id.favorite);
				holder.favorite.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						View parentView = (View)view.getParent();
						Bar bar = (Bar)parentView.getTag(R.id.tag_bar);
						if(bar != null) {
							L.d(TAG, "bar name : " + bar.getNameKor());
							if(bar.isFavorite()) { 
								removeFavorite(bar);
								Toast.makeText(view.getContext(), R.string.do_unfavorite, Toast.LENGTH_LONG).show();
							}else {
								Toast.makeText(view.getContext(), R.string.do_favorite, Toast.LENGTH_LONG).show(); 
								addFavorite(bar);
							}
						}
					}
				});	
			} else {
				holder = (ViewHolder)convertView.getTag();
			}

			Bar bar = mBarList.get(position); 
			if(bar != null && holder.barImage != null) {
				convertView.setTag(R.id.tag_bar, bar); 

				int id = Integer.valueOf(bar.getId());
				ThumbnailHolder thumbnailHolder = ThumbnailHolder.getInstance();
				Bitmap thumbnail = thumbnailHolder.retrieve(bar.getThumbnailUrl());
				// 1. cache에 이미지가 있다면 표시한다.
				if(thumbnail != null) {
					holder.barImage.setImageBitmap(thumbnail);
				}else {
				// 2. 정보가 수정되었다면 웹에서 이미지를 불러오고 cache와 file에 저장한다.
//					if(isUpdated(bar)) {
//						getImageFromWeb(bar.getThumbnailUrl(), id);
//					}else {
				// 3. 정보가 수정되지 않았다면 file에서 이미지를 불러온다.
						thumbnail = loadImageFromFile(id);
						if(thumbnail == null) {
							// file에서 이미지가 없다면 기본으로 설정된 이미지를 불러온다.
							thumbnail = BitmapFactory.decodeResource(getResources(), ImageResource.BAR[id]);
							String fileName = Constants.BAR + Constants.THUMBNAIL + id + ".png";
							Util.saveBitmapToFile(thumbnail, fileName, mContext);
						}
						holder.barImage.setImageBitmap(thumbnail);
						thumbnailHolder.save(bar.getThumbnailUrl(), thumbnail);
//					}
				}				
				
				holder.nameKor.setText(bar.getNameKor());
				String nameEngStr = bar.getNameEng().replace(" ", "");
				if(nameEngStr.isEmpty()) {
					holder.nameEng.setText("");
				}else {
					holder.nameEng.setText("(" + bar.getNameEng() + ")");
				}
				
				LinearLayout.LayoutParams lps = (android.widget.LinearLayout.LayoutParams) holder.nameKor.getLayoutParams();
				if(bar.isNew()) { 
					lps.leftMargin = margin15;
					holder.newIcon.setVisibility(View.VISIBLE);
				}else { 
					lps.leftMargin = margin0; 
					holder.newIcon.setVisibility(View.GONE); 
				}
				
				holder.district.setText(bar.getAddress().getDistrict()); 
				
				holder.subways.removeAllViews(); 
				Subway subway = bar.getAddress().getMap().getSubway();
				if(subway != null) {
					List<Line> lines = subway.getLines();
					for (Line line : lines) {
						ImageView subwayIV = new ImageView(inflater.getContext());
						subwayIV.setImageResource(ImageResource.SUBWAY[line.getId()]);
						LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, height);
						lp.rightMargin = margin5;
						holder.subways.addView(subwayIV, lp);
					}
					
					Station station = getStation(subway);
					
					TextView nameTV = new TextView(inflater.getContext());
					nameTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
					nameTV.setTextColor(getResources().getColor(R.color.black));
					if(station != null) {
						nameTV.setText(station.getName()); 
					}
					LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					lp.leftMargin = margin3;
					holder.subways.addView(nameTV, lp);
					
					TextView exitTV = new TextView(inflater.getContext());
					exitTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
					exitTV.setTextColor(getResources().getColor(R.color.black));
					if(station != null) {
						exitTV.setText(station.getExit() + getResources().getString(R.string.str_exit));
					}
					LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					lp2.leftMargin = margin5;
					holder.subways.addView(exitTV, lp2);
					holder.subways.setGravity(Gravity.CENTER_VERTICAL);
				}else {
					ImageView subwayIV = new ImageView(inflater.getContext());
					subwayIV.setImageResource(R.drawable.blank_57_57); 
					LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, height);
					holder.subways.addView(subwayIV, lp);
				}
			}
			
			if(bar.isFavorite()) {
				holder.favoriteIcon.setImageResource(R.drawable.icon_favorite_fill);
			}else {
				holder.favoriteIcon.setImageResource(R.drawable.icon_unfavorite);
			}
			
			return convertView;
		}

		private Station getStation(Subway subway) {

			Station station = null;
			
			if(subway != null) {
				List<Station> stationList = subway.getStations();
				if(stationList != null) {
					for (Station aStation : stationList) {
						if(aStation != null) {
							station  = aStation;
							break;
						}
					} 
				}
			}
			
			return station;
		}
		
		private Bitmap loadImageFromFile(int id) {
			String fileName = Constants.BAR + Constants.THUMBNAIL + id + ".png";
			
			Bitmap bitmap = Util.loadBitmapFromFile(fileName, mContext);
			
			return bitmap;
		}
		
		private void getImageFromWeb(String url, int id) {
			ThumbnailHolder thumbnailHolder = ThumbnailHolder.getInstance();
			if(!thumbnailHolder.isInTask(url)) {
				String fileName = Constants.BAR + Constants.THUMBNAIL + id + ".png";
				DownloadImagesTask task = new DownloadImagesTask(mRefreshList, fileName, mContext);
				thumbnailHolder.setTask(url, task);
				task.execute(url);
			}
		}
		
		private boolean isUpdated(Bar bar) {
			boolean isUpdated = false;
			SharedPreferences prefs = mContext.getSharedPreferences(Constants.CLUB_AND_BAR_PREF, Context.MODE_PRIVATE);
	    	Date lastUpdateDate = bar.getLastUpdateDate();
	    	if(lastUpdateDate != null) {
	    		long updateDate = lastUpdateDate.getTime();
	    		long savedDate = prefs.getLong(Constants.BAR_LAST_UPDATED_DATE + bar.getId(), 0L);
	    		if(updateDate != savedDate) {
	    			isUpdated = true;
	    			
	    			SharedPreferences.Editor editor = prefs.edit();
					editor.putLong(Constants.BAR_LAST_UPDATED_DATE + bar.getId(), updateDate);
					editor.commit();
	    		}
	    	}
	    	
	    	return isUpdated;
		}		
	}
	
	public static class ViewHolder {
		public ImageView barImage;
		public TextView nameKor;
		public TextView nameEng;
		public TextView district; 
		public LinearLayout subways;
		public ImageView newIcon;
		public View favorite;
		public ImageView favoriteIcon;
	}
	
	@Override
	public void refreshAdapter() {
		Collections.sort(mFavoriteList, new BarComparator());
		Collections.sort(mBarList, new BarComparator());  
		
		if(mFavoriteAdapter != null) {
			mFavoriteAdapter.notifyDataSetChanged();
		}
		if(mBarAdapter != null) {
			mBarAdapter.notifyDataSetChanged();
		}
		if(mAdapter != null) {
			mAdapter.notifyDataSetChanged();
		}
	}	

	@Override
	public void refreshList() {
		L.d(TAG, "refreshList() is called"); 
		
		List<Bar> barList = ClubAndBarHolder.getInstance().retrieveBar();
		if(barList != null && mFavoriteList != null && mBarList != null) {
			mBarList.clear();
			mFavoriteList.clear();
			for (Bar bar : barList) {
				if (bar.isFavorite()) {
					mFavoriteList.add(bar);
				}else {
					mBarList.add(bar);
				}
			}
			
			refreshAdapter();
		}
	}
	
	private void addFavorite(Bar bar) {
		
		if(bar == null) {
			L.d(TAG, "addFavorite() : bar is null.");
			return;
		}
		
		String id = bar.getId();
		boolean isInFavoriteList = false;
		
		for (Bar aBar : mFavoriteList) {
			if(id.equals(aBar.getId())) {
				isInFavoriteList = true;
			} 
		}
		
		if(!isInFavoriteList) {
			Bar barToAdd = null;
			
			int indexOfBarInList = 0;
			for (int i = 0; i < mBarList.size(); i++) {
				Bar aBar = mBarList.get(i);
				if(id.equals(aBar.getId())) {
					barToAdd = aBar;
					indexOfBarInList = i;
				}
			}
			
			if(barToAdd != null) {
				barToAdd.setFavorite(true);
				mFavoriteList.add(barToAdd);
				mBarList.remove(indexOfBarInList);
			}
		}
		
		new FavoriteParser(Constants.BAR, Constants.ACTION_FOLLOW, id).execute();
		
		refreshAdapter();
	} 
	
	private void removeFavorite(Bar bar) {
		
		if(bar == null) {
			L.d(TAG, "removeFavorite() : bar is null.");
			return;
		}
		
		String id = bar.getId();
		boolean isInFavoriteList = false;
		
		for (Bar aBar : mFavoriteList) {
			if(id.equals(aBar.getId())) {
				isInFavoriteList = true;
			}
		}
		
		if(isInFavoriteList) {
			
			Bar barToRemove = null;
			for (Bar aBar : mFavoriteList) {
				if(id.equals(aBar.getId())) {
					barToRemove = aBar;
				}
			}
			
			if(barToRemove != null) { 
				mFavoriteList.remove(barToRemove);
				barToRemove.setFavorite(false);
				mBarList.add(barToRemove);
			}
		}
		
		new FavoriteParser(Constants.BAR, Constants.ACTION_UNFOLLOW, id).execute();
		
		refreshAdapter();
	}
	
	private Bar getBar(String barId) {
		Bar bar = null;
		
		if(barId != null) { 
			bar = ClubAndBarHolder.getInstance().retrieveBar(barId);
		}
		 
		return bar;
	}
}
