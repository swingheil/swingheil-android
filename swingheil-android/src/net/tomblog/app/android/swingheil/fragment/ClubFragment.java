package net.tomblog.app.android.swingheil.fragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.activity.ClubDetailActivity;
import net.tomblog.app.android.swingheil.adapter.SeparatedListAdapter;
import net.tomblog.app.android.swingheil.parser.DownloadImagesTask;
import net.tomblog.app.android.swingheil.parser.RefreshAdapter;
import net.tomblog.app.android.swingheil.parser.RefreshClubList;
import net.tomblog.app.android.swingheil.parser.server.ClubListParser;
import net.tomblog.app.android.swingheil.parser.server.FavoriteParser;
import net.tomblog.app.android.swingheil.util.ClubAndBarHolder;
import net.tomblog.app.android.swingheil.util.ClubComparator;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.ImageResource;
import net.tomblog.app.android.swingheil.util.L;
import net.tomblog.app.android.swingheil.util.ThumbnailHolder;
import net.tomblog.app.android.swingheil.util.Util;
import net.tomblog.app.android.swingheil.view.ActionItem;
import net.tomblog.app.android.swingheil.view.QuickAction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;

public class ClubFragment extends Fragment implements RefreshAdapter, RefreshClubList {

	private static final String TAG = ClubFragment.class.getName();
	
    private static final String KEY_TITLE = "title";
    private static final String KEY_INDICATOR_COLOR = "indicator_color";
    private static final String KEY_DIVIDER_COLOR = "divider_color";

	private Context mContext;
	private ListView mClubListView;
	
	private SeparatedListAdapter mAdapter = null;
	private BaseAdapter mFavoriteAdapter = null;
	private BaseAdapter mClubAdapter = null;
	private RefreshAdapter mRefreshList= null;
	
	private ArrayList<Club> mFavoriteList = null;
	private ArrayList<Club> mClubList = null;
	private static String mLastSelectedId = "";
	 
    /**
     * @return a new instance of {@link ClubFragment}, adding the parameters into a bundle and
     * setting them as arguments.
     */
    public static ClubFragment newInstance(CharSequence title, int indicatorColor,
            int dividerColor) {
        Bundle bundle = new Bundle();
        bundle.putCharSequence(KEY_TITLE, title);
        bundle.putInt(KEY_INDICATOR_COLOR, indicatorColor);
        bundle.putInt(KEY_DIVIDER_COLOR, dividerColor);

        ClubFragment fragment = new ClubFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	L.d(TAG, " >>> called onCreateView()");
    	
        return inflater.inflate(R.layout.pager_item_club, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
		L.d(TAG, " >>> called onViewCreated()");
		mContext = view.getContext();
		
		inflateView(view);

		initObject();

    }

	private void inflateView(View view) {
		L.d(TAG, " >>> called inflateView()");

		mClubListView = (ListView) view.findViewById(R.id.club_list);
	}
	
	private void initObject() {
		L.d(TAG, " >>> called initObject()");
		
		mRefreshList = this;
		
		if(mFavoriteList == null) {
			mFavoriteList = new ArrayList<Club>();
		}
		if(mClubList == null) {
			mClubList = new ArrayList<Club>();
		}
		
		List<Club> clubList = ClubAndBarHolder.getInstance().retrieveClub();
		if(clubList != null && mFavoriteList.size() == 0 && mClubList.size() == 0) {
			for (Club club : clubList) {
				if (club.isFavorite()) {
					mFavoriteList.add(club);
				}else {
					mClubList.add(club);
				}
			}
		}
		
		final QuickAction mQuickAction = new QuickAction(mContext);
		ActionItem addItem = new ActionItem(QuickAction.DETAIL, getResources().getString(R.string.quick_action_detail), 
				getResources().getDrawable(R.drawable.popup_menu_icon_detail));
		ActionItem acceptItem = new ActionItem(QuickAction.FAVORITE, getResources().getString(R.string.quick_action_favorite), 
				getResources().getDrawable(R.drawable.popup_menu_icon_favorite));
		ActionItem uploadItem = new ActionItem(QuickAction.UNFAVORITE, getResources().getString(R.string.quick_action_unfavorite), 
				getResources().getDrawable(R.drawable.popup_menu_icon_unfavorite)); 
		mQuickAction.addActionItem(addItem);
		mQuickAction.addActionItem(acceptItem);
		mQuickAction.addActionItem(uploadItem);
		mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction quickAction, int pos, int actionId) {
				if (actionId == QuickAction.DETAIL) { 
					Intent intent = new Intent(getActivity(), ClubDetailActivity.class);
					intent.putExtra(Constants.CLUB_ID, mLastSelectedId); 
					startActivity(intent);
				} else if(mLastSelectedId != null && !"".equalsIgnoreCase(mLastSelectedId)) {
					if(actionId == QuickAction.FAVORITE){ 
						addFavorite(getClub(mLastSelectedId)); 
					} else if(actionId == QuickAction.UNFAVORITE){
						removeFavorite(getClub(mLastSelectedId));
					}
				} 
			}
		});
		mQuickAction.setOnDismissListener(new PopupWindow.OnDismissListener() {
			@Override
			public void onDismiss() {
			}
		});
		
		mFavoriteAdapter = new ClubListAdapter(mContext, mFavoriteList);
		mClubAdapter = new ClubListAdapter(mContext, mClubList);

		// create our list and custom adapter
		mAdapter = new SeparatedListAdapter(mContext);
		mAdapter.addSection(R.string.list_header_favorite, mFavoriteAdapter);
		mAdapter.addSection(R.string.list_header_club, mClubAdapter);
		
		mClubListView.setAdapter(mAdapter);
		mClubListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				L.d(TAG, "onItemClick() is called position : " + position + " id : " + id); 
				Club club = (Club)view.getTag(R.id.tag_club);
				if(club != null) {
					L.d(TAG, "club name : " + club.getNameKor());
					mLastSelectedId = club.getId();
					Intent intent = new Intent(getActivity(), ClubDetailActivity.class);
					intent.putExtra(Constants.CLUB_ID, mLastSelectedId); 
					startActivity(intent);
				}
			}
		});
		mClubListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				L.d(TAG, "onItemLongClick() is called position : " + position + " id : " + id);
				Club club = (Club)view.getTag(R.id.tag_club);
				if(club != null) {
					L.d(TAG, "club name : " + club.getNameKor());
					mLastSelectedId = club.getId();
					
					mQuickAction.show(view); 
				}
				return true;
			}
		});
		
		refreshAdapter();
		
		new ClubListParser(mContext, this).execute();

	}
	
	@Override
	public void onResume() {
		super.onResume();
		L.d(TAG, "onResume() is called"); 
    	ClubAndBarHolder holder = ClubAndBarHolder.getInstance();
    	Club club = holder.retrieveClub(mLastSelectedId);
    	if(club != null) {
    		L.d(TAG, "club name : " + club.getNameKor()); 
    		if(club.isFavorite()) { 
    			addFavorite(club); 
    		}else { 
    			removeFavorite(club);
    		}
    	} else {
    		L.e(TAG, "last selected club is null"); 
    	}
	}
	
	private class ClubListAdapter extends BaseAdapter {

		private LayoutInflater inflater = null;
		private List<Club> mClubList = null;
		private final int margin0 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
		private final int margin15 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
		private Context mContext;

		public ClubListAdapter(Context context, List<Club> clubList) {
			inflater = LayoutInflater.from(context);
			mClubList = clubList;
			mContext = context;
		}

		@Override
		public int getCount() {
			return mClubList.size();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.list_item_club, parent, false);
				convertView.setTag(holder);
				
				holder.clubImage = (ImageView) convertView.findViewById(R.id.club_image);
				holder.nameKor = (TextView) convertView.findViewById(R.id.nameKor);
				holder.nameEng = (TextView) convertView.findViewById(R.id.nameEng);
				holder.district = (TextView) convertView.findViewById(R.id.district);
				holder.newIcon = (ImageView) convertView.findViewById(R.id.icon_new);
				holder.favoriteIcon = (ImageView) convertView.findViewById(R.id.icon_favorite);
				holder.favorite = (View) convertView.findViewById(R.id.favorite);
				holder.favorite.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						View parentView = (View)view.getParent();
						Club club = (Club)parentView.getTag(R.id.tag_club);
						if(club != null) {
							L.d(TAG, "club name : " + club.getNameKor());
							if(club.isFavorite()) {
								removeFavorite(club);
								Toast.makeText(view.getContext(), R.string.do_unfavorite, Toast.LENGTH_LONG).show();
							}else {
								Toast.makeText(view.getContext(), R.string.do_favorite, Toast.LENGTH_LONG).show(); 
								addFavorite(club);
							}
						}
					}
				});	
			} else {
				holder = (ViewHolder)convertView.getTag();
			}

			Club club = mClubList.get(position); 
			if(club != null && holder.clubImage != null) {
				convertView.setTag(R.id.tag_club, club); 
				
				int id = Integer.valueOf(club.getId());
				ThumbnailHolder thumbnailHolder = ThumbnailHolder.getInstance();
				Bitmap thumbnail = thumbnailHolder.retrieve(club.getThumbnailUrl());
				// 1. cache에 이미지가 있다면 표시한다.
				if(thumbnail != null) {
					holder.clubImage.setImageBitmap(thumbnail);
				}else {
				// 2. 정보가 수정되었다면 웹에서 이미지를 불러오고 cache와 file에 저장한다.
//					if(isUpdated(club)) {
						// TODO 추후에 수 할것 
//						getImageFromWeb(club.getThumbnailUrl(), id);
//					}else {
				// 3. 정보가 수정되지 않았다면 file에서 이미지를 불러온다.
						thumbnail = loadImageFromFile(id);
						if(thumbnail == null) {
							// file에서 이미지가 없다면 기본으로 설정된 이미지를 불러온다.
							thumbnail = BitmapFactory.decodeResource(getResources(), ImageResource.CLUB[id]);
							String fileName = Constants.CLUB + Constants.THUMBNAIL + id + ".png";
							Util.saveBitmapToFile(thumbnail, fileName, mContext);
						}
						holder.clubImage.setImageBitmap(thumbnail);
						thumbnailHolder.save(club.getThumbnailUrl(), thumbnail);
//					}
				}
				
				holder.nameKor.setText(club.getNameKor());
				String nameEngStr = club.getNameEng().replace(" ", "");
				if(nameEngStr.isEmpty()) {
					holder.nameEng.setText("");
				}else {
					holder.nameEng.setText("(" + club.getNameEng() + ")");
				}
				
				LinearLayout.LayoutParams lp = (android.widget.LinearLayout.LayoutParams) holder.nameKor.getLayoutParams();
				if(club.isNew()) { 
					lp.leftMargin = margin15;
					holder.newIcon.setVisibility(View.VISIBLE);
				}else {
					lp.leftMargin = margin0; 
					holder.newIcon.setVisibility(View.GONE); 
				}  
				
				ClubAndBarHolder clubAndBarHolder = ClubAndBarHolder.getInstance();
				if(clubAndBarHolder != null) {
					Bar aBar = club.getBar();
					if(aBar != null) {
						Bar bar = clubAndBarHolder.retrieveBar(aBar.getId());
						if(bar != null) {
							holder.district.setText(bar.getAddress().getDistrict()); 
						}
					}
				}
				
				if(club.isFavorite()) {
					holder.favoriteIcon.setImageResource(R.drawable.icon_favorite_fill);
				}else {
					holder.favoriteIcon.setImageResource(R.drawable.icon_unfavorite);
				}
			}
			
			return convertView;
		}

		private Bitmap loadImageFromFile(int id) {
			String fileName = Constants.CLUB + Constants.THUMBNAIL + id + ".png";
			
			Bitmap bitmap = Util.loadBitmapFromFile(fileName, mContext);
			
			return bitmap;
		}

		private void getImageFromWeb(String url, int id) {
			ThumbnailHolder thumbnailHolder = ThumbnailHolder.getInstance();
			if(!thumbnailHolder.isInTask(url)) {
				String fileName = Constants.CLUB + Constants.THUMBNAIL + id + ".png";
				DownloadImagesTask task = new DownloadImagesTask(mRefreshList, fileName, mContext);
				thumbnailHolder.setTask(url, task);
				task.execute(url);
			}
		}

		private boolean isUpdated(Club club) {
			boolean isUpdated = false;
			SharedPreferences prefs = mContext.getSharedPreferences(Constants.CLUB_AND_BAR_PREF, Context.MODE_PRIVATE);
	    	Date lastUpdateDate = club.getLastUpdateDate();
	    	if(lastUpdateDate != null) {
	    		long updateDate = lastUpdateDate.getTime();
	    		long savedDate = prefs.getLong(Constants.CLUB_LAST_UPDATED_DATE + club.getId(), 0L);
	    		if(updateDate != savedDate) {
	    			isUpdated = true;
	    			
	    			SharedPreferences.Editor editor = prefs.edit();
					editor.putLong(Constants.CLUB_LAST_UPDATED_DATE + club.getId(), updateDate);
					editor.commit();
	    		}
	    	}
	    	
	    	return isUpdated;
		}
	}
	
	public static class ViewHolder {
		public ImageView clubImage;
		public TextView nameKor;
		public TextView nameEng;
		public TextView district; 
		public ImageView newIcon;
		public View favorite;
		public ImageView favoriteIcon;
	}
	
	@Override
	public void refreshAdapter() {
		Collections.sort(mFavoriteList, new ClubComparator());
		Collections.sort(mClubList, new ClubComparator()); 
		
		if(mFavoriteAdapter != null) {
			mFavoriteAdapter.notifyDataSetChanged();
		}
		if(mClubAdapter != null) {
			mClubAdapter.notifyDataSetChanged();
		}
		if(mAdapter != null) {
			mAdapter.notifyDataSetChanged();
		}
	}
	
	@Override
	public void refreshList() {
		L.d(TAG, "refreshList() is called"); 
		
		List<Club> clubList = ClubAndBarHolder.getInstance().retrieveClub();
		if(clubList != null && mFavoriteList != null && mClubList != null) {
			mClubList.clear();
			mFavoriteList.clear();
			for (Club club : clubList) {
				if (club.isFavorite()) {
					mFavoriteList.add(club);
				}else {
					mClubList.add(club);
				}
			}
			
			refreshAdapter();
		}
	}
	
	private void addFavorite(Club club) {
		
		if(club == null) {
			L.d(TAG, "addFavorite() : club is null.");
			return;
		}
		
		String id = club.getId();
		boolean isInFavoriteList = false;
		
		for (Club aClub : mFavoriteList) {
			if(id.equals(aClub.getId())) {
				isInFavoriteList = true;
			}
		}
		
		if(!isInFavoriteList) {
			Club clubToAdd = null;
			int indexOfClubInList = 0;
			for (int i = 0; i < mClubList.size(); i++) {
				Club aClub = mClubList.get(i);
				if(id.equals(aClub.getId())) {
					clubToAdd = aClub;
					indexOfClubInList = i;
				}
			}
			
			if(clubToAdd != null) {
				clubToAdd.setFavorite(true);
				mFavoriteList.add(clubToAdd);
				mClubList.remove(indexOfClubInList);
			}
		}
		
		new FavoriteParser(Constants.CLUB, Constants.ACTION_FOLLOW, id).execute();
		
		refreshAdapter();
	}
	
	private void removeFavorite(Club club) {
		
		if(club == null) {
			L.d(TAG, "removeFavorite() : club is null.");
			return;
		}
		
		String id = club.getId();
		boolean isInFavoriteList = false;
		
		for (Club aClub : mFavoriteList) {
			if(id.equals(aClub.getId())) {
				isInFavoriteList = true;
			}
		}
		
		if(isInFavoriteList) {
			
			Club clubToRemove = null;
			for (Club aClub : mFavoriteList) {
				if(id.equals(aClub.getId())) {
					clubToRemove = aClub;
				}
			}
			
			if(clubToRemove != null) { 
				mFavoriteList.remove(clubToRemove);
				clubToRemove.setFavorite(false);
				mClubList.add(clubToRemove);
			}
		}
		
		new FavoriteParser(Constants.CLUB, Constants.ACTION_UNFOLLOW, id).execute();
		
		refreshAdapter();
	}
	
	private Club getClub(String clubId) {
		Club club = null;
		
		if(clubId != null) { 
			club = ClubAndBarHolder.getInstance().retrieveClub(clubId);
		}
		
		return club;
	}

}
