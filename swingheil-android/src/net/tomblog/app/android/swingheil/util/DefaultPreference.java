package net.tomblog.app.android.swingheil.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class DefaultPreference {
	protected SharedPreferences sharedPref;
	protected Editor editor;
	
	private static final String DEFAULT_SHARED = "Default_Preferences";
	
	public DefaultPreference(Context context) {
		sharedPref = context.getSharedPreferences(DEFAULT_SHARED, Context.MODE_PRIVATE);
		editor = sharedPref.edit();
	}
	
	public void storePreference(String name, String value) {
		editor.putString(name, value);
		editor.commit();
	}
	
	public String getPreference(String name) {
		return sharedPref.getString(name, null);
	}
}