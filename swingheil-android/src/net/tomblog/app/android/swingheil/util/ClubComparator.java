package net.tomblog.app.android.swingheil.util;

import java.util.Comparator;

import com.swingheil.domain.group.Club;

public class ClubComparator implements Comparator<Club> {

	@Override
	public int compare(Club first, Club second) {
		return second.getLastUpdateDate().compareTo(first.getLastUpdateDate());
	}

}
