package net.tomblog.app.android.swingheil.util;

import java.util.HashMap;
import java.util.Map;

import net.tomblog.app.android.swingheil.parser.DownloadImagesTask;
import android.graphics.Bitmap;

public class ThumbnailHolder {
//	private static final String TAG = ThumbnailHolder.class.getName();
	
	private Map<String, Bitmap> thumbnailMap = new HashMap<String,Bitmap>();
	private Map<String, DownloadImagesTask> taskMap = new HashMap<String, DownloadImagesTask>();

	private static final ThumbnailHolder holder = new ThumbnailHolder();
	
	public static ThumbnailHolder getInstance() {
		return holder;
	}
	  
	public void save(String url, Bitmap bitmap) {
		thumbnailMap.put(url, bitmap);
	}
	
	public Bitmap retrieve(String url) {
		return thumbnailMap.get(url);
	}

	public void remove(String url) {
		thumbnailMap.remove(url); 
	}

	public boolean isInTask(String url) {
		boolean isInTask = false;

		isInTask = taskMap.containsKey(url);
		
		return isInTask;
	}

	public void setTask(String url, DownloadImagesTask task) {
		taskMap.put(url, task);
	}

	public void removeTask(String url) {
		taskMap.remove(url);
	}
}
