package net.tomblog.app.android.swingheil.util;

public class Constants {
	
	public static final String CLIENT_ID = "DAN-somx9ib39y5u";

	public static final int REQUEST_EVENT_DETAIL = 111;
	public static final int REQUEST_CLUB_DETAIL = 112;
	public static final int REQUEST_BAR_DETAIL = 113;
	
	public static final String EVENT = "event";
	public static final String CLUB = "club";
	public static final String BAR = "bar";
	public static final String GROUP_TYPE = "groupType";
	public static final String GROUP_ID = "groupId";
	public static final String IS_FROM_GCM_NOTI = "isFromGcmNoti";
	
	public static final String CLUB_ID = "clubId";
	public static final String BAR_ID = "barId";
	public static final String EVENT_ID = "eventId";
	public static final String USER_ID = "userId";
	
	public static final String ADDRESS_TYPE = "addressType";
	public static final int ADDRESS_TYPE_ROAD = 1;
	public static final int ADDRESS_TYPE_LAND = 2;
	
	public static final String USER_PREF = "userPref";
	
	public static final String EVENT_PREF = "eventPref";
	
	public static final String CLUB_AND_BAR_PREF = "clubAndBarPref";
	public static final String IS_THERE_CLUB_INFO = "isThereClubInfo";
	public static final String IS_THERE_BAR_INFO = "isThereBarInfo";
	public static final String CLUB_LAST_UPDATED_DATE = "clubLastUpdatedDate";
	public static final String BAR_LAST_UPDATED_DATE = "barLastUpdatedDate";
	public static final String CLUB_DETAIL_LAST_UPDATED_DATE = "clubDetailLastUpdatedDate";
	
	public static final String JSON_BARS = "bars";
	public static final String JSON_CLUBS = "clubs";
	public static final String JSON_ID = "id";
	public static final String JSON_RESULTS = "results";
	public static final String JSON_SUCCESS = "success";
	public static final String JSON_MESSAGE = "message";
	
	public static final String SWING_ALL = "00";
	public static final String SWING_HEIL = "01";
	public static final String SWING_BAR = "02";
	public static final String SWING_CLUB = "03";
	public static final String SWING_FAVORITE = "04";
	
	public static final String ACTION_FOLLOW = "follow"; 
	public static final String ACTION_UNFOLLOW = "unfollow"; 
	
	public static final int NEW_DAY_LIMIT = -3;
	
	public static final String THUMBNAIL = "Thumbnail"; 
	public static final String DETAIL = "Detail"; 
	public static final String BASE_IMAGE_DIR = "data/data/net.tomblog.app.swingheil/files/"; 
	
}
