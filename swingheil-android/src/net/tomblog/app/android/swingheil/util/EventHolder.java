package net.tomblog.app.android.swingheil.util;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class EventHolder {
//	private static final String TAG = EventHolder.class.getName();
	
	private Map<String, Long> lastUpdateDateOfClubMap = new HashMap<String, Long>();
	private Map<String, Long> lastUpdateDateOfBarMap = new HashMap<String,Long>();
	private Map<String, Boolean> favoriteMap = new HashMap<String,Boolean>();
	
	private static final EventHolder holder = new EventHolder();
	
	public static EventHolder getInstance() {
		return holder;
	}

	public void saveLastUpdateDateOfClub(String clubId, Date lastUpdateDate) {
		saveLastUpdateDateOfClub(clubId, lastUpdateDate.getTime());
	}
	
	public void saveLastUpdateDateOfClub(String clubId, Long lastUpdateDate) {
		lastUpdateDateOfClubMap.put(clubId, lastUpdateDate);
	}
	
	public boolean hasNewClubEvent(String clubId, Date lastUpdateDate) {
		boolean hasNewEvent = false;
		
		Long preUpdateDate = lastUpdateDateOfClubMap.get(clubId);
		if(preUpdateDate == null) {
			preUpdateDate = 0L;
		}
		
		if(lastUpdateDate != null) {
			Long updateDate = lastUpdateDate.getTime();
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, Constants.NEW_DAY_LIMIT);
			Date standardDate = calendar.getTime();
			
			boolean isNew = lastUpdateDate.after(standardDate);
			
			if(isNew && updateDate.longValue() != preUpdateDate.longValue()) {
				hasNewEvent = true;
			}
		}
		
		return hasNewEvent;
	}
	
	public void saveLastUpdateDateOfBar(String barId, Date lastUpdateDate) {
		saveLastUpdateDateOfBar(barId, lastUpdateDate.getTime()); 
	}
	
	public void saveLastUpdateDateOfBar(String barId, Long lastUpdateDate) {
		lastUpdateDateOfBarMap.put(barId, lastUpdateDate);
	}
	
	public boolean hasNewBarEvent(String barId, Date lastUpdateDate) {
		boolean hasNewEvent = false;
		
		Long preUpdateDate = lastUpdateDateOfBarMap.get(barId);
		if(preUpdateDate == null) {
			preUpdateDate = 0L;
		}
		
		if(lastUpdateDate != null) {
			Long updateDate = lastUpdateDate.getTime();
			
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, Constants.NEW_DAY_LIMIT);
			Date standardDate = calendar.getTime();
			
			boolean isNew = lastUpdateDate.after(standardDate);
			
			if(isNew && updateDate.longValue() != preUpdateDate.longValue()) {
				hasNewEvent = true;
			}
		}
		
		return hasNewEvent;
	}
	
	public void saveEventFavorite(String eventId, boolean favoriete) {
		favoriteMap.put(eventId, favoriete);
	}
	
	public boolean isFavorite(String eventId) {
		boolean isFavorite = false;
		
		Boolean favorite = favoriteMap.get(eventId);
		
		if(favorite != null) {
			if(favorite.booleanValue() == true) {
				isFavorite = true;
			}
		}
		
		return isFavorite;
	}
}
