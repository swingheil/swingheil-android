package net.tomblog.app.android.swingheil.util;

import java.util.Comparator;

import com.swingheil.domain.group.Bar;

public class BarComparator implements Comparator<Bar> {

	@Override
	public int compare(Bar first, Bar second) {
		return second.getLastUpdateDate().compareTo(first.getLastUpdateDate());
	}

}
