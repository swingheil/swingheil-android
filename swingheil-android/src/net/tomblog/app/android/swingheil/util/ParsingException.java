package net.tomblog.app.android.swingheil.util;

public class ParsingException extends RuntimeException {

	private static final long serialVersionUID = 6610669654115532008L;
	
	public ParsingException(String msg) {
		super(msg);
	}
}
