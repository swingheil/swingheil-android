package net.tomblog.app.android.swingheil.util;

import java.util.Comparator;

import com.swingheil.domain.event.Event;

public class EventComparator implements Comparator<Event> {

	@Override
	public int compare(Event first, Event second) {
		int result = 0;
		
		String firstId = first.getId();
		String secondId = second.getId();
		
		if(firstId == null && secondId == null) {
			result = 0;
		}else if(firstId == null && secondId != null) {
			result = -1;
		}else if(firstId != null && secondId == null) {
			result = 1;
		}else {
			result = Integer.valueOf(secondId).compareTo(Integer.valueOf(firstId));
		}
		
		return result;
	}
}
