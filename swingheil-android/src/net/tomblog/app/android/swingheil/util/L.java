package net.tomblog.app.android.swingheil.util;

import android.util.Log;

/**
 * 로그 출력용 클래스 
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class L {
	private static Boolean logFlag = true;
	
	static public void i(String tag, String msg) {
		if (logFlag == true ) {
			Log.i(tag, msg);
		}
	}
	
	static public void d(String tag, String msg) {
		if (logFlag == true ) {
			Log.d(tag, msg);
		}
	}
	
	static public void w(String tag, String msg) {
		if (logFlag == true ) {
			Log.w(tag, msg);
		}
	}
	
	static public void e(String tag, String msg) {
		if (logFlag == true ) {
			Log.e(tag, msg);
		}
	}
}