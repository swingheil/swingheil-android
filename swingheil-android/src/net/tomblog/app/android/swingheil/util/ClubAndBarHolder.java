package net.tomblog.app.android.swingheil.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;

public class ClubAndBarHolder {
//	private static final String TAG = ClubAndBarHolder.class.getName();
	
	private Map<String, Club> clubMap = new HashMap<String, Club>();
	private Map<String, Bar> barMap = new HashMap<String, Bar>();
	private Map<String, List<String>> barOfClubMap = new HashMap<String, List<String>>();

	private static ClubAndBarHolder holder = new ClubAndBarHolder();
	
	public static ClubAndBarHolder getInstance() {
		if(holder == null) {
			holder = new ClubAndBarHolder();
		}
		return holder;
	}
	  
	public void saveClub(String id, Club club) {
		clubMap.put(id, club);
	}

	public List<Club> retrieveClub() {
		return new ArrayList<Club>(clubMap.values());
	}
	
	public Club retrieveClub(String id) {
		return clubMap.get(id);
	}
	
	public void saveBar(String id, Bar bar) {
		barMap.put(id, bar);
	}
	 
	public List<Bar> retrieveBar() {
		return new ArrayList<Bar>(barMap.values());
	}
	
	public Bar retrieveBar(String id) {
		return barMap.get(id);
	}

	public void saveBarOfClub(Club club) {
		if(club != null) {
			Bar bar = club.getBar();
			if(bar != null) {
				String barId = bar.getId();
				String clubId = club.getId();
				List<String> clubIdList = barOfClubMap.get(barId);
				if(clubIdList == null) {
					clubIdList = new ArrayList<String>();
				}
				if(!clubIdList.contains(clubId)) {
					clubIdList.add(clubId);
				}
				barOfClubMap.put(barId, clubIdList);
			}
		}
	}
	
	public List<String> retriveClubIdList(String barId) {
		return barOfClubMap.get(barId);
	}
}



















