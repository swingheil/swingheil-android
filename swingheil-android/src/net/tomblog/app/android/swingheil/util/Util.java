package net.tomblog.app.android.swingheil.util;

import java.io.FileInputStream;
import java.io.OutputStream;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.tomblog.app.android.swingheil.parser.server.BasicParser;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.swingheil.domain.shared.ResponseMessage;

public class Util {

	private static final String TAG = Util.class.getName();

	public static String getDeviceUid(Context context) {
    	TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
    	String deviceUid = telephonyManager.getDeviceId();
    	
    	return deviceUid;
	}
	
	public static String toString(Object o) {
		ArrayList<String> list = new ArrayList<String>();
		Util.toString(o, o.getClass(), list);
		return o.getClass().getName().concat(list.toString());
	}

	private static void toString(Object o, Class<?> clazz, List<String> list) {
		Field f[] = clazz.getDeclaredFields();
		AccessibleObject.setAccessible(f, true);
		for (int i = 0; i < f.length; i++) {
			try {
				list.add(f[i].getName() + "=" + f[i].get(o));
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		if (clazz.getSuperclass().getSuperclass() != null) {
			toString(o, clazz.getSuperclass(), list);
		}
	}
	
	public static boolean isEmpty(String string) {
		boolean isEmpty = true;
		
		if(string != null) {
			string = string.replace(" ", "");
			if(!"".equals(string)) {
				isEmpty = false; 
			}
		}
		
		return isEmpty;
	}
	
	public static ResponseMessage parseResponseMessage(String tag, String data) {
		ResponseMessage responseMessage = null;
		try {
			JSONObject reader = new JSONObject(data);
			boolean success = reader.getBoolean(BasicParser.SUCCESS);
			Log.d(tag, "success : " + success);
			
			String message = reader.getString(BasicParser.MESSAGE);
			Log.d(tag, "message : " + message);
				
			responseMessage = new ResponseMessage(success, message);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return responseMessage;
	}
	
	public static void saveBitmapToFile(Bitmap bitmap, String fileName, Context context) {
		L.d(TAG, "saveBitmapToFile is called. : " + fileName);
		
		if(bitmap == null) {
			return;
		}
		
        try {
        	OutputStream out = null;
            out = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            bitmap.compress(CompressFormat.JPEG, 100, out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	public static Bitmap loadBitmapFromFile(String fileName, Context context) {
		L.d(TAG, "loadBitmapFromFile is called : " + fileName);

		Bitmap bitmap = null;
		FileInputStream fis = null;

		try {
			fis = context.openFileInput(fileName);
			bitmap = BitmapFactory.decodeStream(fis);
			fis.close();
		} catch (Exception e) {
//			e.printStackTrace();
		} 

		if(bitmap != null) {
			L.e(TAG, fileName + " is loaded.");
		}
		
		return bitmap;
	}

	/**
     * Bearer Type별 네트워크 상태 정보 체크
     * @return Bearer Type별 네트워크 상태 정보
     */
    public static boolean isNetworkAvailable(Context context){
    	L.d(TAG, ">>> Called isNetworkAvailable()");
    	
		ConnectivityManager conn_manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		boolean isNetworkAvailable = false;
		try {
			NetworkInfo network_info = conn_manager.getActiveNetworkInfo();
			if (network_info != null && network_info.isConnected()) {
				if (network_info.getType() == ConnectivityManager.TYPE_WIFI) {
					isNetworkAvailable = true;
				} else if (network_info.getType() == ConnectivityManager.TYPE_MOBILE) {
					isNetworkAvailable = true;
					int currentapiVersion = android.os.Build.VERSION.SDK_INT;
					if (currentapiVersion > android.os.Build.VERSION_CODES.ECLAIR_MR1) {
						if (network_info.getType() == ConnectivityManager.TYPE_WIMAX) {
							// API Level 8부터 지원 (Android 2.2)
							// 2.2이상 디바이스일때 예외처리할것.
							isNetworkAvailable = true;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		L.d(TAG, "isNetworkAvailable : " + isNetworkAvailable);
		return isNetworkAvailable;
	}

	public static boolean isEqual(String str1, String str2) {
		boolean isEqual = false;
		
		if(str1 != null && str2 != null) {
			if(str1.equals(str2)) {
				isEqual = true;
			}
		}
		
		return isEqual;
	}
}
