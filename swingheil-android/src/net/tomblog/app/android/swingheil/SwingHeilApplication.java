package net.tomblog.app.android.swingheil;

import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.Util;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class SwingHeilApplication extends Application {
	
	private static SwingHeilApplication appInstance;
	
	private String deviceUid = null;
	private String userId = null;

	@Override 
	public void onCreate() {
		super.onCreate();  
		appInstance = this;
		
		deviceUid = Util.getDeviceUid(getApplicationContext());
		
        SharedPreferences prefs = getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE);
        userId = prefs.getString(Constants.USER_ID, null);
	}

	public static SwingHeilApplication getInstance() {
		return appInstance;
	}

	public String getDeviceUid() {
		return deviceUid;
	}
	
	public void setDeviceUid(String deviceUid) {
		this.deviceUid = deviceUid;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}
}
