package net.tomblog.app.android.swingheil.adapter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.tomblog.app.android.swingheil.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SeparatedListAdapter extends BaseAdapter {
	 
	private static final String TAG = SeparatedListAdapter.class.getName();
	
    public final Map<String, Adapter> sections = new LinkedHashMap<String, Adapter>();
    public final List<String> headers;
    public final static int TYPE_SECTION_HEADER = 0;
    
    private Context mContext;

    public SeparatedListAdapter(Context context) {
        headers = new ArrayList<String>();
        mContext = context;
    }

    public void addSection(String sectionTitle, Adapter adapter) {
        this.headers.add(sectionTitle);
        this.sections.put(sectionTitle, adapter);
    }

    public void addSection(int titleResourceId, Adapter adapter) {
    	String sectionTitle = mContext.getString(titleResourceId);
    	addSection(sectionTitle, adapter);
    	this.notifyDataSetChanged();
    }
    
    public Object getItem(int position) {
        for (Object section : this.sections.keySet()) {
            Adapter adapter = sections.get(section);
            int size = adapter.getCount() + 1;

            // check if position inside this section
            if (position == 0)
                return section;
            if (position < size)
                return adapter.getItem(position - 1);

            // otherwise jump into next section
            position -= size;
        }
        return null;
    }

    public int getCount() {
        // total together all sections, plus one for each section header
        int total = 0;
        for (Adapter adapter : this.sections.values())
            total += adapter.getCount() + 1;
        return total;
    }

    public int getViewTypeCount() {
        // assume that headers count as one, then total all sections
        int total = 1;
        for (Adapter adapter : this.sections.values())
            total += adapter.getViewTypeCount();
        return total;
    }

    public int getItemViewType(int position) {
        int type = 1;
        for (Object section : this.sections.keySet()) {
            Adapter adapter = sections.get(section);
            int size = adapter.getCount() + 1;

            // check if position inside this section
            if (position == 0)
                return TYPE_SECTION_HEADER;
            if (position < size)
                return type + adapter.getItemViewType(position - 1);

            // otherwise jump into next section
            position -= size;
            type += adapter.getViewTypeCount();
        }
        return -1;
    }

    public boolean areAllItemsSelectable() {
        return false;
    }

    public boolean isEnabled(int position) {
        return (getItemViewType(position) != TYPE_SECTION_HEADER);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int sectionnum = 0;
        for (Object section : this.sections.keySet()) {
            Adapter adapter = sections.get(section);
            int size = adapter.getCount() + 1;

            // check if position inside this section
            if (position == 0) {
            	if(adapter.getCount() == 0) {
            		LayoutInflater inflater = LayoutInflater.from(mContext);
            		return inflater.inflate(R.layout.list_item_null , null);
            	}else { 
        			LayoutInflater inflater = LayoutInflater.from(mContext);
        			TextView headerTitle = (TextView)inflater.inflate(R.layout.list_header , null);
        			headerTitle.setText(headers.get(sectionnum));
            		return headerTitle;
            	}
            }
            if (position < size) {
                return adapter.getView(position - 1, convertView, parent);
            }

            // otherwise jump into next section
            position -= size;
            sectionnum++;
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}