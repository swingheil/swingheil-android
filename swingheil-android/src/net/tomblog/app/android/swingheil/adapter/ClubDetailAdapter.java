package net.tomblog.app.android.swingheil.adapter;

import java.util.Date;
import java.util.List;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.activity.BarDetailActivity;
import net.tomblog.app.android.swingheil.activity.NaverMapActivity;
import net.tomblog.app.android.swingheil.parser.DownloadImagesTask;
import net.tomblog.app.android.swingheil.parser.RefreshAdapter;
import net.tomblog.app.android.swingheil.parser.server.FavoriteParser;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.EventHolder;
import net.tomblog.app.android.swingheil.util.ImageResource;
import net.tomblog.app.android.swingheil.util.L;
import net.tomblog.app.android.swingheil.util.ThumbnailHolder;
import net.tomblog.app.android.swingheil.util.Util;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.swingheil.domain.event.Event;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.type.place.Station;
import com.swingheil.domain.type.place.Subway;
import com.swingheil.domain.type.place.Subway.Line;

public class ClubDetailAdapter extends BaseAdapter {
	
	private static final String TAG = ClubDetailAdapter.class.getName();
	
	private static final int TYPE_CLUB_BANNER = 0;
	private static final int TYPE_HOMEPAGE = 1;
	private static final int TYPE_BAR = 2;
	private static final int TYPE_SUBWAY = 3;
	private static final int TYPE_ADDRESS = 4;
	private static final int TYPE_MAP = 5;
	private static final int TYPE_ADMIN = 6;
	private static final int TYPE_CLUB_NOTICE = 7;
	private static final int TYPE_NOTICE_ITEM = 8;
	
	private static final int TYPE_ADDRESS_NO_SUBWAY = 3;
	private static final int TYPE_MAP_NO_SUBWAY = 4;
	private static final int TYPE_ADMIN_NO_SUBWAY = 5;
	private static final int TYPE_BAR_NOTICE_NO_SUBWAY = 6;
	private static final int TYPE_NOTICE_ITEM_NO_SUBWAY = 7;		
	
	private boolean doesExistSubway = false;
	
	private final int[] types = {
			TYPE_CLUB_BANNER, TYPE_HOMEPAGE, TYPE_BAR, TYPE_SUBWAY, TYPE_ADDRESS, 
			TYPE_MAP, TYPE_ADMIN, TYPE_CLUB_NOTICE, TYPE_NOTICE_ITEM 
	};
	private final int TYPE_MAX_COUNT = types.length;
	
	private final int[] typesWithoutSubway = {
			TYPE_CLUB_BANNER, TYPE_HOMEPAGE, TYPE_BAR ,TYPE_ADDRESS_NO_SUBWAY, TYPE_MAP_NO_SUBWAY, 
			TYPE_ADMIN_NO_SUBWAY, TYPE_BAR_NOTICE_NO_SUBWAY, TYPE_NOTICE_ITEM_NO_SUBWAY
	};
	
	private final int TYPE_MAX_COUNT_WITHOUT_SUBWAY = typesWithoutSubway.length;
	
	private LayoutInflater inflater = null;
	private List<Event> mClubEventList = null;
	private Club mClub = null;
	private Bar mBar = null;
	private RefreshAdapter mRefreshAdapter;
	
	private int width;
	private int height;
	private int margin0;
	private int margin3;
	private int margin5;
	private int margin15;
	
	private Context mContext;
	
	private int mAddressType = Constants.ADDRESS_TYPE_ROAD;
	
	public ClubDetailAdapter(LayoutInflater layoutInflater, List<Event> clubEventList, Club club, Bar bar, RefreshAdapter refreshAdapter) {
		inflater = layoutInflater;
		mContext = layoutInflater.getContext();
		mClubEventList = clubEventList;
		mClub = club;
		mBar = bar; 
		Subway subway = bar.getAddress().getMap().getSubway();
		if(subway != null) {
			doesExistSubway = true;
		}
		mRefreshAdapter = refreshAdapter;
		
		DisplayMetrics matrix = mContext.getResources().getDisplayMetrics();
		width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, matrix);
		height = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, matrix);
		margin0 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, matrix);
		margin3 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, matrix);
		margin5 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, matrix);
		margin15 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, matrix);
		
	} 
	
	public List<Event> getData() {
		return mClubEventList;
	}

	@Override
	public int getItemViewType(int position) {
		int type = 0;
		
		if(doesExistSubway) {
			if(position >= types.length) {
				type = types[types.length-1];
			}else {
				type = types[position];
			}
		}else {
			if(position >= typesWithoutSubway.length) {
				type = typesWithoutSubway[typesWithoutSubway.length-1];
			}else {
				type = typesWithoutSubway[position];
			}
		}
		
		return type;
	}

	@Override
	public int getViewTypeCount() {
		int viewTypeCount =  TYPE_MAX_COUNT;
		
		if(doesExistSubway) {
			viewTypeCount =  TYPE_MAX_COUNT;
		}else {
			viewTypeCount =  TYPE_MAX_COUNT_WITHOUT_SUBWAY;
		}
		
		return viewTypeCount;
	}
	
	@Override
	public int getCount() {
		return mClubEventList.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		int type = getItemViewType(position);
		if(!doesExistSubway && type >= 3) {
			type =  type + 1;
		}
		int id = Integer.valueOf(mClub.getId());
		Subway subway = mBar.getAddress().getMap().getSubway();
		
		if (convertView == null) {
			holder = new ViewHolder();

			switch (type) {
				case TYPE_CLUB_BANNER:
					convertView = inflater.inflate(R.layout.list_item_detail_banner, parent, false);
					holder.clubBannerIV = (ImageView) convertView.findViewById(R.id.club_detail_image);
					break;
				case TYPE_HOMEPAGE:
					convertView = inflater.inflate(R.layout.list_item_detail_homepage, parent, false);
					holder.homepageTV = (TextView) convertView.findViewById(R.id.homepage);
					break;
				case TYPE_BAR:
					convertView = inflater.inflate(R.layout.list_item_detail_bar, parent, false);
					holder.barTV = (TextView) convertView.findViewById(R.id.bar);
					holder.barDetailBtn = (Button) convertView.findViewById(R.id.bar_detail);
					holder.barDetailBtn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							Intent intent = new Intent(mContext, BarDetailActivity.class);
							intent.putExtra(Constants.BAR_ID, mBar.getId());
							mContext.startActivity(intent); 
						}
					});
					break;
				case TYPE_SUBWAY:
					convertView = inflater.inflate(R.layout.list_item_detail_subway, parent, false);
					holder.subwaysLL = (LinearLayout)convertView.findViewById(R.id.subways);
					break;
				case TYPE_ADDRESS:
					convertView = inflater.inflate(R.layout.list_item_detail_address, parent, false);
					holder.addressTV = (TextView) convertView.findViewById(R.id.address);
					holder.changeAddressBtn = (Button) convertView.findViewById(R.id.change_address);
					holder.changeAddressBtn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							ViewGroup viewGroup = (ViewGroup)view.getParent();
							Button changeAddress = (Button)view.findViewById(R.id.change_address);
							TextView addressTV = (TextView)viewGroup.findViewById(R.id.address);
							 
							Log.d(TAG, "mAddressType = " + mAddressType);
							 
							if(mAddressType == Constants.ADDRESS_TYPE_ROAD) {
//								changeAddress.setText(getResources().getString(R.string.list_item_address_road));
								addressTV.setText(mBar.getAddress().getLand());
								mAddressType = Constants.ADDRESS_TYPE_LAND; 
							}else { 
//								changeAddress.setText(getResources().getString(R.string.list_item_address_land));
								addressTV.setText(mBar.getAddress().getRoad());
								mAddressType = Constants.ADDRESS_TYPE_ROAD;
							} 
						}
					});
					break;
				case TYPE_MAP:
					convertView = inflater.inflate(R.layout.list_item_detail_map, parent, false);
					holder.showMapBtn = (Button) convertView.findViewById(R.id.show_map);
					holder.showMapBtn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							Log.d(TAG, "mAddressType : " + mAddressType);
							Intent intent = new Intent(mContext, NaverMapActivity.class);
							intent.putExtra(Constants.BAR_ID, mBar.getId());
							intent.putExtra(Constants.ADDRESS_TYPE, mAddressType);
							mContext.startActivity(intent);
						}
					});
					break;
				case TYPE_ADMIN:
					convertView = inflater.inflate(R.layout.list_item_detail_admin, parent, false);
					holder.adminNicknameTV = (TextView) convertView.findViewById(R.id.admin_nickname);
					holder.requestAdminBtn = (Button) convertView.findViewById(R.id.request_admin_button);
					holder.requestAdminBtn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							Toast.makeText(view.getContext(), R.string.toast_under_construction, Toast.LENGTH_LONG).show();
//							Intent intent = new Intent(mContext, EmailRegistrationActivity.class);
//							intent.putExtra(Constants.GROUP_TYPE, Constants.CLUB);
//							intent.putExtra(Constants.GROUP_ID, mClub.getId());
//							startActivity(intent);
						}
					});
					break;
				case TYPE_CLUB_NOTICE:
					convertView = inflater.inflate(R.layout.list_item_detail_notice, parent, false);
					break;
				case TYPE_NOTICE_ITEM:
					convertView = inflater.inflate(R.layout.list_item_detail_notice_item, parent, false);
					holder.eventTitleTV = (TextView) convertView.findViewById(R.id.title);
					holder.eventThumbnailIV = (ImageView) convertView.findViewById(R.id.event_image);
					holder.eventDescriptionTV = (TextView) convertView.findViewById(R.id.description);
					holder.eventDateTV = (TextView) convertView.findViewById(R.id.date);
					holder.newIcon = (ImageView) convertView.findViewById(R.id.icon_new);
					holder.favoriteIcon = (ImageView) convertView.findViewById(R.id.icon_favorite);
					holder.favorite = (View) convertView.findViewById(R.id.favorite);
					holder.favorite.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							View parentView = (View)view.getParent().getParent(); 
							Event event = (Event)parentView.getTag(R.id.tag_event); 
							if(event != null) {
								L.d(TAG, "event id : " + event.getId() + " , title : " + event.getTitle());
								if(event.isFavorite()) {
									removeFavorite(event);
									Toast.makeText(view.getContext(), R.string.do_unfavorite, Toast.LENGTH_LONG).show();
								}else {
									Toast.makeText(view.getContext(), R.string.do_favorite, Toast.LENGTH_LONG).show(); 
									addFavorite(event); 
								}
							}
						}
					});
					break;
			}
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder)convertView.getTag();
		}

		switch (type) {
			case TYPE_CLUB_BANNER:
				ThumbnailHolder bannerHolder = ThumbnailHolder.getInstance();
				Bitmap banner = bannerHolder.retrieve(mClub.getBannerUrl());
				// 1. cache에 이미지가 있다면 표시한다.
				if(banner != null) {
					holder.clubBannerIV.setImageBitmap(banner);
				}else {
				// 2. 정보가 수정되었다면 웹에서 이미지를 불러오고 cache와 file에 저장한다.
					if(isUpdated(mClub)) {
						getImageFromWeb(mClub.getBannerUrl(), id);
					}else {
				// 3. 정보가 수정되지 않았다면 file에서 이미지를 불러온다.
						banner = loadImageFromFile(id);
						if(banner == null) {
							// file에서 이미지가 없다면 기본으로 설정된 이미지를 불러온다.
							banner = BitmapFactory.decodeResource(mContext.getResources(), ImageResource.CLUB_DETAIL[id]);
							String fileName = Constants.CLUB + Constants.DETAIL + id + ".png";
							Util.saveBitmapToFile(banner, fileName, mContext);
						}
						holder.clubBannerIV.setImageBitmap(banner);
						bannerHolder.save(mClub.getBannerUrl(), banner);
					}
				}
				break;
			case TYPE_HOMEPAGE:
				String homePage = mClub.getHomePage();
				holder.homepageTV.setText(homePage);
				break;
			case TYPE_BAR:
				String nameKorStr = mBar.getNameKor();
				String nameEngStr = mBar.getNameEng().replace(" ", "");
				if(nameEngStr.isEmpty()) {
					holder.barTV.setText(nameKorStr);
				}else { 
					holder.barTV.setText(nameKorStr + " (" + mBar.getNameEng() + ")");
				}			
				break;					
			case TYPE_SUBWAY:
				if(subway != null) {
					holder.subwaysLL.removeAllViews();
					
					List<Line> lines = subway.getLines();
					for (Line line : lines) {
						ImageView subwayIV = new ImageView(inflater.getContext());
						subwayIV.setImageResource(ImageResource.SUBWAY[line.getId()]);
						LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, height);
						lp.rightMargin = margin5;
						holder.subwaysLL.addView(subwayIV, lp);
					}
					
					Station station = getStation(subway);
					
					TextView nameTV = new TextView(inflater.getContext());
					nameTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
					nameTV.setTextColor(mContext.getResources().getColor(R.color.black));
					nameTV.setText(station.getName());
					LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					lp.leftMargin = margin3;
					holder.subwaysLL.addView(nameTV, lp);
					
					TextView exitTV = new TextView(inflater.getContext());
					exitTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
					exitTV.setTextColor(mContext.getResources().getColor(R.color.black));
					exitTV.setText(station.getExit() + mContext.getResources().getString(R.string.str_exit));
					LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					lp2.leftMargin = margin5;
					holder.subwaysLL.addView(exitTV, lp2); 
					holder.subwaysLL.setGravity(Gravity.CENTER_VERTICAL);
				}
				break;
			case TYPE_ADDRESS: 
				if(Constants.ADDRESS_TYPE_ROAD == mAddressType) { 
					holder.addressTV.setText(mBar.getAddress().getRoad());  
				}else {
					holder.addressTV.setText(mBar.getAddress().getLand());  
				}
				break;
			case TYPE_MAP:
				break;
			case TYPE_ADMIN:
				holder.adminNicknameTV.setText("");  
				break;
			case TYPE_CLUB_NOTICE:
				break;
			case TYPE_NOTICE_ITEM:
				Event event = mClubEventList.get(position); 
				convertView.setTag(R.id.tag_event, event); 
				
				if(event != null && holder.eventThumbnailIV != null) {
					String url = event.getThumbnailUrl();
					holder.eventThumbnailIV.setTag(url);
					
					ThumbnailHolder thumbnailHolder = ThumbnailHolder.getInstance();
					Bitmap thumbnail = thumbnailHolder.retrieve(url);
					
					if(thumbnail != null) {
						holder.eventThumbnailIV.setBackgroundColor(0);
						holder.eventThumbnailIV.setImageBitmap(thumbnail); 
					}else {
						if(!thumbnailHolder.isInTask(url)) {
							DownloadImagesTask task = new DownloadImagesTask(mRefreshAdapter);
							thumbnailHolder.setTask(url, task);
							task.execute(url);
						} 
					}
					
					if(event.isFavorite()) {
						holder.favoriteIcon.setImageResource(R.drawable.icon_favorite_fill);
					}else {
						holder.favoriteIcon.setImageResource(R.drawable.icon_unfavorite);
					}
					
					holder.eventTitleTV.setText(event.getTitle()); 
					LinearLayout.LayoutParams lp = (android.widget.LinearLayout.LayoutParams) holder.eventTitleTV.getLayoutParams();
					if(event.isNew()) {
						lp.leftMargin = margin15;
						holder.newIcon.setVisibility(View.VISIBLE);
					}else {
						lp.leftMargin = margin0;
						holder.newIcon.setVisibility(View.GONE);
					}  
					holder.eventTitleTV.setLayoutParams(lp);
					 
					holder.eventDescriptionTV.setText(event.getSummary());
					if(event.getPeriod() != null) {
						holder.eventDateTV.setText(event.getPeriod().getFrom().getDate()); 
					}
				}
				break;
		}
		
		return convertView;
	}

	private Bitmap loadImageFromFile(int id) {
		String fileName = Constants.CLUB + Constants.DETAIL + id + ".png";
		
		Bitmap bitmap = Util.loadBitmapFromFile(fileName, mContext);
		
		return bitmap;
	}
	
	private void getImageFromWeb(String url, int id) {
		ThumbnailHolder thumbnailHolder = ThumbnailHolder.getInstance();
		if(!thumbnailHolder.isInTask(url)) {
			String fileName = Constants.CLUB + Constants.DETAIL + id + ".png";
			DownloadImagesTask task = new DownloadImagesTask(mRefreshAdapter, fileName, mContext);
			thumbnailHolder.setTask(url, task);
			task.execute(url);
		}
	}
	
	private boolean isUpdated(Club club) {
		boolean isUpdated = false;
		SharedPreferences prefs = mContext.getSharedPreferences(Constants.CLUB_AND_BAR_PREF, Context.MODE_PRIVATE);
    	Date lastUpdateDate = club.getLastUpdateDate();
    	if(lastUpdateDate != null) {
    		long updateDate = lastUpdateDate.getTime();
    		long savedDate = prefs.getLong(Constants.CLUB_DETAIL_LAST_UPDATED_DATE + club.getId(), 0L);
    		if(updateDate != savedDate) {
    			isUpdated = true;
    			
    			SharedPreferences.Editor editor = prefs.edit();
				editor.putLong(Constants.CLUB_DETAIL_LAST_UPDATED_DATE + club.getId(), updateDate);
				editor.commit();
    		}
    	}
    	
    	return isUpdated;
	}
	
	private Station getStation(Subway subway) {
		Station station = null;
		
		if(subway != null) {
			List<Station> stationList = subway.getStations();
			if(stationList != null) {
				for (Station aStation : stationList) {
					if(aStation != null) {
						station  = aStation;
						break;
					}
				} 
			}
		}
		
		return station;
	}
	
	public void addFavorite(Event event) {
		if(event == null) {
			L.d(TAG, "addFavorite() : event is null.");
			return;
		}
		
		String id = event.getId();
		boolean isNew = event.isNew();
		
		for (Event aEvent : mClubEventList) {
			if(id.equals(aEvent.getId())) {
				aEvent.setFavorite(true);
				aEvent.setNew(isNew);
			}
		}
		
		EventHolder.getInstance().saveEventFavorite(id, true);
		
		new FavoriteParser(Constants.EVENT, Constants.ACTION_FOLLOW, id).execute();
		
		mRefreshAdapter.refreshAdapter();
	}
	
	public void removeFavorite(Event event) {
		if(event == null) {
			L.d(TAG, "removeFavorite() : event is null.");
			return;
		}
		
		String id = event.getId();
		boolean isNew = event.isNew();
		
		for (Event aEvent : mClubEventList) {
			if(id.equals(aEvent.getId())) {
				aEvent.setFavorite(false);
				aEvent.setNew(isNew);
			}
		}
		
		EventHolder.getInstance().saveEventFavorite(id, false);
		
		new FavoriteParser(Constants.EVENT, Constants.ACTION_UNFOLLOW, id).execute();
			
		mRefreshAdapter.refreshAdapter();
	}
	
	public static class ViewHolder {
		public ImageView clubBannerIV;
		public TextView homepageTV;
		public TextView barTV;
		public Button barDetailBtn;
		public LinearLayout subwaysLL;
		public Button changeAddressBtn;
		public Button showMapBtn;
		public Button requestAdminBtn;
		public TextView addressTV;
		public TextView eventTitleTV;
		public ImageView eventThumbnailIV;
		public TextView eventDescriptionTV;
		public TextView eventDateTV;
		public TextView adminNicknameTV;
		public ImageView favoriteIcon;
		public View favorite;
		public ImageView newIcon;
	}
}
