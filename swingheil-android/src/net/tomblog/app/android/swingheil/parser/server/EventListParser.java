package net.tomblog.app.android.swingheil.parser.server;

import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import net.tomblog.app.android.swingheil.SwingHeilApplication;
import net.tomblog.app.android.swingheil.parser.RefreshEventList;
import net.tomblog.app.android.swingheil.provider.EventDBHelper;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.EventHolder;
import net.tomblog.app.android.swingheil.util.L;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.swingheil.domain.event.Event;

/**
 * 이벤트 목록을 파싱하는 클래스
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class EventListParser extends AsyncTask<String, String, List<Event>> implements BasicParser {

	private static final String TAG = EventListParser.class.getName();
	
	private Context mContext = null;
	private RefreshEventList mRefreshEventList = null;  
	private String mCategory = null;
	private int mPage = 1;
	private String mLimit = null;
	private String mId = null;
	
	public EventListParser(Context context, RefreshEventList refreshEventList, String category, int page) {
		mContext = context;
		mRefreshEventList = refreshEventList;
		mCategory = category;
		mLimit = null;
		mPage = page;
	}
	
    public EventListParser(Context context, RefreshEventList refreshEventList, String category, String limit, int page) {
		mContext = context;
		mRefreshEventList = refreshEventList;
		mCategory = category;
		mLimit = limit;
		mPage = page;
	}
    
    public EventListParser(Context context, RefreshEventList refreshEventList, String category, int page, String id) {
    	mContext = context;
    	mRefreshEventList = refreshEventList;
    	mCategory = category;
    	mLimit = null;
    	mPage = page;
    	mId = id;
    }
    
    public EventListParser(Context context, RefreshEventList refreshEventList, String category, String limit, int page, String id) {
    	mContext = context;
    	mRefreshEventList = refreshEventList;
    	mCategory = category;
    	mLimit = limit;
    	mPage = page;
    	mId = id;
    }

	@Override
	protected List<Event> doInBackground(String... args) {
		L.d(TAG, " >>> called doInBackground()");
		
        List<Event> eventList = null; 
		try {
			String url = null;
			
			HttpClient httpclient = new DefaultHttpClient();
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add( new BasicNameValuePair(USER_ID, SwingHeilApplication.getInstance().getUserId()));
			params.add( new BasicNameValuePair(PAGE, String.valueOf(mPage))); 
			if(mLimit == null) {
				mLimit = DEFAULT_LIMIT;
			}
			params.add( new BasicNameValuePair(LIMIT, mLimit));
			if(Constants.SWING_ALL.equals(mCategory)) {
				url = REST_API_BASE_URL + "/event/getEventList/";
			}else if(Constants.SWING_HEIL.equals(mCategory)) {
				url = REST_API_BASE_URL + "/event/getSwingheilEventList/";
			}else if(Constants.SWING_CLUB.equals(mCategory)) {
				url = REST_API_BASE_URL + "/event/getClubEventList/";
				params.add( new BasicNameValuePair(CLUB_ID, mId));
			}else if(Constants.SWING_BAR.equals(mCategory)) {
				url = REST_API_BASE_URL + "/event/getBarEventList/";
				params.add( new BasicNameValuePair(BAR_ID, mId));
			}else if(Constants.SWING_FAVORITE.equals(mCategory)) {
				url = REST_API_BASE_URL + "/event/getFavoriteEventList/"; 
			}else {
				return null;
			}
			
			URI uri = new URI(url + "?" + URLEncodedUtils.format(params, "utf-8"));
			L.i(TAG, "uri : " + uri.toString());
			HttpUriRequest request = new HttpGet(uri);
			HttpResponse response = httpclient.execute(request);
			HttpEntity responseEntity = response.getEntity();
			InputStream source = responseEntity.getContent();
			StringWriter writer = new StringWriter();
			IOUtils.copy(source, writer, "UTF-8");
			String jsonData = writer.toString();
			
			JSONObject reader = new JSONObject(jsonData);
			String eventJson = reader.getString(Constants.JSON_RESULTS);
//			L.d(TAG, eventJson);  
			if(!eventJson.isEmpty()) {
				EventDBHelper db = new EventDBHelper(mContext);
				
				GsonBuilder builder = new GsonBuilder();
				builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
					@Override
					public Date deserialize(JsonElement json, Type typeOfT,
							JsonDeserializationContext context) throws JsonParseException {
						return json == null ? null : new Date(json.getAsLong());
					}
				});
				Gson gson = builder.create();  
				Type type = new TypeToken<List<Event>>(){}.getType();
				eventList  = gson.fromJson(eventJson, type);
				
				if(eventList != null) { 
					EventHolder holder = EventHolder.getInstance();
					
					for (Event event : eventList) {
						Event savedEvent = db.getEvent(event.getId());
						if(savedEvent == null && isNew(event)) {
							event.setNew(true);
						} 
						
						holder.saveEventFavorite(event.getId(), event.isFavorite());
					} 
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		return eventList;
	}
    
    private boolean isNew(Event event) {
    	boolean isNew = false;
    	
    	if(event != null) {
    		Calendar calendar = Calendar.getInstance();
    		calendar.add(Calendar.DAY_OF_MONTH, Constants.NEW_DAY_LIMIT);
    		Date standardDate = calendar.getTime();
    		Date regDate = event.getRegDate();
    		
    		isNew = regDate.after(standardDate);
    	}
		
		return  isNew; 
	}
    
    @Override
    protected void onPostExecute(List<Event> eventList) {
    	super.onPostExecute(eventList);
    	
    	if(eventList != null) {
			mRefreshEventList.refresh(eventList);
		}
    }
}