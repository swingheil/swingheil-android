package net.tomblog.app.android.swingheil.parser.server.user;

import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import net.tomblog.app.android.swingheil.parser.RefreshUserInfo;
import net.tomblog.app.android.swingheil.parser.server.BasicParser;
import net.tomblog.app.android.swingheil.util.L;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.swingheil.domain.user.SocialUser;

/**
 * DeviceUid로 사용자 정보를 가져오는 API
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class GetUserParser extends AsyncTask<String, String, SocialUser> implements BasicParser {

	private static final String TAG = GetUserParser.class.getName();
	
	/*	/rest/user/getUserByDeviceUid 호출 결과 JSON
	{
	  "success": true,
	  "message": {
	    "id": "10",
	    "nickname": "Tom",
	    "devices": [
	      {
	        "deviceUid": "355999051126385",
	        "modelId": "SHV-E300K",
	        "osVersion": "KOT49H 4.4.2",
	        "registrationId": "APA91bFSPXTXuYqqaIYlhOUQnm1unHMnNFg8_3EGIhmKpIV-hxs8ITy-mhZGZTxOqecWaHs5Pl9RNzJ0-_QMPSNfNMqgrehS7NtfSt_EjMNNKr0J5EbklJ6BUB9_w5BMUeE_o4bj-eKwNnoSGBGrSV3KoaShIkxyMRx8HyiRsU9GjndKpSi0Lb8"
	      }
	    ],
	    "name": null,
	    "email": null,
	    "phone": null,
	    "password": null
	  }
	}
	 */
	
	private String mDeviceUid = null;
	RefreshUserInfo mRefreshUserInfo = null;
	
	/**
	 *  DeviceUid로 사용자 정보를 가져오는 API URL
	 */
    public static final String GET_USER_URL = REST_API_BASE_URL + "/user/getUserByDeviceUid";
	
    public GetUserParser(String deviceUid, RefreshUserInfo refreshUserInfo) {
    	mDeviceUid = deviceUid;
    	mRefreshUserInfo = refreshUserInfo;
    }

    @Override
	protected SocialUser doInBackground(String... args) {
		L.d(TAG, " >>> called doInBackground()");
		
		SocialUser socialUser = null;
        
		try {
			HttpClient httpclient = new DefaultHttpClient();
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(DEVICE_UID, mDeviceUid));
			
			URI uri = new URI(GET_USER_URL + "?" + URLEncodedUtils.format(params, "utf-8"));
			L.i(TAG, "uri : " + uri.toString());
			HttpUriRequest request = new HttpGet(uri);
			HttpResponse response = httpclient.execute(request);
			HttpEntity responseEntity = response.getEntity();
			InputStream source = responseEntity.getContent();
			StringWriter writer = new StringWriter();
			IOUtils.copy(source, writer, "UTF-8");
			String jsonData = writer.toString();
			
			L.d(TAG, "responseBody : \n" + jsonData);
			socialUser = parseSocialUser(jsonData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		return socialUser;
	}
    
    @Override
    protected void onPostExecute(SocialUser socialUser) {
    	L.d(TAG, " >>> called doInBackground()");
    	mRefreshUserInfo.refreshUserInfo(socialUser);
    }
    
	private SocialUser parseSocialUser(String data) {
		SocialUser socialUser = null;
		try {
			JSONObject reader = new JSONObject(data);
			boolean success = reader.getBoolean(SUCCESS);
			Log.d(TAG, "success : " + success);
			
			if(success) { 
				String message = reader.getString(MESSAGE);
				Log.d(TAG, "message : " + message);
				
				Gson gson = new GsonBuilder().create();
				socialUser = gson.fromJson(message, SocialUser.class);  
				
				Log.d(TAG, "socialUser : " + socialUser);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		return socialUser;
	}

}