package net.tomblog.app.android.swingheil.parser.server;

import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import net.tomblog.app.android.swingheil.SwingHeilApplication;
import net.tomblog.app.android.swingheil.parser.RefreshBarList;
import net.tomblog.app.android.swingheil.util.ClubAndBarHolder;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.L;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.swingheil.domain.group.Bar;

/**
 * 바 목록을 파싱하는 클래스
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class BarListParser extends AsyncTask<String, String, List<Bar>> implements BasicParser {

	private static final String TAG = BarListParser.class.getName();
	
	private Context mContext = null;
	private RefreshBarList mRefreshList = null;  
	private int mPage = 1;
	
    public BarListParser(Context context, RefreshBarList refreshList) {
		mContext = context;
		mRefreshList = refreshList;
	}
    
	@Override
	protected List<Bar> doInBackground(String... args) {
		L.d(TAG, " >>> called doInBackground()");
		
        List<Bar> barList = null; 
		try {
			String url = null;
			
			HttpClient httpclient = new DefaultHttpClient();
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add( new BasicNameValuePair(USER_ID, SwingHeilApplication.getInstance().getUserId()));
			params.add( new BasicNameValuePair(PAGE, String.valueOf(mPage)));
			params.add( new BasicNameValuePair(LIMIT, "100"));
			url = REST_API_BASE_URL + "/bar/getBarList/"; 
			
			URI uri = new URI(url + "?" + URLEncodedUtils.format(params, "utf-8"));
//			L.i(TAG, "uri : " + uri.toString());
			HttpUriRequest request = new HttpGet(uri);
			HttpResponse response = httpclient.execute(request);
			HttpEntity responseEntity = response.getEntity();
			InputStream source = responseEntity.getContent();
			StringWriter writer = new StringWriter();
			IOUtils.copy(source, writer, "UTF-8");
			String jsonData = writer.toString();
			
			JSONObject reader = new JSONObject(jsonData);
			String barJson = reader.getString(Constants.JSON_RESULTS);
//			L.d(TAG, barJson);  
			if(!barJson.isEmpty()) {
				SharedPreferences prefs = mContext.getSharedPreferences(Constants.CLUB_AND_BAR_PREF, Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString(Constants.JSON_BARS, barJson);
				editor.putString(Constants.IS_THERE_BAR_INFO, "Y");
				editor.commit();
				
				GsonBuilder builder = new GsonBuilder(); 
				builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
					@Override
					public Date deserialize(JsonElement json, Type typeOfT,
							JsonDeserializationContext context) throws JsonParseException {
						return json == null ? null : new Date(json.getAsLong());
					}
				});
				Gson gson = builder.create();  
				Type type = new TypeToken<List<Bar>>(){}.getType();
				barList  = gson.fromJson(barJson, type);
				
				if(barList != null) { 
					ClubAndBarHolder holder = ClubAndBarHolder.getInstance();
					for (Bar bar : barList) {
						if(isUpdated(bar) && isNew(bar)) {
							bar.setNew(true);
						} 
						holder.saveBar(bar.getId(), bar);
					} 
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		return barList;
	}
    
    private boolean isNew(Bar bar) {
    	boolean isNew = false;
    	
    	if(bar != null) {
    		try {
    			Calendar calendar = Calendar.getInstance();
    			calendar.add(Calendar.DAY_OF_MONTH, Constants.NEW_DAY_LIMIT);
    			Date standardDate = calendar.getTime();
    			Date regDate = bar.getLastUpdateDate();
    			
    			isNew = regDate.after(standardDate);
			} catch (Exception e) {
				L.e(TAG, "lastUpdateDate parsing error.");  
			}
    	}
		
		return  isNew; 
	}
    
    private boolean isUpdated(Bar bar) {
		boolean isUpdated = false;
		
		SharedPreferences prefs = mContext.getSharedPreferences(Constants.EVENT_PREF, Context.MODE_PRIVATE);
    	Date lastUpdateDate = bar.getLastUpdateDate();
    	if(lastUpdateDate != null) {
    		long updateDate = lastUpdateDate.getTime();
    		long savedDate = prefs.getLong(Constants.BAR_ID + bar.getId(), 0L);
    		if(updateDate != savedDate) {
    			isUpdated = true;
    		}
    	}
    	
    	return isUpdated;
	}
    
    @Override
    protected void onPostExecute(List<Bar> barList) {
    	super.onPostExecute(barList);
    	
    	if(barList != null && mRefreshList != null) { 
    		mRefreshList.refreshList();
		}
    }
}