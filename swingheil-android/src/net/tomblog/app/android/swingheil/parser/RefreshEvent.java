package net.tomblog.app.android.swingheil.parser;

import com.swingheil.domain.event.Event;

public interface RefreshEvent {
	public void refresh(Event event);
}
