package net.tomblog.app.android.swingheil.parser.server;

import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.tomblog.app.android.swingheil.SwingHeilApplication;
import net.tomblog.app.android.swingheil.parser.RefreshEvent;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.EventHolder;
import net.tomblog.app.android.swingheil.util.L;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.swingheil.domain.event.Event;

/**
 * 이벤트를 파싱하는 클래스
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class EventParser extends AsyncTask<String, String, Event> implements BasicParser {

	private static final String TAG = EventParser.class.getName();
	
	private RefreshEvent mRefreshEvent = null;  
	private String mEventId = null;
	
	/**
	 *  이벤트 목록 조회 URL
	 */
    public EventParser(RefreshEvent refreshEvent, String eventId) {
		mRefreshEvent = refreshEvent;
		mEventId = eventId;
	}

	@Override
	protected Event doInBackground(String... args) {
		L.d(TAG, " >>> called doInBackground()");
		
        Event event = null; 
		try {
			String url = null;
			
			HttpClient httpclient = new DefaultHttpClient();
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add( new BasicNameValuePair(USER_ID, SwingHeilApplication.getInstance().getUserId()));
			params.add( new BasicNameValuePair(EVENT_ID, mEventId));
			url = REST_API_BASE_URL + "/event/getEvent/";
			
			URI uri = new URI(url + "?" + URLEncodedUtils.format(params, "utf-8"));
			L.i(TAG, "uri : " + uri.toString());
			HttpUriRequest request = new HttpGet(uri);
			HttpResponse response = httpclient.execute(request);
			HttpEntity responseEntity = response.getEntity();
			InputStream source = responseEntity.getContent();
			StringWriter writer = new StringWriter();
			IOUtils.copy(source, writer, "UTF-8");
			String eventJson = writer.toString();
			L.i(TAG, "eventJson : " + eventJson);
			if(!eventJson.isEmpty()) {
				
				GsonBuilder builder = new GsonBuilder();
				builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
					@Override
					public Date deserialize(JsonElement json, Type typeOfT,
							JsonDeserializationContext context) throws JsonParseException {
						return json == null ? null : new Date(json.getAsLong());
					}
				});
				Gson gson = builder.create();  
				Type type = new TypeToken<Event>(){}.getType();
				event  = gson.fromJson(eventJson, type);
				
				if(event != null) { 
					EventHolder holder = EventHolder.getInstance();
					holder.saveEventFavorite(event.getId(), event.isFavorite());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		return event;
	}
    
    @Override
    protected void onPostExecute(Event event) {
    	super.onPostExecute(event);
    	
    	if(event != null) {
			mRefreshEvent.refresh(event);
		}
    }
}