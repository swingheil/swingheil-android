package net.tomblog.app.android.swingheil.parser.server.user;

import java.util.ArrayList;
import java.util.List;

import net.tomblog.app.android.swingheil.model.server.Device;
import net.tomblog.app.android.swingheil.parser.RefreshUserInfo;
import net.tomblog.app.android.swingheil.parser.server.BasicParser;
import net.tomblog.app.android.swingheil.util.L;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.swingheil.domain.user.SocialUser;

/**
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class RegisterDeviceParser extends AsyncTask<String, String, SocialUser> implements BasicParser {

	private static final String TAG = RegisterDeviceParser.class.getName();
	
	/* Device Register 호출 결과 JSON
	{
	  "success": true,
	  "message": {
	    "id": "4",
	    "nickname": "Hong",
	    "devices": [
	      {
	        "deviceUid": "deviceUid555",
	        "modelId": "modelId14555",
	        "osVersion": "osVersion124555",
	        "registrationId": "registrationId11555"
	      }
	    ],
	    "name": null,
	    "email": null,
	    "phone": null,
	    "password": null
	  }
	}
	 */
	public static final String NICKNAME = "nickname";
	public static final String DEVICE_UID = "deviceUid";
	public static final String MODEL_ID = "modelId";
	public static final String OS_VERSION = "osVersion";
	public static final String REGISTRAION_ID = "registrationId";
	public static final String SUCCESS = "success";
	public static final String MESSAGE = "message";
	
	public static final String USER_ID = "userId";
	
	private Device mDevice = null;
	RefreshUserInfo mRefreshUserInfo = null;
	boolean mWithoutRefreshUserInfo = false;
	
	/**
	 *  디바이스 등록용 URL
	 */
    public static final String DEVICE_REGISTER_URL = REST_API_BASE_URL + "/user/register";
	
    public RegisterDeviceParser(Device device, RefreshUserInfo refreshUserInfo, boolean withoutRefreshUserInfo) {
    	mDevice = device;
    	mRefreshUserInfo = refreshUserInfo;
    	mWithoutRefreshUserInfo = withoutRefreshUserInfo;
    }

    @Override
	protected SocialUser doInBackground(String... params) {
		L.d(TAG, " >>> called doInBackground()");
		
		SocialUser socialUser = null;
        
		try {
			HttpClient httpclient = new DefaultHttpClient();
			// specify the URL you want to post to
			HttpPost httppost = new HttpPost(DEVICE_REGISTER_URL);
			L.d(TAG, "url : " + DEVICE_REGISTER_URL);
			
			// create a list to store HTTP variables and their values
			List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
			// add an HTTP variable and value pair
			nameValuePairs.add(new BasicNameValuePair(NICKNAME, mDevice.getNickname()));
			nameValuePairs.add(new BasicNameValuePair(DEVICE_UID, mDevice.getDeviceUid()));
			nameValuePairs.add(new BasicNameValuePair(MODEL_ID, mDevice.getModelId()));
			nameValuePairs.add(new BasicNameValuePair(OS_VERSION, mDevice.getOsVersion()));
			nameValuePairs.add(new BasicNameValuePair(REGISTRAION_ID, mDevice.getRegistrationId()));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

			// send the variable and value, in other words post, to the URL
			HttpResponse response = httpclient.execute(httppost);
			
			String responseBody = EntityUtils.toString(response.getEntity());
			
			L.d(TAG, "responseBody : \n" + responseBody);
			socialUser = parseSocialUser(responseBody);
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		return socialUser;
	}
    
    @Override
    protected void onPostExecute(SocialUser socialUser) {
    	L.d(TAG, " >>> called doInBackground()");
    	if(mWithoutRefreshUserInfo) {
    		mRefreshUserInfo.refreshUserInfo(socialUser); 
    	}
    }
    
	private SocialUser parseSocialUser(String data) {
		SocialUser socialUser = null;
		try {
			JSONObject reader = new JSONObject(data);
			boolean success = reader.getBoolean(SUCCESS);
			Log.d(TAG, "success : " + success);
			
			if(success) { 
				String message = reader.getString(MESSAGE);
				Log.d(TAG, "message : " + message);
				
				Gson gson = new GsonBuilder().create();
				socialUser = gson.fromJson(message, SocialUser.class);  
				
				Log.d(TAG, "socialUser : " + socialUser);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return socialUser;
	}

}