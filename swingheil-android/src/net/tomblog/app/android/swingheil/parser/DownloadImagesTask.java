package net.tomblog.app.android.swingheil.parser;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import net.tomblog.app.android.swingheil.parser.server.BasicParser;
import net.tomblog.app.android.swingheil.util.L;
import net.tomblog.app.android.swingheil.util.ThumbnailHolder;
import net.tomblog.app.android.swingheil.util.Util;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

public class DownloadImagesTask extends AsyncTask<String, Void, Bitmap> implements BasicParser {
	
	private static final String TAG = DownloadImagesTask.class.getName();
	
	private String mUrl = null;
	private RefreshAdapter mRefreshAdapter = null;  
	private boolean needToSave = false;
	private String fileName = null;
	private Context context = null;

	public DownloadImagesTask(RefreshAdapter refreshAdapter) {
		this.mRefreshAdapter = refreshAdapter;
	}
	
	public DownloadImagesTask(RefreshAdapter refreshAdapter, String fileName, Context context) {
		this.mRefreshAdapter = refreshAdapter;
		this.needToSave = true;
		this.fileName = fileName;
		this.context = context;
	}

	@Override
	protected Bitmap doInBackground(String... args) {
		mUrl = args[0];
		return downloadImage(mUrl);
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		L.d(TAG, " >>> called onPostExecute() : " + mUrl);
		
		ThumbnailHolder thumbnailHolder = ThumbnailHolder.getInstance();
		thumbnailHolder.save(mUrl, result);
		thumbnailHolder.removeTask(mUrl);
		
		if(needToSave) {
			Util.saveBitmapToFile(result, fileName, context);
		}
		
		if(mRefreshAdapter != null) {
			mRefreshAdapter.refreshAdapter();
		}
	}

	private Bitmap downloadImage(String url) {
		Bitmap bitmap = null;
		try {
			URL fullUrl = new URL(IMAGE_BASE_URL + url);
			L.d(TAG, " >>> called downloadImage() : " + fullUrl);
			HttpURLConnection con = (HttpURLConnection) fullUrl.openConnection();
			InputStream is = con.getInputStream();
			bitmap = BitmapFactory.decodeStream(is);
			is.close();
			con.disconnect();
			if (bitmap != null) {
				L.d(TAG, " >>> bmp is loaded : " + fullUrl);
			} else {
				L.d(TAG, " >>> bmp is null : " + fullUrl);
			}
		} catch (Exception e) { 
			e.printStackTrace();
		}
		
		return bitmap;
	}

}
