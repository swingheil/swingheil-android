package net.tomblog.app.android.swingheil.parser.server;

import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import net.tomblog.app.android.swingheil.SwingHeilApplication;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.L;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.os.AsyncTask;

/**
 * Event, Bar, Club을 follow/unfollow 
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class FavoriteParser extends AsyncTask<Void, Void, Void> implements BasicParser {

	private static final String TAG = FavoriteParser.class.getName();
	
	/**
	 *  이벤트 목록 조회 URL
	 */
	private String mTarget = null;
	private String mAction = null;
    private String mId = null;
	 
    public FavoriteParser(String target, String action, String id) {
    	mAction = action;
    	mTarget = target;
    	mId = id;
	}

	@Override
	protected Void doInBackground(Void... arg0) {
		L.d(TAG, " >>> called doInBackground()");
		
		String apiUrl = null;
		String id = null;
    	if(Constants.CLUB.equalsIgnoreCase(mTarget)) {
    		apiUrl = REST_API_BASE_URL + "/club/" + mAction;
    		id = "clubId";
    	}else if(Constants.BAR.equals(mTarget)) {
    		apiUrl = REST_API_BASE_URL + "/bar/" + mAction;
    		id = "barId";
    	}else if(Constants.EVENT.equalsIgnoreCase(mTarget)) {
    		apiUrl = REST_API_BASE_URL + "/event/" + mAction;
    		id = "eventId";
    	}else {
    		return null;
    	}
    	
		try {
			HttpClient httpclient = new DefaultHttpClient();
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add( new BasicNameValuePair(USER_ID, SwingHeilApplication.getInstance().getUserId()));   
			params.add( new BasicNameValuePair(id, mId));  
			
			URI uri = new URI(apiUrl + "?" + URLEncodedUtils.format(params, "utf-8"));
			L.d(TAG, "uri : " + uri.toString());
			HttpUriRequest request = new HttpPost(uri);

			HttpResponse response = httpclient.execute(request);
			
			HttpEntity responseEntity = response.getEntity();
			InputStream source = responseEntity.getContent();
			
			StringWriter writer = new StringWriter();
			IOUtils.copy(source, writer, "UTF-8");
			String jsonData = writer.toString();
			
			JSONObject reader = new JSONObject(jsonData);
			String success = reader.getString(Constants.JSON_SUCCESS);
			String message = reader.getString(Constants.JSON_MESSAGE);

			L.e(TAG, "success : " + success);
			L.e(TAG, "message : " + message); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
    
}