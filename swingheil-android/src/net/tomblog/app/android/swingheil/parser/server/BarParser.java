package net.tomblog.app.android.swingheil.parser.server;

import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import net.tomblog.app.android.swingheil.SwingHeilApplication;
import net.tomblog.app.android.swingheil.parser.RefreshBar;
import net.tomblog.app.android.swingheil.util.ClubAndBarHolder;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.L;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.swingheil.domain.group.Bar;

/**
 * 바를 파싱하는 클래스
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class BarParser extends AsyncTask<String, String, Bar> implements BasicParser {

	private static final String TAG = BarParser.class.getName();
	
	private RefreshBar mRefreshBar = null;  
	private String mBarId = "";
	
    public BarParser(RefreshBar refreshBar, String barId) {
		mRefreshBar = refreshBar;
		mBarId = barId;
	}
    
	@Override
	protected Bar doInBackground(String... args) {
		L.d(TAG, " >>> called doInBackground()");
		
        Bar bar = null; 
		try {
			String url = null;
			
			HttpClient httpclient = new DefaultHttpClient();
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add( new BasicNameValuePair(USER_ID, SwingHeilApplication.getInstance().getUserId()));
			params.add( new BasicNameValuePair(BAR_ID, mBarId));
			url = REST_API_BASE_URL + "/bar/getBar/"; 
			
			URI uri = new URI(url + "?" + URLEncodedUtils.format(params, "utf-8"));
			L.i(TAG, "uri : " + uri.toString());
			HttpUriRequest request = new HttpGet(uri);
			HttpResponse response = httpclient.execute(request);
			HttpEntity responseEntity = response.getEntity();
			InputStream source = responseEntity.getContent();
			StringWriter writer = new StringWriter();
			IOUtils.copy(source, writer, "UTF-8");
			String jsonData = writer.toString();
			
			L.d(TAG, jsonData);  
			if(!jsonData.isEmpty()) {
				GsonBuilder builder = new GsonBuilder(); 
				builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
					@Override
					public Date deserialize(JsonElement json, Type typeOfT,
							JsonDeserializationContext context) throws JsonParseException {
						return json == null ? null : new Date(json.getAsLong());
					}
				});
				Gson gson = builder.create();  
				Type type = new TypeToken<Bar>(){}.getType();
				bar = gson.fromJson(jsonData, type);
				
				if(bar != null) { 
					ClubAndBarHolder holder = ClubAndBarHolder.getInstance();
					if(isNew(bar)) {
						bar.setNew(true);
					} 
					holder.saveBar(bar.getId(), bar);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		return bar;
	}
    
    private boolean isNew(Bar bar) {
    	boolean isNew = false;
    	
    	if(bar != null) {
    		try {
    			Calendar calendar = Calendar.getInstance();
    			calendar.add(Calendar.DAY_OF_MONTH, Constants.NEW_DAY_LIMIT);
    			Date standardDate = calendar.getTime();
    			Date regDate = bar.getLastUpdateDate();
    			
    			isNew = regDate.after(standardDate);
			} catch (Exception e) {
				L.e(TAG, "lastUpdateDate parsing error.");  
			}
    	}
		
		return  isNew; 
	}
    
    @Override
    protected void onPostExecute(Bar bar) {
    	super.onPostExecute(bar);
    	
    	if(bar != null) { 
    		mRefreshBar.refreshBar();
		}
    }
}