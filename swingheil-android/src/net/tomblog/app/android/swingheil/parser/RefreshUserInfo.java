package net.tomblog.app.android.swingheil.parser;

import com.swingheil.domain.user.SocialUser;

public interface RefreshUserInfo {
	public void refreshUserInfo(SocialUser socialUser); 
}
