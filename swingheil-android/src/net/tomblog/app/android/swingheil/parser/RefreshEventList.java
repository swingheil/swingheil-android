package net.tomblog.app.android.swingheil.parser;

import java.util.List;

import com.swingheil.domain.event.Event;

public interface RefreshEventList {
	public void refresh(List<Event> eventList);
}
