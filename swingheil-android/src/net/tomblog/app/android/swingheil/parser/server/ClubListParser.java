package net.tomblog.app.android.swingheil.parser.server;

import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import net.tomblog.app.android.swingheil.SwingHeilApplication;
import net.tomblog.app.android.swingheil.parser.RefreshClubList;
import net.tomblog.app.android.swingheil.util.ClubAndBarHolder;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.L;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.swingheil.domain.group.Club;

/**
 * 동호회 목록을 파싱하는 클래스
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class ClubListParser extends AsyncTask<String, String, List<Club>> implements BasicParser {

	private static final String TAG = ClubListParser.class.getName();
	
	private Context mContext = null;
	private RefreshClubList mRefreshList = null;  
	private int mPage = 1;
	
    public ClubListParser(Context context, RefreshClubList refreshList) {
		mContext = context;
		mRefreshList = refreshList;
	}
    
	@Override
	protected List<Club> doInBackground(String... args) {
		L.d(TAG, " >>> called doInBackground()");
		
        List<Club> clubList = null; 
		try {
			String url = null;
			
			HttpClient httpclient = new DefaultHttpClient();
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add( new BasicNameValuePair(USER_ID, SwingHeilApplication.getInstance().getUserId()));
			params.add( new BasicNameValuePair(PAGE, String.valueOf(mPage)));
			params.add( new BasicNameValuePair(LIMIT, "100"));
			url = REST_API_BASE_URL + "/club/getClubList/"; 
			
			URI uri = new URI(url + "?" + URLEncodedUtils.format(params, "utf-8"));
//			L.i(TAG, "uri : " + uri.toString());
			HttpUriRequest request = new HttpGet(uri);
			HttpResponse response = httpclient.execute(request);
			HttpEntity responseEntity = response.getEntity();
			InputStream source = responseEntity.getContent();
			StringWriter writer = new StringWriter();
			IOUtils.copy(source, writer, "UTF-8");
			String jsonData = writer.toString();
			
			JSONObject reader = new JSONObject(jsonData);
			String clubJson = reader.getString(Constants.JSON_RESULTS);
//			L.d(TAG, clubJson);  
			if(!clubJson.isEmpty()) {
				SharedPreferences prefs = mContext.getSharedPreferences(Constants.CLUB_AND_BAR_PREF, Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString(Constants.JSON_CLUBS, clubJson);
				editor.putString(Constants.IS_THERE_CLUB_INFO, "Y");
				editor.commit();
				
				GsonBuilder builder = new GsonBuilder(); 
				builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
					@Override
					public Date deserialize(JsonElement json, Type typeOfT,
							JsonDeserializationContext context) throws JsonParseException {
						return json == null ? null : new Date(json.getAsLong());
					}
				});
				Gson gson = builder.create();  
				Type type = new TypeToken<List<Club>>(){}.getType();
				clubList  = gson.fromJson(clubJson, type);
				
				if(clubList != null) { 
					ClubAndBarHolder holder = ClubAndBarHolder.getInstance();
					for (Club club : clubList) {
						if(isUpdated(club) && isNew(club)) {
							club.setNew(true);
						} 
						holder.saveClub(club.getId(), club);
						holder.saveBarOfClub(club);
					} 
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		return clubList;
	}
    
    private boolean isNew(Club club) {
    	boolean isNew = false;
    	
    	if(club != null) {
    		try {
    			Calendar calendar = Calendar.getInstance();
    			calendar.add(Calendar.DAY_OF_MONTH, Constants.NEW_DAY_LIMIT);
    			Date standardDate = calendar.getTime();
    			Date regDate = club.getLastUpdateDate();
    			
    			isNew = regDate.after(standardDate);
			} catch (Exception e) {
				L.e(TAG, "lastUpdateDate parsing error.");  
			}
    	}
		
		return  isNew; 
	}
    
    private boolean isUpdated(Club club) {
		boolean isUpdated = false;
		
		SharedPreferences prefs = mContext.getSharedPreferences(Constants.EVENT_PREF, Context.MODE_PRIVATE);
    	Date lastUpdateDate = club.getLastUpdateDate();
    	if(lastUpdateDate != null) {
    		long updateDate = lastUpdateDate.getTime();
    		long savedDate = prefs.getLong(Constants.CLUB_ID + club.getId(), 0L);
    		if(updateDate != savedDate) {
    			isUpdated = true;
    		}
    	}
    	
    	return isUpdated;
	}
    
    @Override
    protected void onPostExecute(List<Club> clubList) {
    	super.onPostExecute(clubList);
    	
    	if(clubList != null&& mRefreshList != null) { 
    		mRefreshList.refreshList();
		}
    }
}