package net.tomblog.app.android.swingheil.parser.server;

public interface BasicParser {

	// DEV
//	public static final String REST_API_BASE_URL = "http://app-devswingheil.rhcloud.com/swingheil/rest";
//	public static final String IMAGE_BASE_URL = "http://app-devswingheil.rhcloud.com/swingheil";
	
	// PRD
	public static final String REST_API_BASE_URL = "http://app-swingheil.rhcloud.com/swingheil/rest";
	public static final String IMAGE_BASE_URL = "http://app-swingheil.rhcloud.com/swingheil";
	
	public static final String CATEGORY = "category";
	public static final String DEVICE_UID = "deviceUid";
	public static final String USER_ID = "userId";
	public static final String BAR_ID = "barId";
	public static final String CLUB_ID = "clubId";
	public static final String EVENT_ID = "eventId";
	public static final String PAGE = "page";
	public static final String LIMIT = "limit";
	public static final String DEFAULT_LIMIT = "10"; 
	public static final String SUCCESS = "success";
	public static final String MESSAGE = "message";
	public static final String NICKNAME = "nickname";
	public static final String MODEL_ID = "modelId";
	public static final String OS_VERSION = "osVersion";
	public static final String REGISTRAION_ID = "registrationId";
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	public static final String PHONE = "phone";
	
	
}
