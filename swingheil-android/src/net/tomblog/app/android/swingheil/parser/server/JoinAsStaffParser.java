package net.tomblog.app.android.swingheil.parser.server;

import java.util.ArrayList;
import java.util.List;

import net.tomblog.app.android.swingheil.SwingHeilApplication;
import net.tomblog.app.android.swingheil.parser.RefreshResult;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.L;
import net.tomblog.app.android.swingheil.util.Util;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;

import com.swingheil.domain.shared.ResponseMessage;

/**
 * 동호회 / 바의 운영자로 등록하는 클래스
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class JoinAsStaffParser extends AsyncTask<String, String, ResponseMessage> implements BasicParser {

	private static final String TAG = JoinAsStaffParser.class.getName();
	
	String mGroupType = null;
	String mGroupId = null;
	RefreshResult mRefreshResult = null;
	
    public JoinAsStaffParser(String groupType, String groupId, RefreshResult refreshResult) {
    	mGroupType = groupType;
    	mGroupId = groupId;
    	mRefreshResult = refreshResult;
    }

    @Override
	protected ResponseMessage doInBackground(String... params) {
		L.d(TAG, " >>> called doInBackground()");
		ResponseMessage responseMessage = null;
		
		String requestUrl = REST_API_BASE_URL;
		if(Constants.BAR.equals(mGroupType)) {
			requestUrl += "/bar/joinAsStaff";
		}else if(Constants.CLUB.equals(mGroupType)){
			requestUrl += "/club/joinAsStaff";
		} else {
			return null;
		}
		
		try {
			HttpClient httpclient = new DefaultHttpClient();
			// specify the URL you want to post to
			HttpPost httppost = new HttpPost(requestUrl);
			L.d(TAG, "url : " + requestUrl);
			
			// create a list to store HTTP variables and their values
			List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
			// add an HTTP variable and value pair
			nameValuePairs.add(new BasicNameValuePair(USER_ID, SwingHeilApplication.getInstance().getUserId()));
			if(Constants.BAR.equals(mGroupType)) {
				nameValuePairs.add(new BasicNameValuePair(BAR_ID, mGroupId));
			}else if(Constants.CLUB.equals(mGroupType)){
				nameValuePairs.add(new BasicNameValuePair(CLUB_ID, mGroupId));
			}
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

			// send the variable and value, in other words post, to the URL
			HttpResponse response = httpclient.execute(httppost);
			
			String responseBody = EntityUtils.toString(response.getEntity());
			
			L.d(TAG, "responseBody : \n" + responseBody);
			
			responseMessage = Util.parseResponseMessage(TAG, responseBody);
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		return responseMessage;
	}
    
    @Override
    protected void onPostExecute(ResponseMessage responseMessage) {
    	L.d(TAG, " >>> called doInBackground()");
    	mRefreshResult.refreshResult(responseMessage); 
    }
    
}