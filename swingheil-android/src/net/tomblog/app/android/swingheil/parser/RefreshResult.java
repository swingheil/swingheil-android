package net.tomblog.app.android.swingheil.parser;

import com.swingheil.domain.shared.ResponseMessage;

public interface RefreshResult {
	public void refreshResult(ResponseMessage responseMessage); 
}
