package net.tomblog.app.android.swingheil.parser.cafe;

import java.net.HttpURLConnection;
import java.util.List;

import oauth.signpost.OAuthConsumer;

import net.tomblog.app.android.swingheil.model.Article;

public abstract class ArticleParser {
	
	// Intent의 put & getExtra 용
	public static final String INTENT_CAFE_ID = "net.tomblog.app.swingheil.CafeId";
	public static final String INTENT_BOARD_ID = "net.tomblog.app.swingheil.BoardId";

	// 각 포털 싸이트 ID
	public static final int NAVER = 0;
	public static final int DAUM = 1;
	// 각 까페의 cafeIndexes 
	public static final int CAFE_SWING_ADE = 0;
	public static final int CAFE_CRAZY_SWING = 1;
	public static final int CAFE_SWING_HEIL = 2;
	// 각 까페의 포탈 사이트 ID를 저장한 배열
	protected static final int[] potalSiteIdArray = {0, 1, 0};
	// 각 까페의 cafeIds 
	protected static final String[] cafeIdArray = {"17281865", "CrazySwing", "23671962"};
	// 각 까페의 공지사항 boardIds 
	protected static final String[] noticeBoardIdArray = {"1", "F0jq", "1"};
	
	protected OAuthConsumer mConsumer = null;
	
	// Parsing 작업을 중지할때
	protected boolean mIsCanceled;
	protected HttpURLConnection mRequest;
	
	/**
	 * 특정 까페가 속한 PotalSiteId를 반환한다.
	 * @param cafeIndex
	 * @return
	 */
	public static int getPotalSiteId(int cafeIndex) {
		return potalSiteIdArray[cafeIndex];
	}
	
	/**
	 * 특정 까페가 속한 PotalSiteId를 반환한다.
	 * @param cafeIndex
	 * @return
	 */
	public static int getPotalSiteId(String cafeId) {
		int cafeIndex = 0;
		for (int i = 0; i < cafeIdArray.length; i++) {
			if(cafeId.equals(cafeIdArray[i])) {
				cafeIndex = i;
				break;
			}
		}
		return potalSiteIdArray[cafeIndex];
	}

	/**
	 * 특정 까페의 아이디를 반환한다.
	 * @param cafeIndex
	 * @return
	 */
	public static String getCafeId(int cafeIndex) {
		return cafeIdArray[cafeIndex];
	}

	/**
	 * 특정 까페의 공지 게시판의 아이디를 반환한다.
	 * @param cafeIndex
	 * @return
	 */
	public static String getNoticeBoardId(int cafeIndex) {
		return noticeBoardIdArray[cafeIndex];
	}
	
	/**
	 * 파싱 작업을 취소한다.
	 */
	public void cancelGettingArticleList() {
		mIsCanceled = true;
		if(mRequest != null) {
			mRequest.disconnect();
		}
	}
	
	public void setIsCanceled(boolean isCanceled) {
		this.mIsCanceled = isCanceled;
	}

	/**
	 * 각 포털 사이트에 따른 게시판 내용을 볼 수 있는 URL을 반환한다.
	 * @param potalSiteId
	 * @param article
	 * @return
	 */
	public abstract String getArticleReadURL(Article article);
	
	/**
	 * 특정 까페의 게시판 글을 반환한다.
	 * @param cafeId
	 * @param boardId
	 * @param searchPage
	 * @param searchPerPage
	 * @return
	 * @throws Exception 
	 */
	public abstract List<Article> getArticleList(String cafeId, String boardId, int searchPage, int searchPerPage) throws Exception;
	
	public interface ArticleListener {
		public int ERROR_NETWORK_PROBLEM = 1;
		public int ERROR_CANCELED = 2;
		public void onComplete(boolean needToIncreasePaging); 
		public void onError(int errorCode, String msg);
	}

}