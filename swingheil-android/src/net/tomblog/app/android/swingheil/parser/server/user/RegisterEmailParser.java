package net.tomblog.app.android.swingheil.parser.server.user;

import java.util.ArrayList;
import java.util.List;

import net.tomblog.app.android.swingheil.SwingHeilApplication;
import net.tomblog.app.android.swingheil.parser.RefreshResult;
import net.tomblog.app.android.swingheil.parser.server.BasicParser;
import net.tomblog.app.android.swingheil.util.L;
import net.tomblog.app.android.swingheil.util.Util;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;

import com.swingheil.domain.shared.ResponseMessage;
import com.swingheil.domain.user.SocialUser;

/**
 * 이메일, 비밀번호, 전화번호를 등록하는 클래스
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class RegisterEmailParser extends AsyncTask<String, String, ResponseMessage> implements BasicParser {

	private static final String TAG = RegisterEmailParser.class.getName();
	
	SocialUser mSocialUser = null;
	RefreshResult mRefreshResult = null;
	
	/**
	 *  사용자 email 등록 URL
	 */
    public static final String EMAIL_REGISTER_URL = REST_API_BASE_URL + "/user/registerEmail";
	
    public RegisterEmailParser(SocialUser socialUser, RefreshResult refreshResult) {
    	mSocialUser = socialUser;
    	mRefreshResult = refreshResult;
    }

    @Override
	protected ResponseMessage doInBackground(String... params) {
		L.d(TAG, " >>> called doInBackground()");
		ResponseMessage responseMessage = null;
		
		try {
			HttpClient httpclient = new DefaultHttpClient();
			// specify the URL you want to post to
			HttpPost httppost = new HttpPost(EMAIL_REGISTER_URL);
			L.d(TAG, "url : " + EMAIL_REGISTER_URL);
			
			// create a list to store HTTP variables and their values
			List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
			// add an HTTP variable and value pair
			nameValuePairs.add(new BasicNameValuePair(USER_ID, SwingHeilApplication.getInstance().getUserId()));
			nameValuePairs.add(new BasicNameValuePair(DEVICE_UID, SwingHeilApplication.getInstance().getDeviceUid()));
			nameValuePairs.add(new BasicNameValuePair(EMAIL, mSocialUser.getEmail()));
			nameValuePairs.add(new BasicNameValuePair(PASSWORD, mSocialUser.getPassword()));
			nameValuePairs.add(new BasicNameValuePair(PHONE, mSocialUser.getPhone()));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

			// send the variable and value, in other words post, to the URL
			HttpResponse response = httpclient.execute(httppost);
			
			String responseBody = EntityUtils.toString(response.getEntity());
			
			L.d(TAG, "responseBody : \n" + responseBody);
			
			responseMessage = Util.parseResponseMessage(TAG, responseBody);
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		return responseMessage;
	}
    
    @Override
    protected void onPostExecute(ResponseMessage responseMessage) {
    	L.d(TAG, " >>> called doInBackground()");
    	mRefreshResult.refreshResult(responseMessage); 
    }
    
}