/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.tomblog.app.android.swingheil.gcm;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.activity.EventDetailActivity;
import net.tomblog.app.android.swingheil.parser.RefreshEvent;
import net.tomblog.app.android.swingheil.parser.server.EventParser;
import net.tomblog.app.android.swingheil.util.ClubAndBarHolder;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.Util;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.swingheil.domain.event.Event;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.type.enums.EventCategory;
import com.swingheil.domain.type.enums.NotiType;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService implements RefreshEvent {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    private static final String TAG = GcmIntentService.class.getSimpleName();
    
    public static final String GCM_KEY_TITLE = "title";
    public static final String GCM_KEY_MESSAGE = "message";
    public static final String GCM_KEY_TYPE = "notiType";
    public static final String GCM_KEY_EVENT_ID = "eventId";
    
    private Bundle mExtras = null;
    
    public GcmIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM will be
             * extended in the future with new message types, just ignore any message types you're
             * not interested in, or that you don't recognize.
             */
        	Log.d(TAG, "Received: " + extras.toString());
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
            // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // Post notification of received message.
                sendNotification(extras);
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(Bundle extras) {
    	String type = extras.getString(GCM_KEY_TYPE);
    	
    	NotiType notiType = NotiType.findBy(type);
		if (notiType == NotiType.App) {
			// 어플의 공지사항일 경우
		} else if (notiType == NotiType.Bar
    			|| notiType == NotiType.Club
    			|| notiType == NotiType.Instructor
    			|| notiType == NotiType.SwingHeil) {
			// 바, 클럽, 강사, 스윙하일의 공지사항일 경우
			mExtras = extras;
			String eventId = extras.getString(GCM_KEY_EVENT_ID);
			new EventParser(this, eventId).execute();
    	} else {
    		// Exception
    	}
    }

	@Override
	public void refresh(Event event) {
		if(mExtras != null && event != null) {
	    	String title = mExtras.getString(GCM_KEY_TITLE);
	    	String message = mExtras.getString(GCM_KEY_MESSAGE);
	    	String notiType = mExtras.getString(GCM_KEY_TYPE);
	    	String eventId = mExtras.getString(GCM_KEY_EVENT_ID);
	    	Log.d(TAG, "title: " + title);
	    	Log.d(TAG, "message: " + message);
	    	Log.d(TAG, "notiType: " + notiType);
	    	Log.d(TAG, "eventId: " + eventId);
	    	if(event.getSponsor() != null && event.getCategory() != null) {
	    		String id = event.getSponsor().getId();
	    		NotiType aNotiType = NotiType.findBy(notiType);
	    		if(NotiType.SwingHeil == aNotiType) {
	    			String appName = getResources().getString(R.string.app_name_kor);
	    			title = appName + "(" + getResources().getString(R.string.app_name) + ")";
	    		} else if(NotiType.Club == aNotiType) {
	    			Club club = ClubAndBarHolder.getInstance().retrieveClub(id);
	    			if(club != null) {
	    				title = club.getNameKor();
	    				if(!Util.isEmpty(club.getNameEng())) {
	    					title = title + "(" + club.getNameEng() + ")";
	    				}
	    			}
	    		} else if(NotiType.Bar == aNotiType) {
	    			Bar bar = ClubAndBarHolder.getInstance().retrieveBar(id);
	    			if(bar != null) {
	    				title = bar.getNameKor();
	    				if(!Util.isEmpty(bar.getNameEng())) {
	    					title = title + "(" + bar.getNameEng() + ")";
	    				}
	    			}
	    		}
	    	}
	    	
	    	mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
	    	
	    	Intent intent = new Intent(this, EventDetailActivity.class);
	    	intent.putExtra(Constants.EVENT, event); 
	    	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    	intent.putExtra(Constants.IS_FROM_GCM_NOTI, true);
	    	PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
	    	
	    	NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
	    	mBuilder.setSmallIcon(R.drawable.icon_launcher)
	    	.setTicker(getResources().getText(R.string.noti_ticker_msg))
	    	.setStyle(new NotificationCompat.BigTextStyle())
	    	.setContentTitle(title)
	    	.setContentText(message)
	    	.setWhen(System.currentTimeMillis())
//    	.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
	    	.setAutoCancel(true);
	    	
	    	mBuilder.setContentIntent(contentIntent);
	    	mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
		}
	}
}
