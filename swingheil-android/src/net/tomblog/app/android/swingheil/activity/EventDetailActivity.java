package net.tomblog.app.android.swingheil.activity;

import java.util.ArrayList;
import java.util.List;

import net.daum.adam.publisher.AdView;
import net.daum.adam.publisher.AdView.AnimationType;
import net.daum.adam.publisher.AdView.OnAdClickedListener;
import net.daum.adam.publisher.AdView.OnAdClosedListener;
import net.daum.adam.publisher.AdView.OnAdFailedListener;
import net.daum.adam.publisher.AdView.OnAdLoadedListener;
import net.daum.adam.publisher.AdView.OnAdWillLoadListener;
import net.daum.adam.publisher.impl.AdError;
import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.parser.DownloadImagesTask;
import net.tomblog.app.android.swingheil.parser.RefreshAdapter;
import net.tomblog.app.android.swingheil.parser.server.FavoriteParser;
import net.tomblog.app.android.swingheil.provider.EventDBHelper;
import net.tomblog.app.android.swingheil.util.ClubAndBarHolder;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.EventHolder;
import net.tomblog.app.android.swingheil.util.ImageResource;
import net.tomblog.app.android.swingheil.util.L;
import net.tomblog.app.android.swingheil.util.ThumbnailHolder;
import net.tomblog.app.android.swingheil.util.Util;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.swingheil.domain.event.Event;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.group.Group;
import com.swingheil.domain.type.DateTime;
import com.swingheil.domain.type.Period;
import com.swingheil.domain.type.enums.EventCategory;
import com.swingheil.domain.type.place.Address;
import com.swingheil.domain.type.place.RoughMap;
import com.swingheil.domain.type.place.Station;
import com.swingheil.domain.type.place.Subway;
import com.swingheil.domain.type.place.Subway.Line;

/**
 * Event 상세화면을 보여주는 Activity
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class EventDetailActivity extends ActionBarActivity implements RefreshAdapter {  

	private static final String TAG = EventDetailActivity.class.getName();
	
    private LinearLayout adWrapper = null;
    private AdView adView = null;
	
	public static final String EVENT_ID = "net.tomblog.app.swingheil.EVENT_ID";

	private ListView mListView;
	private BaseAdapter mEventDetailAdapter = null;
	private ArrayList<String> mEventDetailList = null;
	private int mDefaultInfoSize = 6;
	private RefreshAdapter mRefreshList= null;
	private boolean mIsFromGcmNoti = false;
	
	private Menu mMenu;
	private Event mEvent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		L.d(TAG, " >>> called onCreate()");  
		
		setContentView(R.layout.activity_event_detail); 
  
        adWrapper = (LinearLayout) findViewById(R.id.adWrapper);
        initAdam();
        
		inflateView();

		initObject();
		
	}
	
	private void inflateView() {
		L.d(TAG, " >>> called inflateView()");
	
		mListView = (ListView)findViewById(R.id.eventDetailList); 
		
	}

	
    private void initObject() {
    	L.d(TAG, " >>> called initObject()");
    	
    	mRefreshList = this;
    	Intent intent = getIntent();

    	mIsFromGcmNoti = intent.getBooleanExtra(Constants.IS_FROM_GCM_NOTI, false);
    	mEvent = (Event) intent.getSerializableExtra(Constants.EVENT);
    	if(mEvent != null) {
    		String url = mEvent.getThumbnailUrl();
			ThumbnailHolder thumbnailHolder = ThumbnailHolder.getInstance();
			Bitmap thumbnail = thumbnailHolder.retrieve(url);
			if(thumbnail == null) {
				thumbnail = BitmapFactory.decodeResource(getResources(), R.drawable.event_img_none_white);  
			}
			thumbnail = Bitmap.createScaledBitmap(thumbnail, 280, 280, true);
			BitmapDrawable icon = new BitmapDrawable(thumbnail);
			
            ActionBar actionBar = getSupportActionBar();
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.brown)));
            actionBar.setTitle(mEvent.getTitle()); 
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setIcon(icon);
            
            EventDBHelper db = new EventDBHelper(this);
            db.addEvent(mEvent);
			
			mEvent.setNew(false);
			setResult(RESULT_OK, getIntent().putExtra(Constants.EVENT, mEvent));
    	}else {
    		finish();
    		return;
    	}
    	
    	mEventDetailList = new ArrayList<String>();
    	for (int i = 0; i < mDefaultInfoSize; i++) {
    		mEventDetailList.add(new String());
		}
    	
		mEventDetailAdapter = new EventDetailAdapter(LayoutInflater.from(this), mEventDetailList, mEvent);
		mListView.setAdapter(mEventDetailAdapter); 	
    }   	
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_menu, menu); 
        mMenu = menu;
        return true; 
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	
    	if(mEvent != null) {
    		MenuItem favoriteItem = mMenu.findItem(R.id.favorite);
    		MenuItem unfavoriteItem = mMenu.findItem(R.id.unfavorite);
    		if(mEvent.isFavorite()) {
    			favoriteItem.setVisible(true);
    			unfavoriteItem.setVisible(false);
    		}else {
    			favoriteItem.setVisible(false);
    			unfavoriteItem.setVisible(true);
    		}
    	}
    	
    	return super.onPrepareOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.favorite:
			Toast.makeText(getApplicationContext(), R.string.do_unfavorite, Toast.LENGTH_LONG).show();
			MenuItem unfavoriteItem = mMenu.findItem(R.id.unfavorite);
			unfavoriteItem.setVisible(true);
			item.setVisible(false);
			mEvent.setFavorite(false);
			setResult(RESULT_OK, getIntent().putExtra(Constants.EVENT, mEvent));
			
			EventHolder.getInstance().saveEventFavorite(mEvent.getId(), true);
			
			new FavoriteParser(Constants.EVENT, Constants.ACTION_FOLLOW, mEvent.getId()).execute();
			
			return true;
		case R.id.unfavorite:
			Toast.makeText(getApplicationContext(), R.string.do_favorite, Toast.LENGTH_LONG).show();
			MenuItem favoriteItem = mMenu.findItem(R.id.favorite);
			favoriteItem.setVisible(true); 
			item.setVisible(false);
			mEvent.setFavorite(true);
			setResult(RESULT_OK, getIntent().putExtra(Constants.EVENT, mEvent));
			
			EventHolder.getInstance().saveEventFavorite(mEvent.getId(), false);
			
			new FavoriteParser(Constants.EVENT, Constants.ACTION_UNFOLLOW, mEvent.getId()).execute();
			
			return true;
		default:
			return super.onOptionsItemSelected(item); 
		}
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	L.d(TAG, " >>> called onDestroy()");
    	
    	// Poster 이미지를 메모리에서 삭제한다.
    	ThumbnailHolder thumbnailHolder = ThumbnailHolder.getInstance();
    	thumbnailHolder.remove(mEvent.getPosterUrl());

    	if ( adView != null ) {
            adView.destroy();
            adView = null;
        }		
	}
	
    @Override
    public void onBackPressed() {
    	super.onBackPressed();
    	
    	if(mIsFromGcmNoti) {
    		Intent intent = new Intent(getApplicationContext(), SwingHeilActivity.class); 
    		startActivity(intent);	 
    		finish();
    	}
    }
    
	private void initAdam() {
        // Ad@m sdk 초기화 시작
		adView = (AdView) findViewById(R.id.adview);
        adView.setRequestInterval(5);

        // 광고 클릭시 실행할 리스너
        adView.setOnAdClickedListener(new OnAdClickedListener() {
            public void OnAdClicked() {
                L.d(TAG, "광고를 클릭했습니다.");
            }
        });

        // 광고 내려받기 실패했을 경우에 실행할 리스너
        adView.setOnAdFailedListener(new OnAdFailedListener() {
            public void OnAdFailed(AdError arg0, String arg1) {
                adWrapper.setVisibility(View.GONE);
                L.d(TAG, arg1);
            }
        });

        // 광고를 정상적으로 내려받았을 경우에 실행할 리스너
        adView.setOnAdLoadedListener(new OnAdLoadedListener() {
            public void OnAdLoaded() {
                adWrapper.setVisibility(View.VISIBLE);
                L.d(TAG, "광고가 정상적으로 로딩되었습니다.");
            }
        });

        // 광고를 불러올때 실행할 리스너
        adView.setOnAdWillLoadListener(new OnAdWillLoadListener() {
            public void OnAdWillLoad(String arg1) {
                L.d(TAG, "광고를 불러옵니다. : " + arg1);
            }
        });

        // 광고를 닫았을때 실행할 리스너
        adView.setOnAdClosedListener(new OnAdClosedListener() {
            public void OnAdClosed() {
                L.d(TAG, "광고를 닫았습니다.");
            }
        });

        // 할당 받은 clientId 설정
        adView.setClientId(Constants.CLIENT_ID);

        adView.setRequestInterval(12);

        // Animation 효과 : 기본 값은 AnimationType.NONE
        adView.setAnimationType(AnimationType.FLIP_HORIZONTAL);

        adView.setVisibility(View.VISIBLE);
    }	    
    
    private void scaleImage(ImageView view, int dp)
    {
        // Get the ImageView and its bitmap
        Drawable drawing = view.getDrawable();
        if (drawing == null) {
            return; // Checking for null & return, as suggested in comments
        }
        Bitmap bitmap = ((BitmapDrawable)drawing).getBitmap();

        // Get current dimensions AND the desired bounding box
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int bounding = dpToPx(dp);

        // Determine how much to scale: the dimension requiring less scaling is
        // closer to the its side. This way the image always stays inside your
        // bounding box AND either x/y axis touches it.  
        float xScale = ((float) bounding) / width;
        float yScale = ((float) bounding) / height;
        float scale = (xScale <= yScale) ? xScale : yScale;

        // Create a matrix for the scaling and add the scaling data
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);

        // Create a new bitmap and convert it to a format understood by the ImageView 
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        width = scaledBitmap.getWidth(); // re-use
        height = scaledBitmap.getHeight(); // re-use
        BitmapDrawable result = new BitmapDrawable(scaledBitmap);

        // Apply the scaled bitmap
        view.setImageDrawable(result);

        // Now change ImageView's dimensions to match the scaled image
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams(); 
        params.width = width;
        params.height = height;
        view.setLayoutParams(params); 
    }

    private int dpToPx(int dp)
    {
        float density = getApplicationContext().getResources().getDisplayMetrics().density;
        return Math.round((float)dp * density);
    }
    
	private class EventDetailAdapter extends BaseAdapter { 
		
		private static final int TYPE_POSTER = 0;
		private static final int TYPE_TITLE = 1;
		private static final int TYPE_DATE = 2;
		private static final int TYPE_TIME = 3;
		private static final int TYPE_LOCATION = 4;
		private static final int TYPE_DESCRIPTION = 5;

		private final int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
		private final int height = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
		private final int margin3 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getResources().getDisplayMetrics());
		private final int margin5 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics());
		
		private final int[] types = {
				TYPE_POSTER, TYPE_TITLE, TYPE_DATE, TYPE_TIME, TYPE_LOCATION, 
				TYPE_DESCRIPTION
		};
		private final int TYPE_MAX_COUNT = types.length;
		
		private LayoutInflater inflater = null;
		private List<String> mEventDetailList = null;
		private Event mEvent = null;

		public EventDetailAdapter(LayoutInflater layoutInflater, List<String> eventDetailList, Event event) {
			inflater = layoutInflater;
			mEventDetailList = eventDetailList;
			mEvent = event;
		} 

		@Override
		public int getItemViewType(int position) {
			int type = 0;
			
			if(position >= types.length) {
				type = types[types.length-1];
			}else {
				type = types[position];
			}
			
			return type;
		}

		@Override
		public int getViewTypeCount() {
			int viewTypeCount =  TYPE_MAX_COUNT;
			
			return viewTypeCount;
		}
		
		@Override
		public int getCount() {
			return mEventDetailList.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			int type = getItemViewType(position);
			
			if (convertView == null) {
				holder = new ViewHolder();

				switch (type) {
					case TYPE_POSTER:
						convertView = inflater.inflate(R.layout.list_item_event_detail_poster, parent, false);
						holder.posterIV = (ImageView) convertView.findViewById(R.id.poster);
						holder.posterIV.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								Intent intent = new Intent(getApplicationContext(), ImageDetailActivity.class); 
								intent.putExtra(ImageDetailActivity.POSTER_URL, mEvent.getPosterUrl());
								intent.putExtra(ImageDetailActivity.THUMBNAIL_URL, mEvent.getThumbnailUrl());
								startActivity(intent);	 
							}
						});
						break;
					case TYPE_TITLE:
						convertView = inflater.inflate(R.layout.list_item_event_detail_title, parent, false); 
						holder.titleTV = (TextView) convertView.findViewById(R.id.title);
						break;
					case TYPE_DATE:
						convertView = inflater.inflate(R.layout.list_item_event_detail_date, parent, false);
						holder.startDateTV = (TextView) convertView.findViewById(R.id.start_date);
						holder.endDateTV = (TextView) convertView.findViewById(R.id.end_date);
						holder.dateBarV = (View) convertView.findViewById(R.id.date_bar);
						holder.endDateL = (View) convertView.findViewById(R.id.end_date_layout);
						break;
					case TYPE_TIME:
						convertView = inflater.inflate(R.layout.list_item_event_detail_time, parent, false);
						holder.startTimeTV = (TextView) convertView.findViewById(R.id.start_time);
						holder.endTimeTV = (TextView) convertView.findViewById(R.id.end_time);
						holder.timeBarV = (View) convertView.findViewById(R.id.time_bar);
						holder.endTimeL = (View) convertView.findViewById(R.id.end_time_layout);
						break;
					case TYPE_LOCATION: 
						convertView = inflater.inflate(R.layout.list_item_event_detail_location, parent, false);
						holder.locationL = (View) convertView.findViewById(R.id.location_layout);
						holder.locationL.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								if(mEvent != null) {
									Address eventAddress = mEvent.getPlace();
									Group sponsor = mEvent.getSponsor();
									if(isNewPlace(eventAddress, sponsor)) {
										// 새로운 주소인 경우 
										// TODO 주소로 지도 표시하는 것은 다음 버전에 구현.
//										String eventId = mEvent.getId(); 
//										intent.putExtra(Constants.EVENT_ID, eventId); 
//										startActivity(intent);
									} else {
										// 동호회나 바의 기본 주소인 경우 
										if(sponsor != null) {
											Bar bar = getBar(sponsor);
											if(bar != null) {
												Intent intent = new Intent(getApplicationContext(), NaverMapActivity.class);
												String barId = bar.getId();
												intent.putExtra(Constants.BAR_ID, barId); 
												intent.putExtra(Constants.ADDRESS_TYPE, Constants.ADDRESS_TYPE_ROAD);
												startActivity(intent);
											}
										}
									}
								}
							}
						});
						holder.locationNameTV = (TextView) convertView.findViewById(R.id.location_name);
						holder.locationDistrictTV = (TextView) convertView.findViewById(R.id.location_district);
						holder.locationSubwayL = (LinearLayout) convertView.findViewById(R.id.location_subways);
						holder.locationImageIV = (ImageView) convertView.findViewById(R.id.location_image);
						break;
					case TYPE_DESCRIPTION:
						convertView = inflater.inflate(R.layout.list_item_event_detail_description, parent, false);
						holder.descriptionTV = (TextView) convertView.findViewById(R.id.description);
						break;
				}
				 
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder)convertView.getTag();
			}

			switch (type) {
				case TYPE_POSTER:
					
					String posterUrl = mEvent.getPosterUrl();
					ThumbnailHolder thumbnailHolder = ThumbnailHolder.getInstance();
					Bitmap poster = thumbnailHolder.retrieve(posterUrl);
					
					if(poster != null) {
						holder.posterIV.setImageBitmap(poster); 
					}else {
						Bitmap thumbnail = thumbnailHolder.retrieve(mEvent.getThumbnailUrl());
						if(thumbnail != null) {
							holder.posterIV.setImageBitmap(thumbnail);
						}
						
						if(!thumbnailHolder.isInTask(posterUrl)) {
							DownloadImagesTask task = new DownloadImagesTask(mRefreshList);
							thumbnailHolder.setTask(posterUrl, task);
							task.execute(posterUrl);
						}  
					}
					
					scaleImage(holder.posterIV, 330);
					break;
				case TYPE_TITLE:
					holder.titleTV.setText(mEvent.getTitle());
					break;
				case TYPE_DATE:
					Period startPeriod = mEvent.getPeriod();
					if(startPeriod != null && startPeriod.getFrom() != null && !Util.isEmpty(startPeriod.getFrom().getDate())) {
						DateTime startDate = startPeriod.getFrom();
						if(startDate != null) {
							holder.startDateTV.setText(startDate.getDate());
						} 
						DateTime endDate = startPeriod.getTo();
						if(endDate != null && !Util.isEmpty(endDate.getDate())) {
							holder.dateBarV.setVisibility(View.VISIBLE);
							holder.endDateL.setVisibility(View.VISIBLE);
							holder.endDateTV.setText(endDate.getDate());
						}else {
							holder.dateBarV.setVisibility(View.GONE);
							holder.endDateL.setVisibility(View.GONE);
						}
					}else {
						convertView = inflater.inflate(R.layout.list_item_null, parent, false);
					}
					break;					
				case TYPE_TIME:
					Period endPeriod = mEvent.getPeriod();
					if(endPeriod != null && endPeriod.getFrom() != null && !Util.isEmpty(endPeriod.getFrom().getTime())) {
						DateTime startTime = endPeriod.getFrom();
						if(startTime != null) {
							holder.startTimeTV.setText(startTime.getTime());
						}
						DateTime endTime = endPeriod.getTo();
						if(endTime != null && !Util.isEmpty(endTime.getTime())) { 
							holder.timeBarV.setVisibility(View.VISIBLE); 
							holder.endTimeL.setVisibility(View.VISIBLE);
							holder.endTimeTV.setText(endTime.getTime());
						}else {
							holder.timeBarV.setVisibility(View.GONE);
							holder.endTimeL.setVisibility(View.GONE); 
						}
					}else {
						convertView = inflater.inflate(R.layout.list_item_null, parent, false);
					}
					break;
				case TYPE_LOCATION: 
					Group sponsor = mEvent.getSponsor();
					Address eventAddress = mEvent.getPlace();
					if(isNewPlace(eventAddress, sponsor)) {
						if (existAddressInfo(eventAddress)) {
							setLocationAndSubwayInfo(holder, eventAddress);
						} else {
							convertView = inflater.inflate(R.layout.list_item_null, parent, false);
						}
					} else {
						if (sponsor != null) {
							Bar bar = getBar(sponsor);
							if (bar != null) {
								setLocationAndSubwayInfo(holder, bar);
							} else {
								convertView = inflater.inflate(R.layout.list_item_null, parent, false);
							}
						} else {
							convertView = inflater.inflate(R.layout.list_item_null, parent, false);
						}
					}
 					break;
				case TYPE_DESCRIPTION:
					holder.descriptionTV.setText(mEvent.getDetail());
					break;
			}
			
			return convertView;
		}

		private boolean existAddressInfo(Address address) {
			boolean existAddressInfo = false;
			
			if(!Util.isEmpty(address.getRoad()) 
					|| !Util.isEmpty(address.getLand()) 
					|| !Util.isEmpty(address.getDistrict())) {
				existAddressInfo = true;
			}
			
			return existAddressInfo;
		}

		private Bar getBar(Group sponsor) {
			String barId = "";
			if(EventCategory.Club.code().equals(mEvent.getCategory().getCode())) {
				Club club = ClubAndBarHolder.getInstance().retrieveClub(sponsor.getId());
				if(club != null) {
					barId = club.getBar().getId();
				}
			}else {
				barId = sponsor.getId();
			}
			Bar bar = ClubAndBarHolder.getInstance().retrieveBar(barId);
			return bar;
		}

		private void setLocationAndSubwayInfo(ViewHolder holder, Address address) {
			holder.locationImageIV.setVisibility(View.GONE);
			holder.locationNameTV.setText(address.getDistrict());

			String locationDetail = "";
			if(!Util.isEmpty(address.getRoad())) {
				locationDetail = address.getRoad();;
			} else if(!Util.isEmpty(address.getLand())) {
				locationDetail = address.getLand();;
			}
			holder.locationDistrictTV.setText(locationDetail); 
			 
			if(address.getMap() != null) {
				setSubwayInfo(holder, address.getMap());
			}
		}

		private void setLocationAndSubwayInfo(ViewHolder holder, Bar bar) {
			holder.locationImageIV.setVisibility(View.VISIBLE);
			holder.locationImageIV.setImageResource(ImageResource.BAR[Integer.valueOf(bar.getId())]); 

			String locationName = bar.getNameKor(); 
			String nameEngStr = bar.getNameEng().replace(" ", "");
			if(!nameEngStr.isEmpty()) {
				locationName = locationName + "  (" + bar.getNameEng() + ")";
			} 
			holder.locationNameTV.setText(locationName);
			
			holder.locationDistrictTV.setText(bar.getAddress().getDistrict()); 
			
			if(bar.getAddress().getMap() != null) {
				setSubwayInfo(holder, bar.getAddress().getMap());
			}
		}

		private void setSubwayInfo(ViewHolder holder, RoughMap map) {
			holder.locationSubwayL.removeAllViews(); 
			Subway subway = map.getSubway();
			if(subway != null) { 
				List<Line> lines = subway.getLines();
				for (Line line : lines) {
					ImageView subwayIV = new ImageView(inflater.getContext());
					subwayIV.setImageResource(ImageResource.SUBWAY[line.getId()]);
					LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, height);
					lp.rightMargin = margin5;
					holder.locationSubwayL.addView(subwayIV, lp);
				}
				 
				Station station = getStation(subway);
				
				TextView nameTV = new TextView(inflater.getContext());
				nameTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
				nameTV.setTextColor(getResources().getColor(R.color.black));
				if(station != null) {
					nameTV.setText(station.getName()); 
				}
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				lp.leftMargin = margin3;
				holder.locationSubwayL.addView(nameTV, lp);
				
				TextView exitTV = new TextView(inflater.getContext());
				exitTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
				exitTV.setTextColor(getResources().getColor(R.color.black));
				if(station != null) {
					exitTV.setText(station.getExit() + getResources().getString(R.string.str_exit));
				}
				LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				lp2.leftMargin = margin5;
				holder.locationSubwayL.addView(exitTV, lp2);
				holder.locationSubwayL.setGravity(Gravity.CENTER_VERTICAL);
			}
		}

		private Station getStation(Subway subway) {

			Station station = null;
			
			if(subway != null) {
				List<Station> stationList = subway.getStations();
				if(stationList != null) {
					for (Station aStation : stationList) {
						if(aStation != null) {
							station  = aStation;
							break;
						}
					} 
				}
			}
			
			return station;
		}

		private boolean isNewPlace(Address eventAddress, Group sponsor) {
			boolean isNewPlace = true;
			
			Bar bar = getBar(sponsor);
			if(bar != null) {
				Address barAddress = bar.getAddress();
				if(barAddress != null) {
					if(Util.isEqual(eventAddress.getDistrict(), barAddress.getDistrict())
							&& Util.isEqual(eventAddress.getLand(), barAddress.getLand()) 
							&& Util.isEqual(eventAddress.getRoad(), barAddress.getRoad())) {
						isNewPlace = false;
					}
				}
			}
			
			return isNewPlace;
		}
	}
	
	public static class ViewHolder {
		public ImageView posterIV;
		public TextView titleTV;
		public TextView startDateTV;
		public TextView endDateTV;
		public TextView startTimeTV;
		public TextView endTimeTV;
		public TextView locationNameTV;
		public TextView locationDistrictTV;
		public LinearLayout locationSubwayL;
		public ImageView locationImageIV;
		public View locationL;
		public TextView descriptionTV;
		public View dateBarV;
		public View endDateL;
		public View timeBarV;
		public View endTimeL;
	}

	@Override
	public void refreshAdapter() {
		if(mEventDetailAdapter != null) {
			mEventDetailAdapter.notifyDataSetChanged();
		}
	}    
	
}