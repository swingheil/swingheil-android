package net.tomblog.app.android.swingheil.activity;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.parser.RefreshResult;
import net.tomblog.app.android.swingheil.parser.server.JoinAsStaffParser;
import net.tomblog.app.android.swingheil.util.ClubAndBarHolder;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.L;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.shared.ResponseMessage;

/**
 * 동호회 / 바의 관리자로 등록하는 Activity
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class JoinAsStaffActivity extends ActionBarActivity implements RefreshResult {

	private static final String TAG = JoinAsStaffActivity.class.getName();
	
	private TextView mStaffTermsTV;
	private Button mRegisterBtn;
	private RefreshResult mRefreshResult = null;
	
	private boolean isClicked = false;
	private String mGroupType = null;
	private String mGroupId = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		L.d(TAG, " >>> called onCreate()");  
		
		setContentView(R.layout.activity_staff_registration); 
  
		inflateView();

		initObject(getIntent());
	}
	
	
	private void inflateView() {
		L.d(TAG, " >>> called inflateView()");
	
		mStaffTermsTV = (TextView)findViewById(R.id.staff_terms_text);
		mRegisterBtn = (Button)findViewById(R.id.register_button);
	}

    private void initObject(Intent intent) {
    	L.d(TAG, " >>> called initObject()");
    	
    	mRefreshResult = this;
    	
    	mGroupType = intent.getStringExtra(Constants.GROUP_TYPE);
    	mGroupId = intent.getStringExtra(Constants.GROUP_ID);
    	
    	String groupTypeName = "";
    	String groupName = "";
    	ClubAndBarHolder holder = ClubAndBarHolder.getInstance();
    	if(Constants.CLUB.equalsIgnoreCase(mGroupType)) {
    		groupTypeName = getResources().getString(R.string.str_club);
    		Club club = holder.retrieveClub(mGroupId);
    		if(club != null) {
    			groupName = club.getNameKor(); 
    		}
    	}else {
    		groupTypeName = getResources().getString(R.string.str_bar);
    		Bar bar = holder.retrieveBar(mGroupId);
    		if(bar != null) {
    			groupName = bar.getNameKor();
    		}
    	}
    	
    	String termsString = getResources().getString(R.string.str_staff_terms);
    	String terms = String.format(termsString, groupName, groupTypeName);
    	mStaffTermsTV.setText(terms);
    	
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.brown)));
        actionBar.setTitle(getResources().getText(R.string.request_admin)); 
        actionBar.setDisplayHomeAsUpEnabled(true);
        
        mRegisterBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!isClicked) {
					isClicked = true;
					
					new JoinAsStaffParser(mGroupType, mGroupId, mRefreshResult).execute();
				}
			}

		});
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item); 
		}
    }

	@Override
	public void refreshResult(ResponseMessage responseMessage) {
		L.d(TAG, " >>> called refreshResult()");
		if(responseMessage != null) {
			if(responseMessage.isSuccess()) {
				Toast.makeText(getApplicationContext(), R.string.toast_register_success, Toast.LENGTH_LONG).show();
				finish();
			} else {
				isClicked = false;
				Toast.makeText(getApplicationContext(), R.string.toast_register_failed, Toast.LENGTH_LONG).show();
			}
		}
	}
}