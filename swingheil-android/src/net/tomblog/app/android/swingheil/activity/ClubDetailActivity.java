package net.tomblog.app.android.swingheil.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.daum.adam.publisher.AdView;
import net.daum.adam.publisher.AdView.AnimationType;
import net.daum.adam.publisher.AdView.OnAdClickedListener;
import net.daum.adam.publisher.AdView.OnAdClosedListener;
import net.daum.adam.publisher.AdView.OnAdFailedListener;
import net.daum.adam.publisher.AdView.OnAdLoadedListener;
import net.daum.adam.publisher.AdView.OnAdWillLoadListener;
import net.daum.adam.publisher.impl.AdError;
import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.adapter.ClubDetailAdapter;
import net.tomblog.app.android.swingheil.parser.RefreshAdapter;
import net.tomblog.app.android.swingheil.parser.RefreshEventList;
import net.tomblog.app.android.swingheil.parser.server.EventListParser;
import net.tomblog.app.android.swingheil.parser.server.FavoriteParser;
import net.tomblog.app.android.swingheil.util.ClubAndBarHolder;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.EventComparator;
import net.tomblog.app.android.swingheil.util.ImageResource;
import net.tomblog.app.android.swingheil.util.L;
import net.tomblog.app.android.swingheil.view.ActionItem;
import net.tomblog.app.android.swingheil.view.QuickAction;
import net.tomblog.app.android.swingheil.view.SwipyRefreshLayout;
import net.tomblog.app.android.swingheil.view.SwipyRefreshLayoutDirection;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.swingheil.domain.event.Event;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.type.place.Subway;

/**
 * Club 상세화면을 보여주는 Activity
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class ClubDetailActivity extends ActionBarActivity implements RefreshAdapter, RefreshEventList {

	private static final String TAG = ClubDetailActivity.class.getName();
	
    private LinearLayout adWrapper = null;
    private AdView adView = null;
    
	private ListView mListView;
	private SwipyRefreshLayoutDirection mRefreshDirection = null; 
	private SwipyRefreshLayout mSwipeRefreshLayout;
	private ClubDetailAdapter mClubDetailAdapter = null;

	private ArrayList<Event> mClubEventList = null;
	private Map<String, Event> mEventMap = null;
	private Club mClub = null;
	private Bar mBar = null;
	private Menu mMenu;
	private Event mSelectedEvent = null;
	private int mPage = 1;
	
	private int mDefaultInfoSize = 8;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		L.d(TAG, " >>> called onCreate()");  
		
		setContentView(R.layout.activity_club_detail); 
  
        adWrapper = (LinearLayout) findViewById(R.id.adWrapper);
        initAdam();
        
		inflateView();

		initObject(getIntent());
		
		L.d(TAG, " mDefaultInfoSize : " + mDefaultInfoSize);
		
		if(mClub != null) {
			new EventListParser(getApplicationContext(), this, Constants.SWING_CLUB, mPage, mClub.getId()).execute();
		}
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		L.d(TAG, " >>> called onNewIntent()");  
		
		setContentView(R.layout.activity_club_detail); 
		  
		inflateView();

		initObject(intent);
		
		L.d(TAG, " mDefaultInfoSize : " + mDefaultInfoSize);
		
		if(mClub != null) {
//			new EventListParser(getApplicationContext(), this, Constants.SWING_CLUB, mPage, mClub.getId()).execute();
		}
	}
	
	private void inflateView() {
		L.d(TAG, " >>> called inflateView()");
	
		mListView = (ListView)findViewById(R.id.clubDetailList);
		mSwipeRefreshLayout = (SwipyRefreshLayout)findViewById(R.id.swipe_refresh_layout); 
	}

    private void initObject(Intent intent) {
    	L.d(TAG, " >>> called initObject()");
    	
    	String clubId = intent.getStringExtra(Constants.CLUB_ID);
    	L.d(TAG, "clubId : " + clubId);
    	mClub = ClubAndBarHolder.getInstance().retrieveClub(clubId);
    	if(mClub != null) {
    		L.d(TAG, mClub.getNameKor());
    		mClub.setNew(false);
    		SharedPreferences prefs = getSharedPreferences(Constants.EVENT_PREF, Context.MODE_PRIVATE);
    		SharedPreferences.Editor editor = prefs.edit();
			editor.putLong(Constants.CLUB_ID + clubId, mClub.getLastUpdateDate().getTime());
			editor.commit(); 
    	}else {
    		finish();
    		return;
    	}
    	
    	ClubAndBarHolder holder = ClubAndBarHolder.getInstance();
    	mBar = holder.retrieveBar(mClub.getBar().getId());
    	Subway subway = mBar.getAddress().getMap().getSubway();
    	if(subway == null) {
    		mDefaultInfoSize = 7;  
    	}
    	
    	String clubName = mClub.getNameKor();
		String nameEngStr = mClub.getNameEng().replace(" ", "");
		if(!nameEngStr.isEmpty()) {
			clubName = clubName + " (" + mClub.getNameEng() + ")";
		}
    	
		Bitmap thumbnail = BitmapFactory.decodeResource(getResources(), ImageResource.CLUB[Integer.valueOf(clubId)]);
		thumbnail = Bitmap.createScaledBitmap(thumbnail, 280, 280, true);
		BitmapDrawable icon = new BitmapDrawable(thumbnail);
		
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.brown)));
        actionBar.setTitle(clubName); 
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setIcon(icon);
        
        mEventMap = new HashMap<String, Event>();
        mClubEventList = new ArrayList<Event>();
    	for (int i = 0; i < mDefaultInfoSize; i++) {
    		mClubEventList.add(new Event());
		}
    	
    	final QuickAction mQuickAction = new QuickAction(this);
		ActionItem addItem = new ActionItem(QuickAction.DETAIL, getResources().getString(R.string.quick_action_detail), 
				getResources().getDrawable(R.drawable.popup_menu_icon_detail));
		ActionItem acceptItem = new ActionItem(QuickAction.FAVORITE, getResources().getString(R.string.quick_action_favorite), 
				getResources().getDrawable(R.drawable.popup_menu_icon_favorite));
		ActionItem uploadItem = new ActionItem(QuickAction.UNFAVORITE, getResources().getString(R.string.quick_action_unfavorite), 
				getResources().getDrawable(R.drawable.popup_menu_icon_unfavorite)); 
		mQuickAction.addActionItem(addItem);
		mQuickAction.addActionItem(acceptItem);
		mQuickAction.addActionItem(uploadItem);
		mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction quickAction, int pos, int actionId) {
				if (actionId == QuickAction.DETAIL) { 
					Intent intent = new Intent(getApplicationContext(), EventDetailActivity.class);
					intent.putExtra(Constants.EVENT, mSelectedEvent); 
					startActivityForResult(intent, Constants.REQUEST_EVENT_DETAIL);
				} else if(mSelectedEvent != null && mSelectedEvent instanceof Event){
					if(actionId == QuickAction.FAVORITE){
						mClubDetailAdapter.addFavorite(mSelectedEvent);
					} else if(actionId == QuickAction.UNFAVORITE){
						mClubDetailAdapter.removeFavorite(mSelectedEvent);
					}
				} 
			}
		});
		mQuickAction.setOnDismissListener(new PopupWindow.OnDismissListener() {
			@Override
			public void onDismiss() {
			}
		});
    	
		mClubDetailAdapter = new ClubDetailAdapter(LayoutInflater.from(this), mClubEventList, mClub, mBar, this);
		mListView.setAdapter(mClubDetailAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				L.d(TAG, "onItemClick() is called position : " + position + " id : " + id);
				Event event = (Event)view.getTag(R.id.tag_event);
				if(event != null) {
					L.d(TAG, "event title : " + event.getTitle());
					Intent intent = new Intent(getApplicationContext(), EventDetailActivity.class);
					intent.putExtra(Constants.EVENT, event); 
					startActivityForResult(intent, Constants.REQUEST_EVENT_DETAIL); 
				}
			}
		});
		mListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				L.d(TAG, "onItemLongClick() is called position : " + position + " id : " + id);
				mSelectedEvent = (Event)view.getTag(R.id.tag_event);

				if(mSelectedEvent != null) {
					mQuickAction.show(view);
				}
				 
				return true;
			}
		});
		
		mSwipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                L.d("MainActivity", "Refresh triggered at "
                    + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
                refreshContent(direction);
                mSwipeRefreshLayout.setRefreshing(false);
            }
		});
		mSwipeRefreshLayout.setColorSchemeResources(R.color.blue, R.color.green, R.color.orange, R.color.red);
    }   	

	private void refreshContent(SwipyRefreshLayoutDirection direction) {
		if(mClub != null) {
			if(direction == SwipyRefreshLayoutDirection.TOP) {
				mRefreshDirection = SwipyRefreshLayoutDirection.TOP;
				new EventListParser(getApplicationContext(), this, Constants.SWING_CLUB, 1, mClub.getId()).execute();
			}else {
				mRefreshDirection = SwipyRefreshLayoutDirection.BOTTOM;
				new EventListParser(getApplicationContext(), this, Constants.SWING_CLUB, mPage, mClub.getId()).execute();
			}
		}
	}
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_menu, menu); 
        mMenu = menu;
        return true; 
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	
    	if(mClub != null) {
    		MenuItem favoriteItem = mMenu.findItem(R.id.favorite);
    		MenuItem unfavoriteItem = mMenu.findItem(R.id.unfavorite);
    		if(mClub.isFavorite()) {
    			favoriteItem.setVisible(true);
    			unfavoriteItem.setVisible(false);
    		}else {
    			favoriteItem.setVisible(false);
    			unfavoriteItem.setVisible(true);
    		}
    	}
    	
    	return super.onPrepareOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.favorite:
			Toast.makeText(getApplicationContext(), R.string.do_unfavorite, Toast.LENGTH_LONG).show();
			MenuItem unfavoriteItem = mMenu.findItem(R.id.unfavorite);
			unfavoriteItem.setVisible(true);
			item.setVisible(false);
			mClub.setFavorite(false);
			
			new FavoriteParser(Constants.CLUB, Constants.ACTION_FOLLOW, mClub.getId()).execute();
			
			return true;
		case R.id.unfavorite:
			Toast.makeText(getApplicationContext(), R.string.do_favorite, Toast.LENGTH_LONG).show();
			MenuItem favoriteItem = mMenu.findItem(R.id.favorite);
			favoriteItem.setVisible(true); 
			item.setVisible(false);
			mClub.setFavorite(true);

			new FavoriteParser(Constants.CLUB, Constants.ACTION_UNFOLLOW, mClub.getId()).execute();
			
			return true;
		default:
			return super.onOptionsItemSelected(item); 
		}
    }
    
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data); 
		L.d(TAG, " >>> called onActivityResult() requestCode : " + requestCode + ", resultCode : " + resultCode);
		
		if(requestCode == Constants.REQUEST_EVENT_DETAIL && resultCode== Activity.RESULT_OK){
			Event event = (Event) data.getSerializableExtra(Constants.EVENT);
	    	if(event != null) {
	    		if(event.isFavorite()) { 
	    			mClubDetailAdapter.addFavorite(event);
	    		}else {
	    			mClubDetailAdapter.removeFavorite(event);
	    		}
	    		
	    	}
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		L.d(TAG, " >>> called onDestroy()");
		
        if ( adView != null ) {
            adView.destroy();
            adView = null;
        }		
	}
	
	private void initAdam() {
        // Ad@m sdk 초기화 시작
		adView = (AdView) findViewById(R.id.adview);
        adView.setRequestInterval(5);

        // 광고 클릭시 실행할 리스너
        adView.setOnAdClickedListener(new OnAdClickedListener() {
            public void OnAdClicked() {
                L.d(TAG, "광고를 클릭했습니다.");
            }
        });

        // 광고 내려받기 실패했을 경우에 실행할 리스너
        adView.setOnAdFailedListener(new OnAdFailedListener() {
            public void OnAdFailed(AdError arg0, String arg1) {
                adWrapper.setVisibility(View.GONE);
                L.d(TAG, arg1);
            }
        });

        // 광고를 정상적으로 내려받았을 경우에 실행할 리스너
        adView.setOnAdLoadedListener(new OnAdLoadedListener() {
            public void OnAdLoaded() {
                adWrapper.setVisibility(View.VISIBLE);
                L.d(TAG, "광고가 정상적으로 로딩되었습니다.");
            }
        });

        // 광고를 불러올때 실행할 리스너
        adView.setOnAdWillLoadListener(new OnAdWillLoadListener() {
            public void OnAdWillLoad(String arg1) {
                L.d(TAG, "광고를 불러옵니다. : " + arg1);
            }
        });

        // 광고를 닫았을때 실행할 리스너
        adView.setOnAdClosedListener(new OnAdClosedListener() {
            public void OnAdClosed() {
                L.d(TAG, "광고를 닫았습니다.");
            }
        });

        // 할당 받은 clientId 설정
        adView.setClientId(Constants.CLIENT_ID);

        adView.setRequestInterval(12);

        // Animation 효과 : 기본 값은 AnimationType.NONE
        adView.setAnimationType(AnimationType.FLIP_HORIZONTAL);

        adView.setVisibility(View.VISIBLE);
    }	
	
	
	@Override
	public void refresh(List<Event> eventList) {
		// 기본 정보들을 제외하고 현재있는 공지사항들을 모두 삭제한다. 
		
		if (eventList != null) {
			int lastEventNum = mClubEventList.size();
			for (Event event : eventList) {
				if (!mEventMap.containsKey(event.getId())) {
					mClubEventList.add(event);
					mEventMap.put(event.getId(), event);
					L.d(TAG, event.getId() + event.getTitle() + " was added.");
				}
			}

			if (eventList.size() != 0) {
				Collections.sort(mClubEventList, new EventComparator());
				refreshAdapter();
				if (mRefreshDirection == null) {
					// 처음 진입시 자동로딩 
					mPage++;
				}else if (mRefreshDirection == SwipyRefreshLayoutDirection.BOTTOM) {
					// 화면의 아래에서 Pull Up 로딩 
					mPage++;
					mListView.setSelection(lastEventNum - 1);
				} // 화면의 상단에서 Pull Down 로딩 
			}
			
		}
	}

	@Override
	public void refreshAdapter() {
		if (mClubDetailAdapter != null) {
			mClubDetailAdapter.notifyDataSetChanged();
		}
	}
}
