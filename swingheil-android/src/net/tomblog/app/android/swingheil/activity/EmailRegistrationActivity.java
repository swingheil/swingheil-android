package net.tomblog.app.android.swingheil.activity;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.parser.RefreshResult;
import net.tomblog.app.android.swingheil.parser.server.user.RegisterEmailParser;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.L;
import net.tomblog.app.android.swingheil.util.Util;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.swingheil.domain.shared.ResponseMessage;
import com.swingheil.domain.user.SocialUser;

/**
 * Email, 비밀번호, 전화번호를 등록하는 Activity
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class EmailRegistrationActivity extends ActionBarActivity implements RefreshResult {

	private static final String TAG = EmailRegistrationActivity.class.getName();
	
	private EditText mEmailET;
	private EditText mPsswordET;
	private EditText mPasswordCheckET;
	private EditText mPhoneET;
	private Button mRegisterBtn;
	private RefreshResult mRefreshResult = null;
	
	private boolean isClicked = false;
	private String mGroupType = null;
	private String mGroupId = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		L.d(TAG, " >>> called onCreate()");  
		
		setContentView(R.layout.activity_email_registration); 
  
		inflateView();

		initObject(getIntent());
	}
	
	
	private void inflateView() {
		L.d(TAG, " >>> called inflateView()");
	
		mEmailET = (EditText)findViewById(R.id.email_edittext);
		mPsswordET = (EditText)findViewById(R.id.password_edittext);
		mPasswordCheckET = (EditText)findViewById(R.id.password_check_edittext);
		mPhoneET = (EditText)findViewById(R.id.phone_edittext);
		mRegisterBtn = (Button)findViewById(R.id.register_button);
	}

    private void initObject(Intent intent) {
    	L.d(TAG, " >>> called initObject()");
    	
    	mRefreshResult = this;
    	
    	mGroupType = intent.getStringExtra(Constants.GROUP_TYPE);
    	mGroupId = intent.getStringExtra(Constants.GROUP_ID);
    	
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.brown)));
        actionBar.setTitle(getResources().getText(R.string.email_registration)); 
        actionBar.setDisplayHomeAsUpEnabled(true);
        
        mRegisterBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!isClicked) {
					isClicked = true;
					String email = mEmailET.getText().toString();
					String password = mPsswordET.getText().toString();
					String passwordCheck = mPasswordCheckET.getText().toString();
					String phone = mPhoneET.getText().toString();
					
					boolean isValid = isValid(email, password, passwordCheck, phone);
					if(!isValid) {
						isClicked = false;
						return;
					}

					SocialUser socialUser = new SocialUser();
					socialUser.setEmail(email);
					socialUser.setPassword(password);
					socialUser.setPhone(phone);
					
					new RegisterEmailParser(socialUser, mRefreshResult).execute();
				}
			}

			private boolean isValid(String email, String password, String passwordCheck, String phone) {
				boolean isValid = false;
				
				if(Util.isEmpty(email)) {
					Toast.makeText(getApplicationContext(), R.string.toast_invalid_email, Toast.LENGTH_LONG).show();
					mEmailET.requestFocus();
				} else if(Util.isEmpty(password)) {
					Toast.makeText(getApplicationContext(), R.string.toast_invalid_password, Toast.LENGTH_LONG).show();
					mPsswordET.requestFocus();
				} else if(Util.isEmpty(passwordCheck)) {
					Toast.makeText(getApplicationContext(), R.string.toast_invalid_password, Toast.LENGTH_LONG).show();
					mPasswordCheckET.requestFocus();
				} else if(!password.equals(passwordCheck)) {
					Toast.makeText(getApplicationContext(), R.string.toast_invalid_password_check, Toast.LENGTH_LONG).show();
					mPsswordET.requestFocus();
				} else if(Util.isEmpty(phone)) {
					Toast.makeText(getApplicationContext(), R.string.toast_invalid_phone, Toast.LENGTH_LONG).show();
					mPhoneET.requestFocus();
				} else {
					isValid = true;
				}
				
				return isValid;
			}
		});
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item); 
		}
    }

	@Override
	public void refreshResult(ResponseMessage responseMessage) {
		L.d(TAG, " >>> called refreshResult()");
		if(responseMessage != null) {
			if(responseMessage.isSuccess()) {
				Toast.makeText(getApplicationContext(), R.string.toast_register_success, Toast.LENGTH_LONG).show();
				Intent intent = new Intent(getApplicationContext(), JoinAsStaffActivity.class);
				intent.putExtra(Constants.GROUP_TYPE, mGroupType);
				intent.putExtra(Constants.GROUP_ID, mGroupId);
				startActivity(intent); 
				finish();
			} else {
				isClicked = false;
				Toast.makeText(getApplicationContext(), R.string.toast_register_failed, Toast.LENGTH_LONG).show();
			}
		}
	}
}