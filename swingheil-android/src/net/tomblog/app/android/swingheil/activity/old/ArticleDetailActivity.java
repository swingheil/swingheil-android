package net.tomblog.app.android.swingheil.activity.old;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.util.L;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * WebView를 사용해 게시글을 보여주는 Activity
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class ArticleDetailActivity extends BaseActivity {

	private static final String TAG = ArticleDetailActivity.class.getName();
	
	public static final String PARAM_URL = "net.tomblog.app.swingheil.URL";
	
	private WebView mArticleView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		L.d(TAG, " >>> called onCreate()");  
		requestWindowFeature(Window.FEATURE_PROGRESS);
		setContentView(R.layout.activity_article_detail); 
  
		inflateView();

		initObject();
		
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.article_list_view_menu, menu);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.refresh) {
			mArticleView.reload();
			return true;
		} else {
		}

		return false;
	}
	
	private void inflateView() {
		L.d(TAG, " >>> called inflateView()");
	
		mRootView  = (ViewGroup)findViewById(R.id.root);
		mArticleView = (WebView)findViewById(R.id.article_view);
	}

    private void initObject() {
    	L.d(TAG, " >>> called initObject()");

    	mArticleView.setVerticalScrollbarOverlay(true);
    	mArticleView.getSettings().setJavaScriptEnabled(true);
    	mArticleView.getSettings().setBuiltInZoomControls(true);
    	mArticleView.setWebChromeClient(new WebChromeClient() {

    		@Override
        	public void onProgressChanged(WebView view, int newProgress) {
        		setProgress(newProgress * 100);
        		super.onProgressChanged(view, newProgress);
        	}
        });
    	mArticleView.setWebViewClient(new WebViewClient() {

			@Override
			public void onPageFinished(WebView view, String url) {
				L.d(TAG, ">>> Called onPageFinished(), url=" + url);
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				L.d(TAG, ">>> Called shouldOverrideUrlLoading(), url=" + url);
				return false;
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon)	{
				L.d(TAG, ">>> Called onPageStarted(), url=" + url);
				super.onPageStarted(view, url, favicon);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				L.d(TAG, ">>> Called onReceivedError(), errorCode=" + errorCode);
				L.d(TAG, ">>> Called onReceivedError(), failingUrl=" + failingUrl);
				super.onReceivedError(view, errorCode, description, failingUrl);
			}
			
		});
    	
    	Intent intent = getIntent(); 
    	String url = intent.getStringExtra(PARAM_URL);
    	L.d(TAG, "url : " + url);
    	mArticleView.loadUrl(url);
    }   	
}