package net.tomblog.app.android.swingheil.activity;

import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import net.daum.adam.publisher.AdView;
import net.daum.adam.publisher.AdView.AnimationType;
import net.daum.adam.publisher.AdView.OnAdClickedListener;
import net.daum.adam.publisher.AdView.OnAdClosedListener;
import net.daum.adam.publisher.AdView.OnAdFailedListener;
import net.daum.adam.publisher.AdView.OnAdLoadedListener;
import net.daum.adam.publisher.AdView.OnAdWillLoadListener;
import net.daum.adam.publisher.impl.AdError;
import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.SwingHeilApplication;
import net.tomblog.app.android.swingheil.fragment.SlidingTabsColorsFragment;
import net.tomblog.app.android.swingheil.model.server.ResultBars;
import net.tomblog.app.android.swingheil.model.server.ResultClubs;
import net.tomblog.app.android.swingheil.parser.server.BarListParser;
import net.tomblog.app.android.swingheil.parser.server.ClubListParser;
import net.tomblog.app.android.swingheil.provider.EventDBHelper;
import net.tomblog.app.android.swingheil.util.ClubAndBarHolder;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.L;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;

/**
 * Swing Heil 시작 Activity
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 * 
 */
public class SwingHeilActivity extends FragmentActivity {
	
	public static final String TAG = SwingHeilActivity.class.getName();

    private LinearLayout adWrapper = null;
    private AdView adView = null;
    
	ResultClubs clubs = null;
	ResultBars bars = null;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); 
        L.d(TAG, " >>> called onCreate()");
        setContentView(R.layout.activity_swingheil); 

        adWrapper = (LinearLayout) findViewById(R.id.adWrapper);
        initAdam();
        
        loadClubAndBarInfo();
        
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        SlidingTabsColorsFragment fragment = new SlidingTabsColorsFragment(); 
        transaction.replace(R.id.sliding_tab_menu, fragment); 
        transaction.commit(); 
        
        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.brown)));

        // isNew 이벤트 중에 Constants.NEW_DAY_LIMIT -1 일 이전에 등록된 것은 DB에서 삭제한다.
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, Constants.NEW_DAY_LIMIT - 1); 
        EventDBHelper db = new EventDBHelper(this);
        db.deleteOldEvent(calendar.getTime());
        
        String userId = SwingHeilApplication.getInstance().getUserId();
        L.e(TAG, "userId : " + userId);
    }

	@Override
    protected void onStart() {
    	super.onStart();
        L.d(TAG, " >>> called onStart()");
    	
    }
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		L.d(TAG, " >>> called onActivityResult() requestCode : " + requestCode + ", resultCode : " + resultCode);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		L.d(TAG, " >>> called onDestroy()");
		
		new ClubListParser(this, null).execute();
		new BarListParser(this, null).execute();
		
        if ( adView != null ) {
            adView.destroy();
            adView = null;
        }		
	}

   private void initAdam() {
        // Ad@m sdk 초기화 시작
        adView = (AdView) findViewById(R.id.adview);
        adView.setRequestInterval(5);

        // 광고 클릭시 실행할 리스너
        adView.setOnAdClickedListener(new OnAdClickedListener() {
            public void OnAdClicked() {
                L.d(TAG, "광고를 클릭했습니다.");
            }
        });

        // 광고 내려받기 실패했을 경우에 실행할 리스너
        adView.setOnAdFailedListener(new OnAdFailedListener() {
            public void OnAdFailed(AdError arg0, String arg1) {
                adWrapper.setVisibility(View.GONE);
                L.d(TAG, arg1);
            }
        });

        // 광고를 정상적으로 내려받았을 경우에 실행할 리스너
        adView.setOnAdLoadedListener(new OnAdLoadedListener() {
            public void OnAdLoaded() {
                adWrapper.setVisibility(View.VISIBLE);
                L.d(TAG, "광고가 정상적으로 로딩되었습니다.");
            }
        });

        // 광고를 불러올때 실행할 리스너
        adView.setOnAdWillLoadListener(new OnAdWillLoadListener() {
            public void OnAdWillLoad(String arg1) {
                L.d(TAG, "광고를 불러옵니다. : " + arg1);
            }
        });

        // 광고를 닫았을때 실행할 리스너
        adView.setOnAdClosedListener(new OnAdClosedListener() {
            public void OnAdClosed() {
                L.d(TAG, "광고를 닫았습니다.");
            }
        });

        // 할당 받은 clientId 설정
        adView.setClientId(Constants.CLIENT_ID);

        adView.setRequestInterval(12);

        // Animation 효과 : 기본 값은 AnimationType.NONE
        adView.setAnimationType(AnimationType.FLIP_HORIZONTAL);

        adView.setVisibility(View.VISIBLE);
    }
   
    /**
     * This method converts dp unit to equivalent pixels, depending on device density. 
     * 
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     * 
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }
    
    private void loadClubAndBarInfo() {
    	L.d(TAG, " >>> called loadClubAndBarInfo()");
    	 
    	// 1. preference에 bars와 clubs가 있는지 확인한다. 
    	// 1-1. 있으면 해당 데이터를 읽어서 list를 만든다.
    	// 1-2-1. 없으면 raws의 bars.json과 clubs.json을 읽어서 list를 만든다.
    	// 1-2-2. preference에 bars와 clubs를 저장한다. 
    	// 2. (online 상태일 경우) server에서 bars와 clubs 데이터를 가져와 list를 교체하고 preference에 저장한다.

    	SharedPreferences prefs = getSharedPreferences(Constants.CLUB_AND_BAR_PREF, Context.MODE_PRIVATE);
    	
    	String isThereClubInfo = prefs.getString(Constants.IS_THERE_CLUB_INFO, "N");
    	L.d(TAG, " >>> isThereClubInfo : " + isThereClubInfo);
    	if("Y".equalsIgnoreCase(isThereClubInfo)) {
    	}else {
    		try {
	    		InputStream inputStream = getResources().openRawResource(R.raw.clubs); 
				StringWriter writer = new StringWriter();
				IOUtils.copy(inputStream, writer, "UTF-8");
				String jsonData = writer.toString();
				
				JSONObject reader = new JSONObject(jsonData);
				String clubJson = reader.getString(Constants.JSON_CLUBS);
				if(!clubJson.isEmpty()) {
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString(Constants.JSON_CLUBS, clubJson);
					editor.putString(Constants.IS_THERE_CLUB_INFO, "Y");
					editor.commit();
				}
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
		String clubJson = prefs.getString(Constants.JSON_CLUBS, "");
//		L.d(TAG, "clubJson : " + clubJson);
		if(!clubJson.isEmpty()) {
			try {
				GsonBuilder builder = new GsonBuilder(); 
				builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
					@Override
					public Date deserialize(JsonElement json, Type typeOfT,
							JsonDeserializationContext context) throws JsonParseException {
						return json == null ? null : new Date(json.getAsLong());
					}
				});
				Gson gson = builder.create();  
				Type type = new TypeToken<List<Club>>(){}.getType();
				List<Club> clubList  = gson.fromJson(clubJson, type);
				
				ClubAndBarHolder holder = ClubAndBarHolder.getInstance();
				if(clubList != null) {
					for (Club club : clubList) {
						holder.saveClub(club.getId(), club);
						holder.saveBarOfClub(club);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    	
    	String isThereBarInfo = prefs.getString(Constants.IS_THERE_BAR_INFO, "N");
    	L.d(TAG, " >>> isThereBarInfo : " + isThereBarInfo);
    	if("Y".equalsIgnoreCase(isThereBarInfo)) {
    	}else {
    		try {
    			InputStream inputStream = getResources().openRawResource(R.raw.bars);
    			StringWriter writer = new StringWriter();
    			IOUtils.copy(inputStream, writer, "UTF-8");
    			String jsonData = writer.toString();
    			
    			JSONObject reader = new JSONObject(jsonData);
    			String barsJson = reader.getString(Constants.JSON_BARS);
    			if(!barsJson.isEmpty()) {
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString(Constants.JSON_BARS, barsJson);
					editor.putString(Constants.IS_THERE_BAR_INFO, "Y");
					editor.commit();
				}
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
    	
		String barsJson = prefs.getString(Constants.JSON_BARS, "");
//		L.d(TAG, "barsJson : " + barsJson);
		if(!barsJson.isEmpty()) {
			try {
				GsonBuilder builder = new GsonBuilder(); 
				builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
					@Override
					public Date deserialize(JsonElement json, Type typeOfT,
							JsonDeserializationContext context) throws JsonParseException {
						return json == null ? null : new Date(json.getAsLong());
					}
				});
				Gson gson = builder.create();  
				Type type = new TypeToken<List<Bar>>(){}.getType();
				List<Bar> barList  = gson.fromJson(barsJson, type);
				
				ClubAndBarHolder holder = ClubAndBarHolder.getInstance();
				for (Bar bar : barList) {
					holder.saveBar(bar.getId(), bar);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
    
}