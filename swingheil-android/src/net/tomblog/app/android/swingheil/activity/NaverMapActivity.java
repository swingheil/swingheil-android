/* 
 * NMapViewer.java $version 2010. 1. 1
 * 
 * Copyright 2010 NHN Corp. All rights Reserved. 
 * NHN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms. 
 */

package net.tomblog.app.android.swingheil.activity;

import java.util.List;

import net.daum.adam.publisher.AdView;
import net.daum.adam.publisher.AdView.AnimationType;
import net.daum.adam.publisher.AdView.OnAdClickedListener;
import net.daum.adam.publisher.AdView.OnAdClosedListener;
import net.daum.adam.publisher.AdView.OnAdFailedListener;
import net.daum.adam.publisher.AdView.OnAdLoadedListener;
import net.daum.adam.publisher.AdView.OnAdWillLoadListener;
import net.daum.adam.publisher.impl.AdError;
import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.util.ClubAndBarHolder;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.ImageResource;
import net.tomblog.app.android.swingheil.util.L;
import net.tomblog.app.android.swingheil.view.NMapCalloutCustomOldOverlay;
import net.tomblog.app.android.swingheil.view.NMapCalloutCustomOverlayView;
import net.tomblog.app.android.swingheil.view.NMapPOIflagType;
import net.tomblog.app.android.swingheil.view.NMapViewerResourceProvider;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.nhn.android.maps.NMapActivity;
import com.nhn.android.maps.NMapCompassManager;
import com.nhn.android.maps.NMapController;
import com.nhn.android.maps.NMapLocationManager;
import com.nhn.android.maps.NMapOverlay;
import com.nhn.android.maps.NMapOverlayItem;
import com.nhn.android.maps.NMapView;
import com.nhn.android.maps.maplib.NGeoPoint;
import com.nhn.android.maps.nmapmodel.NMapError;
import com.nhn.android.maps.nmapmodel.NMapPlacemark;
import com.nhn.android.maps.overlay.NMapPOIdata;
import com.nhn.android.maps.overlay.NMapPOIitem;
import com.nhn.android.maps.overlay.NMapPathData;
import com.nhn.android.maps.overlay.NMapPathLineStyle;
import com.nhn.android.mapviewer.overlay.NMapCalloutCustomOverlay;
import com.nhn.android.mapviewer.overlay.NMapCalloutOverlay;
import com.nhn.android.mapviewer.overlay.NMapMyLocationOverlay;
import com.nhn.android.mapviewer.overlay.NMapOverlayManager;
import com.nhn.android.mapviewer.overlay.NMapPOIdataOverlay;
import com.nhn.android.mapviewer.overlay.NMapPathDataOverlay;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.type.place.Address;
import com.swingheil.domain.type.place.GeoLocation;
import com.swingheil.domain.type.place.Station;
import com.swingheil.domain.type.place.Subway;

/**
 * Sample class for map viewer library.
 * 
 * @author kyjkim
 */
public class NaverMapActivity extends NMapActivity {
	
	private static final String TAG = NaverMapActivity.class.getName();

	private RelativeLayout relativeLayout = null;
    private LinearLayout adWrapper = null;
    private AdView adView = null;
    
	// set your API key which is registered for NMapViewer library.
	private static final String API_KEY = "abce8300e703b12280824afa7321b0d2";

	private Bar mBar = null;
	private String mBarName = "";
	private String mAddress = "";
	private List<GeoLocation> mGeoLocationList = null;
	private Subway mSubway = null;
	private int mAddressType = Constants.ADDRESS_TYPE_ROAD;
	
	private MapContainerView mMapContainerView;

	private NMapView mMapView;
	private NMapController mMapController;

	private NMapOverlayManager mOverlayManager;

	private NMapMyLocationOverlay mMyLocationOverlay;
	private NMapLocationManager mMapLocationManager;
	private NMapCompassManager mMapCompassManager;

	private NMapViewerResourceProvider mMapViewerResourceProvider;

	private NMapPOIdataOverlay mFloatingPOIdataOverlay;
	private NMapPOIitem mFloatingPOIitem;
	
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		String barId = getIntent().getStringExtra(Constants.BAR_ID);
    	mBar = ClubAndBarHolder.getInstance().retrieveBar(barId); 
    	if(mBar != null && 
    			mBar.getAddress() != null &&
    			mBar.getAddress().getMap() != null &&
    			mBar.getAddress().getMap().getPoints() != null) {
    		L.d(TAG, mBar.getNameKor());
    	}else {
    		String msg = "The GPS info doesn't exist."; 
    		L.d(TAG, msg);
    		Toast.makeText(NaverMapActivity.this, msg, Toast.LENGTH_SHORT).show();
    		finish();
    		return;
    	}
    	mAddressType = getIntent().getIntExtra(Constants.ADDRESS_TYPE, Constants.ADDRESS_TYPE_ROAD);
        
		inflateView();
		
		initObject();
		
		// create parent view to rotate map view
		mMapContainerView = new MapContainerView(this);
		mMapContainerView.addView(mMapView);

		adWrapper = (LinearLayout) findViewById(R.id.adWrapper);
		initAdam();
		
		relativeLayout = new RelativeLayout(this);
		
		relativeLayout.addView(mMapContainerView);
		relativeLayout.addView(adView);
		
		// set the activity content to the parent view
		setContentView(relativeLayout);
		
        // XML상에 android:layout_alignParentBottom="true" 와 같은 역할을 함
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        // 위에서 만든 레이아웃을 광고 뷰에 적용함.
        adView.setLayoutParams(params);

		// set a registered API key for Open MapViewer Library
		mMapView.setApiKey(API_KEY);

		// initialize map view
		mMapView.setClickable(true);
		mMapView.setEnabled(true);
		mMapView.setFocusable(true);
		mMapView.setFocusableInTouchMode(true);
		mMapView.requestFocus();

		// register listener for map state changes
		mMapView.setOnMapStateChangeListener(onMapViewStateChangeListener);
		mMapView.setOnMapViewTouchEventListener(onMapViewTouchEventListener);
		mMapView.setOnMapViewDelegate(onMapViewTouchDelegate);
		
		// use map controller to zoom in/out, pan and set map center, zoom level etc.
		mMapController = mMapView.getMapController();

		// use built in zoom controls
		NMapView.LayoutParams lp = new NMapView.LayoutParams(LayoutParams.WRAP_CONTENT,
			LayoutParams.WRAP_CONTENT, NMapView.LayoutParams.BOTTOM_RIGHT);
		mMapView.setBuiltInZoomControls(true, lp);
//		mMapView.displayZoomControls(true);
		mMapView.setScalingFactor(2.0f);

		// create resource provider
		mMapViewerResourceProvider = new NMapViewerResourceProvider(this);

		// set data provider listener
		super.setMapDataProviderListener(onDataProviderListener);

		// create overlay manager
		mOverlayManager = new NMapOverlayManager(this, mMapView, mMapViewerResourceProvider);
		// register callout overlay listener to customize it.
		mOverlayManager.setOnCalloutOverlayListener(onCalloutOverlayListener);
		// register callout overlay view listener to customize it.
		mOverlayManager.setOnCalloutOverlayViewListener(onCalloutOverlayViewListener);

		// location manager
		mMapLocationManager = new NMapLocationManager(this);
		mMapLocationManager.setOnLocationChangeListener(onMyLocationChangeListener);

		// compass manager
		mMapCompassManager = new NMapCompassManager(this);

		// create my location overlay
		mMyLocationOverlay = mOverlayManager.createMyLocationOverlay(mMapLocationManager, mMapCompassManager);
		
		
		new Handler().postDelayed(new Runnable() {
			 
			@Override
			public void run() {
				mOverlayManager.clearOverlays();
				
				// add path data overlay
				testPathDataOverlay();
				
				// add path POI data overlay
				testPathPOIdataOverlay();
				
				Toast.makeText(NaverMapActivity.this, mAddress, Toast.LENGTH_LONG).show();
				
			}
		}, 100);
	}

	private void inflateView() {
		L.d(TAG, " >>> called inflateView()");
		
		// create map view
		mMapView = new NMapView(this);
	}

    private void initObject() {
    	L.d(TAG, " >>> called initObject()");
    	String barName = mBar.getNameKor();
		String nameEngStr = mBar.getNameEng().replace(" ", "");
		if(!nameEngStr.isEmpty()) {
			barName = barName + " (" + mBar.getNameEng() + ")";
		}
		mBarName = barName;
		
		int barResourceId = ImageResource.BAR[Integer.valueOf(mBar.getId())];
		if(barResourceId == R.drawable.bar_img_none_blue) {
			barResourceId = R.drawable.bar_img_none_white;  
		}
		Bitmap thumbnail = BitmapFactory.decodeResource(getResources(), barResourceId);
		thumbnail = Bitmap.createScaledBitmap(thumbnail, 280, 280, true);
		BitmapDrawable icon = new BitmapDrawable(thumbnail); 
		
        ActionBar actionBar = getActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.brown)));
        actionBar.setTitle(barName);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setIcon(icon);
        
        Address address = mBar.getAddress();
        mGeoLocationList = address.getMap().getPoints();
        mSubway = address.getMap().getSubway(); 
        
		if(Constants.ADDRESS_TYPE_ROAD == mAddressType) {
			mAddress = address.getRoad();
		}else {
			mAddress = address.getLand();
		}
    }
    	
	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onStop() {

		stopMyLocation();

		super.onStop();
	}

	/* Test Functions */

	private void startMyLocation() {

		if (mMyLocationOverlay != null) {
			if (!mOverlayManager.hasOverlay(mMyLocationOverlay)) {
				mOverlayManager.addOverlay(mMyLocationOverlay);
			}

			if (mMapLocationManager.isMyLocationEnabled()) {

				if (!mMapView.isAutoRotateEnabled()) {
					mMyLocationOverlay.setCompassHeadingVisible(true);

					mMapCompassManager.enableCompass();

					mMapView.setAutoRotateEnabled(true, false);

					mMapContainerView.requestLayout();
				} else {
					stopMyLocation();
				}

				mMapView.postInvalidate();
			} else {
				boolean isMyLocationEnabled = mMapLocationManager.enableMyLocation(true);
				if (!isMyLocationEnabled) {
					Toast.makeText(NaverMapActivity.this, "Please enable a My Location source in system settings",
						Toast.LENGTH_LONG).show();

					Intent goToSettings = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivity(goToSettings);

					return;
				}
			}
		}
	}

	private void stopMyLocation() {
		if (mMyLocationOverlay != null) { 
			mMapLocationManager.disableMyLocation();

			if (mMapView.isAutoRotateEnabled()) {
				mMyLocationOverlay.setCompassHeadingVisible(false);

				mMapCompassManager.disableCompass();

				mMapView.setAutoRotateEnabled(false, false);

				mMapContainerView.requestLayout();
			}
		}
	}

	private void testPathDataOverlay() {

		if(mGeoLocationList != null) {
			// set path data points
			NMapPathData pathData = new NMapPathData(mGeoLocationList.size());
			
			pathData.initPathData();
			for (GeoLocation geoLocation : mGeoLocationList) {
				pathData.addPathPoint(geoLocation.getLongitude(), geoLocation.getLatitude(), NMapPathLineStyle.TYPE_DASH);
			}
			pathData.endPathData();
			
			NMapPathDataOverlay pathDataOverlay = mOverlayManager.createPathDataOverlay(pathData);
			if (pathDataOverlay != null) {
				
				// show all path data
				pathDataOverlay.showAllPathData(0);
			}
		}
		
	}

	private void testPathPOIdataOverlay() {

		if(mGeoLocationList != null) {
			
			NMapPOIdata poiData = new NMapPOIdata(2, mMapViewerResourceProvider, true);
			int selectPointNumber = 0;
			
			int pointCount = mGeoLocationList.size(); 
			if(pointCount > 1) {
				// 지하철역에서 바까지의 경로가 있는 경우
				int markerCount = getMarkerCount(mGeoLocationList);
				
				poiData.beginPOIdata(markerCount);
				int gpsDescriptionCount = 0;
				for (GeoLocation geoLocation : mGeoLocationList) {
					String description = geoLocation.getDescription();
					if(description.startsWith("start")) { 
						gpsDescriptionCount++; 
						String stationAndExit = getStationAndExit(description);
						poiData.addPOIitem(new NGeoPoint(geoLocation.getLongitude(), geoLocation.getLatitude()), stationAndExit, NMapPOIflagType.FROM, null);
					} else if("end".equalsIgnoreCase(description)) {
						poiData.addPOIitem(new NGeoPoint(geoLocation.getLongitude(), geoLocation.getLatitude()), mBarName, NMapPOIflagType.TO, null);
						selectPointNumber = gpsDescriptionCount; 
						gpsDescriptionCount++; 
					} else if(!description.isEmpty()) {
						poiData.addPOIitem(new NGeoPoint(geoLocation.getLongitude(), geoLocation.getLatitude()), description, NMapPOIflagType.SPOT, null);
						gpsDescriptionCount++; 
					}
				}
			}else {
				// 바의 위치만 있는 경우
				poiData.beginPOIdata(1); 
				GeoLocation toPoint = mGeoLocationList.get(0);
				poiData.addPOIitem(new NGeoPoint(toPoint.getLongitude(), toPoint.getLatitude()), mBarName, NMapPOIflagType.TO, null);
				selectPointNumber = 0;
			}
			poiData.endPOIdata();
			L.d(TAG, "pointCount : " + pointCount + ", selectPointNumber : " + selectPointNumber);
			
			// create POI data overlay
			NMapPOIdataOverlay poiDataOverlay = mOverlayManager.createPOIdataOverlay(poiData, null);
			poiDataOverlay.selectPOIitem(selectPointNumber, true);
			
			// set event listener to the overlay
			poiDataOverlay.setOnStateChangeListener(onPOIdataStateChangeListener);
		}
	}

	/**
	 * 출발, 도착과 같은 마커가 표시될 카운트를 반환한다.
	 */
	private int getMarkerCount(List<GeoLocation> mGeoLocationList) {
		int markerCount = 0;
		
		for (GeoLocation geoLocation : mGeoLocationList) {
			if(!"".equalsIgnoreCase(geoLocation.getDescription())) {
				markerCount++;
			}
		}
		
		return markerCount;
	}

	private String getStationAndExit(String description) {
		String stationAndExit = "";
		
		L.d(TAG, "description : " + description);
		
		String stationName = description.replace("start_", "");
		
		L.d(TAG, "stationName : " + stationName);
		
		if(mSubway != null) {
			List<Station> stationList = mSubway.getStations();
			
			Station station = null;
			if(stationList != null) {
				for (Station aStation : stationList) {
					if(aStation != null && stationName.equalsIgnoreCase(aStation.getName())) {
						station  = aStation;
						break;
					}
				} 
			}
			
			if(station != null) {
				stationAndExit = station.getName() + " " + station.getExit() + "번 출구"; 
			}
		}
		
		return stationAndExit;
	}
	
	private void testPOIdataOverlay() {

		// Markers for POI item
		int markerId = NMapPOIflagType.PIN;

		// set POI data
		NMapPOIdata poiData = new NMapPOIdata(2, mMapViewerResourceProvider);
		poiData.beginPOIdata(2);
		NMapPOIitem item = poiData.addPOIitem(127.0630205, 37.5091300, "Pizza 777-111", markerId, 0);
		item.setRightAccessory(true, NMapPOIflagType.CLICKABLE_ARROW);
		poiData.addPOIitem(127.061, 37.51, "Pizza 123-456", markerId, 0);
		poiData.endPOIdata();

		// create POI data overlay
		NMapPOIdataOverlay poiDataOverlay = mOverlayManager.createPOIdataOverlay(poiData, null);

		// set event listener to the overlay
		poiDataOverlay.setOnStateChangeListener(onPOIdataStateChangeListener);

		// select an item
		poiDataOverlay.selectPOIitem(0, true);

		// show all POI data
		//poiDataOverlay.showAllPOIdata(0);
	}

	/* NMapDataProvider Listener */
	private final NMapActivity.OnDataProviderListener onDataProviderListener = new NMapActivity.OnDataProviderListener() {

		@Override
		public void onReverseGeocoderResponse(NMapPlacemark placeMark, NMapError errInfo) {


			if (errInfo != null) {
				Log.e(TAG, "Failed to findPlacemarkAtLocation: error=" + errInfo.toString());

				Toast.makeText(NaverMapActivity.this, errInfo.toString(), Toast.LENGTH_LONG).show();
				return;
			}

			if (mFloatingPOIitem != null && mFloatingPOIdataOverlay != null) {
				mFloatingPOIdataOverlay.deselectFocusedPOIitem();

				if (placeMark != null) {
					mFloatingPOIitem.setTitle(placeMark.toString());
				}
				mFloatingPOIdataOverlay.selectPOIitemBy(mFloatingPOIitem.getId(), false);
			}
		}

	};

	/* MyLocation Listener */
	private final NMapLocationManager.OnLocationChangeListener onMyLocationChangeListener = new NMapLocationManager.OnLocationChangeListener() {

		@Override
		public boolean onLocationChanged(NMapLocationManager locationManager, NGeoPoint myLocation) {

			if (mMapController != null) {
				mMapController.animateTo(myLocation);
			}

			return true;
		}

		@Override
		public void onLocationUpdateTimeout(NMapLocationManager locationManager) {

			// stop location updating
			//			Runnable runnable = new Runnable() {
			//				public void run() {										
			//					stopMyLocation();
			//				}
			//			};
			//			runnable.run();	

			Toast.makeText(NaverMapActivity.this, "Your current location is temporarily unavailable.", Toast.LENGTH_LONG).show();
		}

		@Override
		public void onLocationUnavailableArea(NMapLocationManager locationManager, NGeoPoint myLocation) {

			Toast.makeText(NaverMapActivity.this, "Your current location is unavailable area.", Toast.LENGTH_LONG).show();

			stopMyLocation();
		}

	};

	/* MapView State Change Listener*/
	private final NMapView.OnMapStateChangeListener onMapViewStateChangeListener = new NMapView.OnMapStateChangeListener() {

		@Override
		public void onMapInitHandler(NMapView mapView, NMapError errorInfo) {

			if (errorInfo == null) { // success
				// restore map view state such as map center position and zoom level.
//				restoreInstanceState();

			} else { // fail
				Log.e(TAG, "onFailedToInitializeWithError: " + errorInfo.toString());

				Toast.makeText(NaverMapActivity.this, errorInfo.toString(), Toast.LENGTH_LONG).show();
			}
		}

		@Override
		public void onAnimationStateChange(NMapView mapView, int animType, int animState) {
		}

		@Override
		public void onMapCenterChange(NMapView mapView, NGeoPoint center) {
		}

		@Override
		public void onZoomLevelChange(NMapView mapView, int level) {
		}

		@Override
		public void onMapCenterChangeFine(NMapView mapView) {

		}
	};

	private final NMapView.OnMapViewTouchEventListener onMapViewTouchEventListener = new NMapView.OnMapViewTouchEventListener() {

		@Override
		public void onLongPress(NMapView mapView, MotionEvent ev) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLongPressCanceled(NMapView mapView) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onSingleTapUp(NMapView mapView, MotionEvent ev) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTouchDown(NMapView mapView, MotionEvent ev) {

		}

		@Override
		public void onScroll(NMapView mapView, MotionEvent e1, MotionEvent e2) {
		}

		@Override
		public void onTouchUp(NMapView mapView, MotionEvent ev) {
			// TODO Auto-generated method stub

		}

	};

	private final NMapView.OnMapViewDelegate onMapViewTouchDelegate = new NMapView.OnMapViewDelegate() {

		@Override
		public boolean isLocationTracking() {
			if (mMapLocationManager != null) {
				if (mMapLocationManager.isMyLocationEnabled()) {
					return mMapLocationManager.isMyLocationFixed();
				}
			}
			return false;
		}

	};

	/* POI data State Change Listener*/
	private final NMapPOIdataOverlay.OnStateChangeListener onPOIdataStateChangeListener = new NMapPOIdataOverlay.OnStateChangeListener() {

		@Override
		public void onCalloutClick(NMapPOIdataOverlay poiDataOverlay, NMapPOIitem item) {
			Toast.makeText(NaverMapActivity.this, mAddress, Toast.LENGTH_LONG).show();
		}

		@Override 
		public void onFocusChanged(NMapPOIdataOverlay poiDataOverlay, NMapPOIitem item) {
		}
	};

	private final NMapPOIdataOverlay.OnFloatingItemChangeListener onPOIdataFloatingItemChangeListener = new NMapPOIdataOverlay.OnFloatingItemChangeListener() {

		@Override
		public void onPointChanged(NMapPOIdataOverlay poiDataOverlay, NMapPOIitem item) {
			NGeoPoint point = item.getPoint();

			findPlacemarkAtLocation(point.longitude, point.latitude);

			item.setTitle(null);

		}
	};

	private final NMapOverlayManager.OnCalloutOverlayListener onCalloutOverlayListener = new NMapOverlayManager.OnCalloutOverlayListener() {

		@Override
		public NMapCalloutOverlay onCreateCalloutOverlay(NMapOverlay itemOverlay, NMapOverlayItem overlayItem,
			Rect itemBounds) {

			// handle overlapped items
			if (itemOverlay instanceof NMapPOIdataOverlay) {
				NMapPOIdataOverlay poiDataOverlay = (NMapPOIdataOverlay)itemOverlay;

				// check if it is selected by touch event
				if (!poiDataOverlay.isFocusedBySelectItem()) {
					int countOfOverlappedItems = 1;

					NMapPOIdata poiData = poiDataOverlay.getPOIdata();
					for (int i = 0; i < poiData.count(); i++) {
						NMapPOIitem poiItem = poiData.getPOIitem(i);

						// skip selected item
						if (poiItem == overlayItem) {
							continue;
						}

						// check if overlapped or not
						if (Rect.intersects(poiItem.getBoundsInScreen(), overlayItem.getBoundsInScreen())) {
							countOfOverlappedItems++;
						}
					}

					if (countOfOverlappedItems > 1) {
						String text = countOfOverlappedItems + " overlapped items for " + overlayItem.getTitle();
						Toast.makeText(NaverMapActivity.this, text, Toast.LENGTH_LONG).show();
						return null;
					}
				}
			}

			// use custom old callout overlay
			if (overlayItem instanceof NMapPOIitem) {
				NMapPOIitem poiItem = (NMapPOIitem)overlayItem;

				if (poiItem.showRightButton()) {
					return new NMapCalloutCustomOldOverlay(itemOverlay, overlayItem, itemBounds,
						mMapViewerResourceProvider);
				}
			}

			// use custom callout overlay
			return new NMapCalloutCustomOverlay(itemOverlay, overlayItem, itemBounds, mMapViewerResourceProvider);

			// set basic callout overlay
			//return new NMapCalloutBasicOverlay(itemOverlay, overlayItem, itemBounds);			
		}

	};

	private final NMapOverlayManager.OnCalloutOverlayViewListener onCalloutOverlayViewListener = new NMapOverlayManager.OnCalloutOverlayViewListener() {

		@Override
		public View onCreateCalloutOverlayView(NMapOverlay itemOverlay, NMapOverlayItem overlayItem, Rect itemBounds) {

			if (overlayItem != null) {
				// [TEST] 말풍선 오버레이를 뷰로 설정함
				String title = overlayItem.getTitle();
				if (title != null && title.length() > 5) {
					return new NMapCalloutCustomOverlayView(NaverMapActivity.this, itemOverlay, overlayItem, itemBounds);
				}
			}

			// null을 반환하면 말풍선 오버레이를 표시하지 않음
			return null;
		}
	};


	/* Menus */
	private static final int MENU_ITEM_CLEAR_MAP = 10;
	private static final int MENU_ITEM_MAP_MODE = 20;
	private static final int MENU_ITEM_MAP_MODE_SUB_VECTOR = MENU_ITEM_MAP_MODE + 1;
	private static final int MENU_ITEM_MAP_MODE_SUB_SATELLITE = MENU_ITEM_MAP_MODE + 2;
	private static final int MENU_ITEM_MAP_MODE_SUB_TRAFFIC = MENU_ITEM_MAP_MODE + 3;
	private static final int MENU_ITEM_MAP_MODE_SUB_BICYCLE = MENU_ITEM_MAP_MODE + 4;
	private static final int MENU_ITEM_ZOOM_CONTROLS = 30;
	private static final int MENU_ITEM_MY_LOCATION = 40;

	private static final int MENU_ITEM_TEST_MODE = 50;
	private static final int MENU_ITEM_TEST_POI_DATA = MENU_ITEM_TEST_MODE + 1;
	private static final int MENU_ITEM_TEST_PATH_DATA = MENU_ITEM_TEST_MODE + 2;
	private static final int MENU_ITEM_TEST_FLOATING_DATA = MENU_ITEM_TEST_MODE + 3;
	private static final int MENU_ITEM_TEST_AUTO_ROTATE = MENU_ITEM_TEST_MODE + 4;
	private static final int MENU_ITEM_TEST_NEW_ACTIVITY = MENU_ITEM_TEST_MODE + 7;

	/**
	 * Invoked during init to give the Activity a chance to set up its Menu.
	 * 
	 * @param menu the Menu to which entries may be added
	 * @return true
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		MenuItem menuItem = null;
		SubMenu subMenu = null;
		
		menuItem = menu.add(Menu.NONE, MENU_ITEM_CLEAR_MAP, Menu.CATEGORY_SECONDARY, "지도만 보기");
		menuItem.setAlphabeticShortcut('c');
		menuItem.setIcon(android.R.drawable.ic_menu_mapmode);
		
		menuItem = menu.add(Menu.NONE, MENU_ITEM_TEST_PATH_DATA, Menu.CATEGORY_SECONDARY, "경로선 보기");
		menuItem.setAlphabeticShortcut('p');
		menuItem.setIcon(android.R.drawable.ic_menu_directions);
		
		menuItem = menu.add(Menu.NONE, MENU_ITEM_MY_LOCATION, Menu.CATEGORY_SECONDARY, "내위치");
		menuItem.setAlphabeticShortcut('l');
		menuItem.setIcon(android.R.drawable.ic_menu_mylocation);
		
		subMenu = menu.addSubMenu(Menu.NONE, MENU_ITEM_MAP_MODE, Menu.CATEGORY_SECONDARY, "일반/위성 지도 변경");
		subMenu.setIcon(android.R.drawable.ic_menu_rotate);

		menuItem = subMenu.add(Menu.NONE, MENU_ITEM_MAP_MODE_SUB_VECTOR, Menu.CATEGORY_SECONDARY, "일반지도");
		menuItem.setAlphabeticShortcut('m');

		menuItem = subMenu.add(Menu.NONE, MENU_ITEM_MAP_MODE_SUB_SATELLITE, Menu.CATEGORY_SECONDARY, "위성지도");
		menuItem.setAlphabeticShortcut('s');

//		menuItem = menu.add(Menu.NONE, MENU_ITEM_ZOOM_CONTROLS, Menu.CATEGORY_SECONDARY, "Zoom Controls");
//		menuItem.setAlphabeticShortcut('z');
//		menuItem.setIcon(android.R.drawable.ic_menu_zoom);

//		menuItem = menu.add(Menu.NONE, MENU_ITEM_TEST_POI_DATA, Menu.CATEGORY_SECONDARY, "마커 표시");
//		menuItem.setIcon(android.R.drawable.ic_menu_info_details);
//		menuItem.setAlphabeticShortcut('p');

		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu pMenu) {
		super.onPrepareOptionsMenu(pMenu);

		int viewMode = mMapController.getMapViewMode();

		pMenu.findItem(MENU_ITEM_MAP_MODE_SUB_VECTOR).setChecked(viewMode == NMapView.VIEW_MODE_VECTOR);
		pMenu.findItem(MENU_ITEM_MAP_MODE_SUB_SATELLITE).setChecked(viewMode == NMapView.VIEW_MODE_HYBRID);

		if (mMyLocationOverlay == null) {
			pMenu.findItem(MENU_ITEM_MY_LOCATION).setEnabled(false);
		}

		return true;
	}

	/**
	 * Invoked when the user selects an item from the Menu.
	 * 
	 * @param item the Menu entry which was selected
	 * @return true if the Menu item was legit (and we consumed it), false
	 *         otherwise
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
			case MENU_ITEM_CLEAR_MAP:
				if (mMyLocationOverlay != null) {
					stopMyLocation();
					mOverlayManager.removeOverlay(mMyLocationOverlay);
				}
	
				mMapController.setMapViewMode(NMapView.VIEW_MODE_VECTOR);
				mMapController.setMapViewTrafficMode(false);
				mMapController.setMapViewBicycleMode(false);
	
				mOverlayManager.clearOverlays();
	
				return true;
			case MENU_ITEM_MAP_MODE_SUB_VECTOR:
				mMapController.setMapViewMode(NMapView.VIEW_MODE_VECTOR);
				return true;

			case MENU_ITEM_MAP_MODE_SUB_SATELLITE:
				mMapController.setMapViewMode(NMapView.VIEW_MODE_HYBRID);
				return true;

			case MENU_ITEM_ZOOM_CONTROLS:
				mMapView.displayZoomControls(true);
				return true;

			case MENU_ITEM_MY_LOCATION:
				startMyLocation();
				return true;

			case MENU_ITEM_TEST_POI_DATA:
				mOverlayManager.clearOverlays();

				// add POI data overlay
				testPOIdataOverlay();
				return true;

			case MENU_ITEM_TEST_PATH_DATA:
				mOverlayManager.clearOverlays();

				// add path data overlay
				testPathDataOverlay();

				// add path POI data overlay
				testPathPOIdataOverlay();
				return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		L.d(TAG, " >>> called onDestroy()");
		
        if ( adView != null ) {
            adView.destroy();
            adView = null;
        }		
	}
	
	private void initAdam() {
		// Ad@m 광고 뷰 생성 및 설정
        adView = new AdView(this);

        // 광고 클릭시 실행할 리스너
        adView.setOnAdClickedListener(new OnAdClickedListener() {
            public void OnAdClicked() {
                Log.d(TAG, "광고를 클릭했습니다.");
            }
        });

        // 광고 내려받기 실패했을 경우에 실행할 리스너
        adView.setOnAdFailedListener(new OnAdFailedListener() {
            public void OnAdFailed(AdError arg0, String arg1) {
            	Log.d(TAG, arg1);
            }
        });

        // 광고를 정상적으로 내려받았을 경우에 실행할 리스너
        adView.setOnAdLoadedListener(new OnAdLoadedListener() {
            public void OnAdLoaded() {
                Log.d(TAG, "광고가 정상적으로 로딩되었습니다.");
            }
        });

        // 광고를 불러올때 실행할 리스너
        adView.setOnAdWillLoadListener(new OnAdWillLoadListener() {
            public void OnAdWillLoad(String arg1) {
                Log.d(TAG, "광고를 불러옵니다. : " + arg1);
            }
        });

        // 광고를 닫았을때 실행할 리스너
        adView.setOnAdClosedListener(new OnAdClosedListener() {
            public void OnAdClosed() {
                Log.d(TAG, "광고를 닫았습니다.");
            }
        });

        // 할당 받은 clientId 설정
        adView.setClientId(Constants.CLIENT_ID);

        // 광고 갱신 시간 : 기본 60초
        adView.setRequestInterval(12);

        // Animation 효과 : 기본 값은 AnimationType.NONE
        adView.setAnimationType(AnimationType.FLIP_HORIZONTAL);
        adView.setVisibility(View.VISIBLE);
    }	
	
	/** 
	 * Container view class to rotate map view.
	 */
	private class MapContainerView extends ViewGroup {

		public MapContainerView(Context context) {
			super(context);
		}

		@Override
		protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
			final int width = getWidth();
			final int height = getHeight();
			final int count = getChildCount();
			for (int i = 0; i < count; i++) {
				final View view = getChildAt(i);
				final int childWidth = view.getMeasuredWidth();
				final int childHeight = view.getMeasuredHeight();
				final int childLeft = (width - childWidth) / 2;
				final int childTop = (height - childHeight) / 2;
				view.layout(childLeft, childTop, childLeft + childWidth, childTop + childHeight);
			}

			if (changed) {
				mOverlayManager.onSizeChanged(width, height);
			}
		}

		@Override
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			int w = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
			int h = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
			int sizeSpecWidth = widthMeasureSpec;
			int sizeSpecHeight = heightMeasureSpec;

			final int count = getChildCount();
			for (int i = 0; i < count; i++) {
				final View view = getChildAt(i);

				if (view instanceof NMapView) {
					if (mMapView.isAutoRotateEnabled()) {
						int diag = (((int)(Math.sqrt(w * w + h * h)) + 1) / 2 * 2);
						sizeSpecWidth = MeasureSpec.makeMeasureSpec(diag, MeasureSpec.EXACTLY);
						sizeSpecHeight = sizeSpecWidth;
					}
				}

				view.measure(sizeSpecWidth, sizeSpecHeight);
			}
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
	}
}
