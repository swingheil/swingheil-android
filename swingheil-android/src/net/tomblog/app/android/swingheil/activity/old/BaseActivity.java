package net.tomblog.app.android.swingheil.activity.old;

import net.tomblog.app.android.swingheil.util.L;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.ViewGroup;

/**
 * AD@m 과 같은 광고모듈과 공통 method를 설정하는 기본 Activity
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class BaseActivity extends Activity {
//	public class BaseActivity extends Activity implements AdHttpListener {

	private static final String TAG = BaseActivity.class.getName();
	
	public static final int DIALOG_LOADING = 1;
	public static final int DIALOG_CONNECT_NETWORK = 2;
	
//	private MobileAdView mADView = null;
	protected ViewGroup mRootView;
	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		// AD@m에서 할당 받은 clientId 설정
//		AdConfig.setClientId("12f6Z03T1331fb69923");
//	}
//
//	@Override
//	protected void onResume() {
//		L.d(TAG, " >>> called onResume()");
//		super.onResume();
//	}
//
//	@Override
//	protected void onStart() {
//		L.d(TAG, " >>> called onStart()");
//		super.onStart();
//		
//		addDaumADView();
//	}
//
//	@Override
//	protected void onStop() {
//		L.d(TAG, " >>> called onStart()");
//		super.onStop();
//		
//		removeDaumADView();
//	}
//	
//	@Override
//	public void failedDownloadAd_AdListener(int errorno, String errMsg) {
//		// fail to receive Ad
//		Log.d(TAG, "failedDownloadAd_AdListener errorno : " + errorno + ":" + errMsg);
//		if(mADView != null) {
//			mADView.setVisibility(View.GONE);
//		}
//	}
//
//	@Override
//	public void didDownloadAd_AdListener() {
//	   // success to receive Ad
//		Log.d(TAG, "didDownloadAd_AdListener");
//		if(mADView != null) {
//			mADView.setVisibility(View.VISIBLE);
//		}
//	}
//	
//	private void addDaumADView() {
//		Log.d(TAG, "didDownloadAd_AdListener");
//		
//		if(isNetworkAvailable()) {
//			mADView = new MobileAdView(this);
//			mADView.setAdListener(this);
//			mADView.setVisibility(View.GONE);
//			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//			params.weight = 0;
//			params.setMargins(0, 15, 0, 0);
//			mADView.setLayoutParams(params);
//			
//			mRootView.addView(mADView);
//		}
//	}
//	
//    private void removeDaumADView() {
//    	Log.d(TAG, "didDownloadAd_AdListener");
//    	
//        if( mADView != null ) {
//        	mRootView.removeView(mADView);
//        	mADView.destroy();
//            mADView = null;
//        }
//	}
	
    /**
     * Bearer Type별 네트워크 상태 정보 체크
     * @return Bearer Type별 네트워크 상태 정보
     */
    public boolean isNetworkAvailable(){
    	L.d(TAG, ">>> Called isNetworkAvailable()");
    	
		ConnectivityManager conn_manager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
		boolean isNetworkAvailable = false;
		try {
			NetworkInfo network_info = conn_manager.getActiveNetworkInfo();
			if (network_info != null && network_info.isConnected()) {
				if (network_info.getType() == ConnectivityManager.TYPE_WIFI) {
					isNetworkAvailable = true;
				} else if (network_info.getType() == ConnectivityManager.TYPE_MOBILE) {
					isNetworkAvailable = true;
					int currentapiVersion = android.os.Build.VERSION.SDK_INT;
					if (currentapiVersion > android.os.Build.VERSION_CODES.ECLAIR_MR1) {
						if (network_info.getType() == ConnectivityManager.TYPE_WIMAX) {
							// API Level 8부터 지원 (Android 2.2)
							// 2.2이상 디바이스일때 예외처리할것.
							isNetworkAvailable = true;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		L.d(TAG, "isNetworkAvailable : " + isNetworkAvailable);
		return isNetworkAvailable;
	}
}
