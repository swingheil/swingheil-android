package net.tomblog.app.android.swingheil.activity.old;

import java.io.IOException;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.model.server.Device;
import net.tomblog.app.android.swingheil.parser.RefreshUserInfo;
import net.tomblog.app.android.swingheil.parser.cafe.ArticleParser;
import net.tomblog.app.android.swingheil.parser.server.user.RegisterDeviceParser;
import net.tomblog.app.android.swingheil.util.L;
import net.tomblog.app.android.swingheil.util.Util;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.swingheil.domain.user.SocialUser;

/**
 * Swing Heil 시작 Activity
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 * 
 */
public class OldMainActivity extends Activity implements RefreshUserInfo { 
	
	private static final String TAG = OldMainActivity.class.getName();
	
	public static final String PROPERTY_REG_ID = "registration_id";
	public static final String EXTRA_MESSAGE = "message";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	
	private Button mNaverBoardButton;
	private Button mDaumBoardButton;
	private Button mSwingHeilBoardButton;
	private Button mRegisterDeviceButton;
    
    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     * https://console.developers.google.com/project/apps~swingheils, Project Number:  153187437917
     */
    String SENDER_ID = "153187437917";
	Context mContext;
	GoogleCloudMessaging mGcm;
	// GCM Registeration ID
	String mRegId;
	  
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	L.d(TAG, " >>> called onCreate()");
        setContentView(R.layout.activity_old_main);
        
        // http.keepAlive를 false로 설정하지 않으면 http request를 보낼때, 두번에 한번 에러가 난다.
        System.setProperty("http.keepAlive", "false");
                            
        // Inflate views.
        inflateView();
    
        // Initialize objects.
        initObject();
        
        mContext = getApplicationContext();
        // Check device for Play Services APK. If check succeeds, proceed with GCM registration.
        if (checkPlayServices()) {
        	mGcm = GoogleCloudMessaging.getInstance(this);
        	mRegId = getRegistrationId(mContext);

            if (mRegId.isEmpty()) {  
                registerInBackground();
            }else {
            	Log.d(TAG, "already registered, registrationId : " + mRegId);
            }
        } else {
            Log.d(TAG, "No valid Google Play Services APK found.");
        }
        
        super.onCreate(savedInstanceState);  
    }
	
	@Override
	protected void onStop() {
		L.d(TAG, " >>> called onStart()");
		super.onStop();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.swing_heil_menu, menu);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		int itemId = item.getItemId();
		if (itemId == R.id.set_oauth) {
			intent = new Intent(OldMainActivity.this, SetUpOAuthActivity.class);
			startActivity(intent);
			return true;
		} else {
		}

		return false;
	}

	private void inflateView() {
		L.d(TAG, " >>> called inflateView()");
		
//		mRootView  = (ViewGroup)findViewById(R.id.root);
		mNaverBoardButton = (Button)findViewById(R.id.naver_board_button);
		mDaumBoardButton = (Button)findViewById(R.id.daum_board_button);
		mSwingHeilBoardButton = (Button)findViewById(R.id.swingheil_board_button);
		mRegisterDeviceButton = (Button)findViewById(R.id.swingheil_register_device_button);
	}
	
    private void initObject() {
    	L.d(TAG, " >>> called initObject()");
    	
    	mNaverBoardButton.setOnClickListener(new OnClickListener() {
    		@Override
    		public void onClick(View v) {
    			String cafeId = ArticleParser.getCafeId(ArticleParser.CAFE_SWING_ADE);
    			String boardId = ArticleParser.getNoticeBoardId(ArticleParser.CAFE_SWING_ADE);
    			Intent intent = new Intent(OldMainActivity.this, ArticleListActivity.class);
    			intent.putExtra(ArticleParser.INTENT_CAFE_ID, cafeId);
    			intent.putExtra(ArticleParser.INTENT_BOARD_ID, boardId);
    			startActivity(intent);
    		}
    	});
    	mDaumBoardButton.setOnClickListener(new OnClickListener() {
    		@Override
    		public void onClick(View v) {
    			String cafeId = ArticleParser.getCafeId(ArticleParser.CAFE_CRAZY_SWING);
    			String boardId = ArticleParser.getNoticeBoardId(ArticleParser.CAFE_CRAZY_SWING);
    			Intent intent = new Intent(OldMainActivity.this, ArticleListActivity.class);
    			intent.putExtra(ArticleParser.INTENT_CAFE_ID, cafeId);
    			intent.putExtra(ArticleParser.INTENT_BOARD_ID, boardId);
    			startActivity(intent);
    		}
    	});
    	mSwingHeilBoardButton.setOnClickListener(new OnClickListener() {
    		@Override
    		public void onClick(View v) {
    			String cafeId = ArticleParser.getCafeId(ArticleParser.CAFE_SWING_HEIL);
    			String boardId = ArticleParser.getNoticeBoardId(ArticleParser.CAFE_SWING_HEIL);
    			Intent intent = new Intent(OldMainActivity.this, ArticleListActivity.class);
    			intent.putExtra(ArticleParser.INTENT_CAFE_ID, cafeId);
    			intent.putExtra(ArticleParser.INTENT_BOARD_ID, boardId);
    			startActivity(intent);
    		}
    	});
    	mRegisterDeviceButton.setOnClickListener(new OnClickListener() {
    		@Override
    		public void onClick(View v) {
    			String registrationId = getRegistrationId(mContext);
    			sendRegistrationIdToBackend(registrationId);
    		}
    	}); 
	}
    
    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.d(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId(Context context) {
    	L.d(TAG, " >>> called getRegistrationId()");
        final SharedPreferences prefs = getGcmPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.d(TAG, "Registration not found.");
            return "";
        }
        
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.d(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }
    
    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGcmPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(OldMainActivity.class.getSimpleName(), Context.MODE_PRIVATE);
    }
    
    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    
    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (mGcm == null) {
                        mGcm = GoogleCloudMessaging.getInstance(mContext);
                    }
                    mRegId = mGcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + mRegId;

                    // You should send the registration ID to your server over HTTP, so it
                    // can use GCM/HTTP or CCS to send messages to your app.
                    sendRegistrationIdToBackend(mRegId);

                    // For this demo: we don't need to send it because the device will send
                    // upstream messages to a server that echo back the message using the
                    // 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(mContext, mRegId);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            	L.d(TAG, msg);
            }
        }.execute(null, null, null);
    }
    
    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send
     * messages to your app. Not needed for this demo since the device sends upstream messages
     * to a server that echoes back the message using the 'from' address in the message.
     * @param mRegId2 
     */
    private void sendRegistrationIdToBackend(String registrationId) {
    	L.d(TAG, " >>> called sendRegistrationIdToBackend() registrationId : " +  registrationId); 
    	// mRegId와 push message를 받을지 여부를 DB로 전송한다.
    	
    	String nickname = "Tom";
    	String deviceUid = Util.getDeviceUid(getApplicationContext());
    	String userId = "";
    	String modelId = Build.MODEL;
    	String osVersion = Build.ID + " " + Build.VERSION.RELEASE; 
    	 
    	Device device = new Device(nickname, deviceUid, userId, modelId, osVersion, registrationId);
    	
		new RegisterDeviceParser(device, this, true).execute(); 
    }
    
    /**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGcmPreferences(context);
        int appVersion = getAppVersion(context);
        Log.d(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

	@Override
	public void refreshUserInfo(SocialUser socialUser) {
		// TODO Auto-generated method stub
		
	}
    
}
    