package net.tomblog.app.android.swingheil.activity;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.parser.DownloadImagesTask;
import net.tomblog.app.android.swingheil.parser.RefreshAdapter;
import net.tomblog.app.android.swingheil.util.L;
import net.tomblog.app.android.swingheil.util.ThumbnailHolder;
import net.tomblog.app.android.swingheil.util.TouchImageView;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Window;

/**
 * Image에 대해서 Touch & Zoom 이벤트를 처리해주는 Activity
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class ImageDetailActivity extends Activity implements RefreshAdapter {

	private static final String TAG = ImageDetailActivity.class.getName();
	
	public static final String POSTER_URL = "poster_url";
	public static final String THUMBNAIL_URL = "thumbnail_url";

	private TouchImageView mTouchImage;

	private String mPosterUrl = null;
	private String mThumbnailUrl = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		L.d(TAG, " >>> called onCreate()");  
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_image_detail); 
  
		inflateView();

		initObject();
		
		super.onCreate(savedInstanceState);
	}
	
	private void inflateView() {
		L.d(TAG, " >>> called inflateView()");
	
		mTouchImage = (TouchImageView) findViewById(R.id.touch_image);
	}

	
    private void initObject() {
    	L.d(TAG, " >>> called initObject()");

    	Intent intent = getIntent();
    	mPosterUrl = intent.getStringExtra(POSTER_URL);
    	mThumbnailUrl = intent.getStringExtra(THUMBNAIL_URL);
    	
		ThumbnailHolder thumbnailHolder = ThumbnailHolder.getInstance();
		Bitmap poster = thumbnailHolder.retrieve(mPosterUrl);
		
		if(poster != null) {
			mTouchImage.setImageBitmap(poster); 
		}else {
			Bitmap thumbnail = thumbnailHolder.retrieve(mThumbnailUrl);
			if(thumbnail != null) {
				mTouchImage.setImageBitmap(thumbnail);
			} 
			
			DownloadImagesTask task = new DownloadImagesTask(this);
			thumbnailHolder.setTask(mPosterUrl, task);
			task.execute(mPosterUrl);
		}
    }

	@Override
	public void refreshAdapter() {
		L.d(TAG, " >>> called refresh()");  
		
		ThumbnailHolder thumbnailHolder = ThumbnailHolder.getInstance(); 
		Bitmap poster = thumbnailHolder.retrieve(mPosterUrl);
		if(poster != null) {
			mTouchImage.setImageBitmap(poster); 
		}else {
			L.e(TAG, "poster bitmap is null"); 
		}
	}   	
    
}