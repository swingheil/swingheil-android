package net.tomblog.app.android.swingheil.activity;

import java.io.IOException;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.SwingHeilApplication;
import net.tomblog.app.android.swingheil.activity.old.OldMainActivity;
import net.tomblog.app.android.swingheil.model.server.Device;
import net.tomblog.app.android.swingheil.parser.RefreshUserInfo;
import net.tomblog.app.android.swingheil.parser.server.FavoriteParser;
import net.tomblog.app.android.swingheil.parser.server.user.GetUserParser;
import net.tomblog.app.android.swingheil.parser.server.user.RegisterDeviceParser;
import net.tomblog.app.android.swingheil.util.Constants;
import net.tomblog.app.android.swingheil.util.L;
import net.tomblog.app.android.swingheil.util.Util;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.swingheil.domain.user.SocialUser;

/**
 * Swing Heil 시작 Activity
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 * 
 */
public class StartActivity extends Activity implements RefreshUserInfo { 
	
	private static final String TAG = StartActivity.class.getName();
	private static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	
	private Button mRegisterNicknameBtn;
	private EditText mNicknameEt;
	private Context mContext;
	private GoogleCloudMessaging mGcm;
	// GCM Registeration ID
	private String mRegId;
	/**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     * https://console.developers.google.com/project/apps~swingheils, Project Number:  153187437917
     */
    private String SENDER_ID = "153187437917";
    private String mNickname = "";
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	L.d(TAG, " >>> called onCreate()");
        super.onCreate(savedInstanceState); 
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);  
        
        setContentView(R.layout.activity_start);
                            
        // Inflate views.
        inflateView();
    
        // Initialize objects.
        initObject();
        
        mContext = getApplicationContext();
        
        registerInBackground(false);
        
        String userId = SwingHeilApplication.getInstance().getUserId();
        String deviceUid = SwingHeilApplication.getInstance().getDeviceUid();
        L.d(TAG, "userId : " + userId + ", deviceUid : " + deviceUid);
        // 01] 기기에 사용자 id가 있다면 SwingHeil 메인 페이지로 이동.
        if(userId != null && !"".equals(userId)) {
        	// 이 activity 종료 후 Main Activity 실행.
        	
			Intent intent = new Intent(getApplicationContext(), SwingHeilActivity.class); 
			startActivity(intent);	 
			finish();
        } else {
        	// 02] 사용자 id가 없다면 deviceUid로 서버에 사용자 id 조회한다.
        	// 	     서버에 사용자 id가 있다면 이 activity 종료 후 Main Activity 실행.
        	//	     서버에 사용자 id가 없다면 nickname 등록 후 사용자 id 기기에 저장.
        	if(Util.isNetworkAvailable(mContext)) {
        		new GetUserParser(deviceUid, this).execute();
        	}
        }
    }
	
	@Override
	protected void onStop() {
		L.d(TAG, " >>> called onStart()");
		super.onStop();
	}
	

	private void inflateView() {
		L.d(TAG, " >>> called inflateView()");
		
		mRegisterNicknameBtn = (Button)findViewById(R.id.register_nickname_button);
		mNicknameEt = (EditText)findViewById(R.id.nickname_edittext);
	}
	
    private void initObject() {
    	L.d(TAG, " >>> called initObject()");

    	mRegisterNicknameBtn.setOnClickListener(new OnClickListener() {
    		@Override
    		public void onClick(View v) {
    			String nickname = mNicknameEt.getText().toString();
    			if("".equals(nickname)) {
    				Toast.makeText(getApplicationContext(), R.string.enter_nickname, Toast.LENGTH_LONG).show();
				} else {
					if(Util.isNetworkAvailable(mContext)) {
						setProgressBarIndeterminateVisibility(true);
						mNickname = nickname;
						registerInBackground(true);
					}else {
			    		finish();
			    		Intent intent = new Intent(getApplicationContext(), SwingHeilActivity.class); 
			    		startActivity(intent);
					}
    			}
    		}
    	});
	}

	@Override
	public void refreshUserInfo(SocialUser socialUser) {
		L.d(TAG, " >>> called refreshUserInfo()");
		setProgressBarIndeterminateVisibility(false);
		
		if(socialUser != null && !"".equals(socialUser.getId())) {
			L.d(TAG, socialUser.toString());
	        SharedPreferences prefs = mContext.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE);
	        SharedPreferences.Editor editor = prefs.edit();
			editor.putString(Constants.USER_ID, socialUser.getId());
			editor.commit();
			
    		SwingHeilApplication.getInstance().setUserId(socialUser.getId());

    		finish();
    		
    		Intent intent = new Intent(getApplicationContext(), SwingHeilActivity.class); 
    		startActivity(intent);
		}
	}
	
	/**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground(final boolean withoutRefreshUserInfo) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (mGcm == null) {
                        mGcm = GoogleCloudMessaging.getInstance(mContext);
                    }
                    mRegId = mGcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + mRegId;

                    // You should send the registration ID to your server over HTTP, so it
                    // can use GCM/HTTP or CCS to send messages to your app.
                    sendRegistrationIdToBackend(mRegId, withoutRefreshUserInfo);

                    // For this demo: we don't need to send it because the device will send
                    // upstream messages to a server that echo back the message using the
                    // 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(mContext, mRegId);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            	L.d(TAG, msg);
            }
        }.execute(null, null, null);
    }
    
    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send
     * messages to your app. Not needed for this demo since the device sends upstream messages
     * to a server that echoes back the message using the 'from' address in the message.
     * @param withoutRefreshUserInfo 
     * @param mRegId2 
     */
    private void sendRegistrationIdToBackend(String registrationId, boolean withoutRefreshUserInfo) {
    	L.d(TAG, " >>> called sendRegistrationIdToBackend() registrationId : " +  registrationId); 
    	// mRegId와 push message를 받을지 여부를 DB로 전송한다.
    	
    	String deviceUid = Util.getDeviceUid(getApplicationContext());
    	String modelId = Build.MODEL;
    	String osVersion = Build.ID + " " + Build.VERSION.RELEASE; 
    	 
    	Device device = new Device(mNickname, deviceUid, null, modelId, osVersion, registrationId);
    	
		new RegisterDeviceParser(device, this, withoutRefreshUserInfo).execute(); 
    }
    
    /**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGcmPreferences(context);
        int appVersion = getAppVersion(context);
        Log.d(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }
    
    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGcmPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(OldMainActivity.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
}
    