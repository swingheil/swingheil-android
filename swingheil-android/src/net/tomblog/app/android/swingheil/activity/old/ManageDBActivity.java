package net.tomblog.app.android.swingheil.activity.old;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.model.Article;
import net.tomblog.app.android.swingheil.parser.cafe.ArticleParser;
import net.tomblog.app.android.swingheil.util.DefaultPreference;
import net.tomblog.app.android.swingheil.util.L;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * DB관리를 위한 테스트용 Activity
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 * 
 */
public class ManageDBActivity extends BaseActivity {

	private static final String TAG = ManageDBActivity.class.getName();
	
	public static final int MANAGE_DB = 11;

	private Button mCreateButton;
	private Button mReadButton;
	private Button mReadAllButton;
	private Button mUpdateButton;
	private Button mDeleteButton;
	private Button mDeleteALLButton;

	private static Toast mToast;

	private String mCafeId;
	private String mBoardId;
	private String[] mSelectionArgs;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		L.d(TAG, " >>> called onCreate()");
		setContentView(R.layout.activity_manage_db);

		Intent intent = getIntent();
		mCafeId = intent.getStringExtra(ArticleParser.INTENT_CAFE_ID);
		mBoardId = intent.getStringExtra(ArticleParser.INTENT_BOARD_ID);
		mSelectionArgs = new String[] { mCafeId, mBoardId };

		// Inflate views.
		inflateView();

		// Initialize objects.
		initObject();

		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onStop() {
		L.d(TAG, " >>> called onStart()");
		super.onStop();
	}

	private void inflateView() {
		L.d(TAG, " >>> called inflateView()");

		mRootView = (ViewGroup) findViewById(R.id.root);

		mCreateButton = (Button) findViewById(R.id.create_button);
		mReadButton = (Button) findViewById(R.id.read_button);
		mReadAllButton = (Button) findViewById(R.id.read_all_button);
		mUpdateButton = (Button) findViewById(R.id.update_button);
		mDeleteButton = (Button) findViewById(R.id.delete_button);
		mDeleteALLButton = (Button) findViewById(R.id.delete_all_button);
	}

	private void initObject() {
		L.d(TAG, " >>> called initObject()");

		mCreateButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View paramView) {
				createArticle();
			}
		});
		mReadButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View paramView) {
				readArticle();
			}
		});
		mReadAllButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View paramView) {
				readAllArticle();
			}
		});
		mUpdateButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View paramView) {
				updateArticle();
			}
		});
		mDeleteButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View paramView) {
				deleteArticle();
			}
		});
		mDeleteALLButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View paramView) {
				deleteAllButton();
			}
		});

		mToast = Toast
				.makeText(ManageDBActivity.this, null, Toast.LENGTH_SHORT);
	}

	private void deleteAllButton() {
		getContentResolver().delete(Article.CONTENT_URI, Article.SELECTION, mSelectionArgs);

		mToast.setText("everything was deleted");
		mToast.show();

		deletePref();
		
		setResult(ArticleListActivity.RESET_CURRENT_PAGE);
	}

	private void deleteArticle() {
		Cursor cursor = managedQuery(Article.CONTENT_URI, null, Article.SELECTION, mSelectionArgs,
				null);

		StringBuilder sb = new StringBuilder();
		if (cursor.moveToFirst()) {
			sb.append(cursor.getString(cursor.getColumnIndex(Article._ID))
					+ ", "
					+ cursor.getString(cursor
							.getColumnIndex(Article.ARTICLE_ID))
					+ ", "
					+ cursor.getString(cursor
							.getColumnIndex(Article.ARTICLE_TITLE)));

			String id = cursor.getString(cursor.getColumnIndex(Article._ID));
			getContentResolver().delete(
					Uri.withAppendedPath(Article.CONTENT_URI, id), Article.SELECTION, mSelectionArgs);
		} else {
			sb.append("nothing");
		}

		mToast.setText(sb.toString() + " was deleted");
		mToast.show();
	}

	private void updateArticle() {
		Cursor cursor = managedQuery(Article.CONTENT_URI, null, Article.SELECTION, mSelectionArgs,
				null);

		StringBuilder sb = new StringBuilder();
		Uri uri = null;
		int nextArticleId = 0;
		if (cursor.moveToFirst()) {
			String articleId = cursor.getString(cursor
					.getColumnIndex(Article.ARTICLE_ID));
			nextArticleId = Integer.parseInt(articleId) + 1;
			ContentValues editedValues = new ContentValues();
			editedValues.put(Article.ARTICLE_ID, String.valueOf(nextArticleId));

			String id = cursor.getString(cursor.getColumnIndex(Article._ID));
			uri = Uri.withAppendedPath(Article.CONTENT_URI, id);
			getContentResolver().update(uri, editedValues, Article.SELECTION, mSelectionArgs);

			sb.append(uri.toString() + " " + nextArticleId);
		} else {
			sb.append("there's nothing to update");
		}

		mToast.setText(sb.toString());
		mToast.show();
	}

	private void readAllArticle() {
		Cursor cursor = managedQuery(Article.CONTENT_URI, null, Article.SELECTION, mSelectionArgs,
				null);

		StringBuilder sb = new StringBuilder();
		if (cursor.moveToFirst()) {
			do {
				sb.append(cursor.getString(cursor.getColumnIndex(Article._ID))
						+ ", "
						+ cursor.getString(cursor
								.getColumnIndex(Article.ARTICLE_ID))
						+ ", "
						+ cursor.getString(cursor
								.getColumnIndex(Article.ARTICLE_TITLE)) + "\n");
			} while (cursor.moveToNext());
		} else {
			sb.append("there's nothing to read");
		}

		mToast.setText(sb.toString());
		mToast.show();
	}

	private void readArticle() {
		Cursor cursor = managedQuery(Article.CONTENT_URI, null, Article.SELECTION, mSelectionArgs,
				null);

		StringBuilder sb = new StringBuilder();
		if (cursor.moveToFirst()) {
			sb.append(cursor.getString(cursor.getColumnIndex(Article._ID))
					+ ", "
					+ cursor.getString(cursor
							.getColumnIndex(Article.ARTICLE_ID))
					+ ", "
					+ cursor.getString(cursor
							.getColumnIndex(Article.ARTICLE_TITLE)) + "\n");
		} else {
			sb.append("there's nothing to read");
		}

		mToast.setText(sb.toString());
		mToast.show();
	}

	private void createArticle() {
		long val = System.currentTimeMillis();
		ContentValues values = new ContentValues();
		values.put(Article.BOARD_ID, mBoardId);
		values.put(Article.ARTICLE_ID, (int)(val/10000));
//		values.put(Article.ARTICLE_ID, 1000);
		values.put(Article.ARTICLE_TITLE, "title " + val / 10000);
		values.put(Article.NICKNAME, "nickname");
		values.put(Article.WRITE_DATE, val);
		values.put(Article.COMMENT_COUNT, 30);
		values.put(Article.READ_COUNT, 40);
		values.put(Article.CAFE_ID, mCafeId);
		values.put(Article.HIDDEN, 0);
		values.put(Article.NEW_ARTICLE, 1);
		Uri uri = getContentResolver().insert(Article.CONTENT_URI, values);

		mToast.setText(uri.toString());
		mToast.show();
	}

	private void deletePref() {
		DefaultPreference pref = new DefaultPreference(this);
		pref.storePreference(mCafeId + mBoardId, null);
	}
}