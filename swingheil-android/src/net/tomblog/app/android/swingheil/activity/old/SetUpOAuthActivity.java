package net.tomblog.app.android.swingheil.activity.old;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.oauth.SwingHeilOAuth;
import net.tomblog.app.android.swingheil.oauth.SwingHeilOAuth.OAuthListener;
import net.tomblog.app.android.swingheil.util.L;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

/**
 * OAuth 방식으로 로그인하여 AccessToken을 받기위한 Activity
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 * 
 */
public class SetUpOAuthActivity extends BaseActivity {

	private static final String TAG = SetUpOAuthActivity.class.getName();
	
	private static final int DIALOG_DELETE_NAVER_OAUTH = 1;
	private static final int DIALOG_DELETE_DAUM_OAUTH = 2;
	private static final int DIALOG_DELETE_TWITTER_OAUTH = 3;

	private ToggleButton mNaverButton;
	private ToggleButton mDaumButton;
	private ToggleButton mTwitterButton;
	private TextView mNaverStatus;
	private TextView mDaumStatus;
	private TextView mTwitterStatus;
	
	private String mProviderName = "";
	private SwingHeilOAuth mOAuth;    
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		L.d(TAG, " >>> called onCreate()");
		setContentView(R.layout.activity_set_oauth);
  
		inflateView();
		
		initObject();

		mOAuth = new SwingHeilOAuth(this, SwingHeilOAuth.NAVER, mOAuthListener);
		
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {
		L.d(TAG, " >>> called onResume()");
		super.onResume();
		
		checkAccessTokens();
	}
	
	private void checkAccessTokens() {
		L.d(TAG, " >>> called checkAccessTokens()");
		
		boolean hasAccessToken = mOAuth.hasAccessToken(SwingHeilOAuth.NAVER);
		if(hasAccessToken) {
			mNaverButton.setChecked(true);
			mNaverStatus.setText(R.string.exist_oauth);
		}else {
			mNaverButton.setChecked(false);
			mNaverStatus.setText(R.string.not_exist_oauth);
		}
		hasAccessToken = mOAuth.hasAccessToken(SwingHeilOAuth.DAUM);
		if(hasAccessToken) {
			mDaumButton.setChecked(true);
			mDaumStatus.setText(R.string.exist_oauth);
		}else {
			mDaumButton.setChecked(false);
			mDaumStatus.setText(R.string.not_exist_oauth);
		}
		hasAccessToken = mOAuth.hasAccessToken(SwingHeilOAuth.TWITTER);
		if(hasAccessToken) {
			mTwitterButton.setChecked(true);
			mTwitterStatus.setText(R.string.exist_oauth);
		}else {
			mTwitterButton.setChecked(false);
			mTwitterStatus.setText(R.string.not_exist_oauth);
		}
		
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		String strFormat = getResources().getString(R.string.alert_dialog_delete_oauth);
		String title = String.format(strFormat, mProviderName);
		
		switch (id) {
        case DIALOG_DELETE_NAVER_OAUTH:
            return new AlertDialog.Builder(SetUpOAuthActivity.this)
                .setIcon(R.drawable.alert_dialog_icon) 
                .setTitle(title)
                .setPositiveButton(R.string.alert_dialog_delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    	mOAuth.resetAccessToken(SwingHeilOAuth.NAVER);
                		mNaverStatus.setText(R.string.not_exist_oauth);
                		mNaverButton.setChecked(false);
                    }
                })
                .setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                		mNaverButton.setChecked(true);
                    }
                })
                .create();
        case DIALOG_DELETE_DAUM_OAUTH:
        	return new AlertDialog.Builder(SetUpOAuthActivity.this)
        	.setIcon(R.drawable.alert_dialog_icon) 
        	.setTitle(title)
        	.setPositiveButton(R.string.alert_dialog_delete, new DialogInterface.OnClickListener() {
        		public void onClick(DialogInterface dialog, int whichButton) {
        			mOAuth.resetAccessToken(SwingHeilOAuth.DAUM);
    				mDaumStatus.setText(R.string.not_exist_oauth);
    				mDaumButton.setChecked(false);
        		}
        	})
        	.setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
        		public void onClick(DialogInterface dialog, int whichButton) {
    				mDaumButton.setChecked(true);
        		}
        	})
        	.create();
        case DIALOG_DELETE_TWITTER_OAUTH:
        	return new AlertDialog.Builder(SetUpOAuthActivity.this)
        	.setIcon(R.drawable.alert_dialog_icon) 
        	.setTitle(title)
        	.setPositiveButton(R.string.alert_dialog_delete, new DialogInterface.OnClickListener() {
        		public void onClick(DialogInterface dialog, int whichButton) {
        			mOAuth.resetAccessToken(SwingHeilOAuth.TWITTER);
        			mTwitterStatus.setText(R.string.not_exist_oauth);
        			mTwitterButton.setChecked(false);
        		}
        	})
        	.setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
        		public void onClick(DialogInterface dialog, int whichButton) {
        			mTwitterButton.setChecked(true);
        		}
        	})
        	.create();
        }
		return null;
	}
	
	private void inflateView() {
		L.d(TAG, " >>> called inflateView()");
	  
		mRootView  = (ViewGroup)findViewById(R.id.root);
		
		mNaverButton = (ToggleButton)findViewById(R.id.naver_oauth_button);
		mDaumButton = (ToggleButton)findViewById(R.id.daum_oauth_button);
		mTwitterButton = (ToggleButton)findViewById(R.id.twitter_oauth_button);
		mNaverStatus = (TextView)findViewById(R.id.naver_status);
		mDaumStatus = (TextView)findViewById(R.id.daum_status);
		mTwitterStatus = (TextView)findViewById(R.id.twitter_status);
	}
	
	
    private void initObject() {
    	L.d(TAG, " >>> called initObject()");
    	
		mNaverButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mNaverButton.isChecked()) {
					mNaverButton.setChecked(false);
					mOAuth.changeProvider(SwingHeilOAuth.NAVER);
					mOAuth.authorize();
				}else {
					mNaverButton.setChecked(true);
					mProviderName = getResources().getString(R.string.naver);
					showDialog(DIALOG_DELETE_NAVER_OAUTH);
				}
			}
		});
		mDaumButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mDaumButton.isChecked()) {
					mDaumStatus.setText(R.string.exist_oauth);
				}else {
					mDaumButton.setChecked(true);
					mProviderName = getResources().getString(R.string.daum);
					showDialog(DIALOG_DELETE_DAUM_OAUTH);
				}
			}
		});
		mTwitterButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mTwitterButton.isChecked()) {
					mTwitterStatus.setText(R.string.exist_oauth);
				}else {
					mTwitterButton.setChecked(true);
					mProviderName = getResources().getString(R.string.twitter);
					showDialog(DIALOG_DELETE_TWITTER_OAUTH);
				}
			}
		});
	}
	
	private OAuthListener mOAuthListener = new OAuthListener() {
		
		private static final String TAG = "OAuthListener";
		
		@Override
		public void onComplete(String providerName, int resultCode) {
			L.d(TAG, providerName + " : " + resultCode);			
			if (providerName.equals(SwingHeilOAuth.NAVER)) {
				mNaverStatus.setText(R.string.exist_oauth);
				mNaverButton.setChecked(true);
			} else if (providerName.equals(SwingHeilOAuth.DAUM)) {
			} else if (providerName.equals(SwingHeilOAuth.TWITTER)) {
			}
		}
		
		@Override
		public void onError(String providerName, int resultCode) {
			L.d(TAG, providerName + " : " + resultCode);			
			if (providerName.equals(SwingHeilOAuth.NAVER)) {
				mNaverButton.setChecked(false);
			} else if (providerName.equals(SwingHeilOAuth.DAUM)) {
			} else if (providerName.equals(SwingHeilOAuth.TWITTER)) {
			}
		}
	};
}
