package net.tomblog.app.android.swingheil.oauth;

import net.tomblog.app.android.swingheil.R;
import net.tomblog.app.android.swingheil.util.L;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * OAuth 인증 절차 중 로그인 후 varifier를 받기 위한 다이얼로그
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class OAuthDialog extends Dialog {
	
	private static final String TAG = "OAuthDialog";
	
    static final float[] DIMENSIONS_LANDSCAPE = {460, 260};
    static final float[] DIMENSIONS_PORTRAIT = {280, 420};
    static final FrameLayout.LayoutParams FILL = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                         						ViewGroup.LayoutParams.FILL_PARENT);
    static final int MARGIN = 4;
    static final int PADDING = 2;

    private String mUrl;
    private OAuthDialogListener mListener;
    private ProgressDialog mSpinner;
    private WebView mWebView;
    private LinearLayout mContent;
    private TextView mTitle;
    
    private boolean isCompleted = false;
    private String mProviderName =  "";
    
    public OAuthDialog(Context context, String url, OAuthDialogListener listener, String providerName) {
        super(context);

		mUrl = url;
		mListener = listener;
		mProviderName = providerName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSpinner = new ProgressDialog(getContext());
        
        mSpinner.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mSpinner.setMessage("Loading...");

        mContent = new LinearLayout(getContext());
        
        mContent.setOrientation(LinearLayout.VERTICAL);
        
        setUpTitle();
        setUpWebView();
        
        Display display 	= getWindow().getWindowManager().getDefaultDisplay();
        final float scale 	= getContext().getResources().getDisplayMetrics().density;
        float[] dimensions 	= (display.getWidth() < display.getHeight()) ? DIMENSIONS_PORTRAIT : DIMENSIONS_LANDSCAPE;
        
        addContentView(mContent, new FrameLayout.LayoutParams((int) (dimensions[0] * scale + 0.5f),
        							(int) (dimensions[1] * scale + 0.5f)));
    }

    private void setUpTitle() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        Drawable icon = getIcon(mProviderName);;
        
        mTitle = new TextView(getContext());
        mTitle.setText(getTitle(mProviderName));
        mTitle.setTextColor(Color.WHITE);
        mTitle.setTypeface(Typeface.DEFAULT_BOLD);
        mTitle.setBackgroundColor(0xFFbbd7e9);
        mTitle.setPadding(MARGIN + PADDING, MARGIN, MARGIN, MARGIN);
        mTitle.setCompoundDrawablePadding(MARGIN + PADDING);
        mTitle.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null);
        
        
        mContent.addView(mTitle);
    }

    private String getTitle(String providerName) {
    	String title = null;
    	
		if (providerName.equals(SwingHeilOAuth.NAVER)) {
			title = "Naver";
		} else if (providerName.equals(SwingHeilOAuth.DAUM)) {
			title = "Daum";
		} else if (providerName.equals(SwingHeilOAuth.TWITTER)) {
			title = "Twitter";
		}
		
		return title;
	}

	private Drawable getIcon(String providerName) {
		Drawable icon = null;
		
		if (providerName.equals(SwingHeilOAuth.NAVER)) {
			icon = getContext().getResources().getDrawable(R.drawable.twitter_icon);
		} else if (providerName.equals(SwingHeilOAuth.DAUM)) {
			icon = getContext().getResources().getDrawable(R.drawable.twitter_icon);
		} else if (providerName.equals(SwingHeilOAuth.TWITTER)) {
			icon = getContext().getResources().getDrawable(R.drawable.twitter_icon);
		}
    	
		return icon;
	}

	private void setUpWebView() {
        mWebView = new WebView(getContext());
        
        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.setWebViewClient(new OAuthDialogWebViewClient());
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(mUrl);
        mWebView.setLayoutParams(FILL);
        
        mContent.addView(mWebView);
    }

    private class OAuthDialogWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
        	L.e(TAG, "Redirecting URL : " + url);

        	if (url.startsWith(SwingHeilOAuth.CALLBACK_URL)) {
        		mListener.onComplete(url);
        		
        		OAuthDialog.this.dismiss();
        		return true;
        	} else {
        		return false;
        	}
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        	L.e(TAG, "Page error : " + description);
        	
            super.onReceivedError(view, errorCode, description, failingUrl);
            mListener.onError(description);
            OAuthDialog.this.dismiss();  
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            L.e(TAG, "Loading URL : " + url);
            super.onPageStarted(view, url, favicon);
            
            if(isCompleted == false) {
	            if (url.contains(SwingHeilOAuth.CALLBACK_URL)) {
	            	L.e(TAG, "Contains callback url and will be dissmissed.");
	            		mListener.onComplete(url);
	            		OAuthDialog.this.dismiss();
	            		isCompleted = true;
	            		return;
	            }
            	mSpinner.show();
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            String title = mWebView.getTitle();
            if (title != null && title.length() > 0) {
                mTitle.setText(title);
            }
            mSpinner.dismiss();
        }
        
        
    }
    
	public interface OAuthDialogListener {
		public void onComplete(String value);

		public void onError(String value);
	}
}