package net.tomblog.app.android.swingheil.oauth;

import net.tomblog.app.android.swingheil.model.AccessToken;
import net.tomblog.app.android.swingheil.util.DefaultPreference;
import android.content.Context;

/**
 * Preference에 각 Provider의 OAuth관련 access token과 token secret를 저장한다. 
 * 
 * @author hyunung.park (parkbear01@gmail.com)
 *
 */
public class OAuthPreference extends DefaultPreference{
	
	private static final String ACCESS_TOKEN = "_access_token";
	private static final String TOKEN_SECRET = "_token_secret";
	private static final String SHARED = "OAuth_Preferences";
	
	public OAuthPreference(Context context) {
		super(context);
		sharedPref = context.getSharedPreferences(SHARED, Context.MODE_PRIVATE);
		editor = sharedPref.edit();
	}

	public void storeAccessToken(AccessToken accessToken, String providerName) {
		if(providerName != null && accessToken != null) {
			editor.putString(providerName + ACCESS_TOKEN, accessToken.getAccessToken());
			editor.putString(providerName + TOKEN_SECRET, accessToken.getTokenSecret());
			editor.commit();
		}
	}

	public void resetAccessToken(String providerName) {
		if(providerName != null) {
			editor.putString(providerName + ACCESS_TOKEN, null);
			editor.putString(providerName + TOKEN_SECRET, null);
	
			editor.commit();
		}
	}

	public AccessToken getAccessToken(String providerName) {
		String accessToken = sharedPref.getString(providerName + ACCESS_TOKEN, null);
		String tokenSecret = sharedPref.getString(providerName + TOKEN_SECRET, null);

		if (accessToken != null && tokenSecret != null) {
			return new AccessToken(accessToken, tokenSecret);
		} else {
			return null;
		}
	}
}
